﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using BCEntities;
using BCTrackingDAL;

namespace BCTrackingBL
{
    public class Common
    {

        public static DataTable ListofProposal(string UserId)
        {
            return DBMaster.ListofProposal(UserId);
        }
        public static List<UserEntity> getgroupProduct(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords)
        {
            DataTable dtLead = DBMaster.getgroupProduct(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch);
            List<UserEntity> stateList = new List<UserEntity>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new UserEntity();
                state.productId = Convert.ToInt32(dr["id"]);
                state.productName = dr["GroupName"].ToString();
                state.UserName = dr["FirstName"].ToString();
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<State> getState(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords)
        {
            DataTable dtLead = DBMaster.getState(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch);
            List<State> stateList = new List<State>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new State();
                state.StateId = Convert.ToInt32(dr["StateId"]);
                state.StateName = dr["StateName"].ToString();              
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<Corporates> getCorporate(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords)
        {
            DataTable dtLead = DBMaster.getCorporate(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch);
            List<Corporates> stateList = new List<Corporates>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new Corporates();
                state.CorporateId = Convert.ToInt32(dr["CorporateId"]);
                state.CorporateName = dr["CorporateName"].ToString();
                state.PanNo = dr["PanNo"].ToString();
                state.WebSite = dr["WebSite"].ToString();
                state.Email = dr["Email"].ToString();
                state.CreatedByName = dr["UserName"].ToString();
                state.CorporateType = dr["CorporateType"].ToString();
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<Bank> getBank(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords,string userId)
        {
            
            DataTable dtLead = DBMaster.getBank(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch,userId);
            List<Bank> stateList = new List<Bank>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new Bank();
                state.BankId = Convert.ToInt32(dr["BankId"]);
                state.BankName = dr["BankName"].ToString();
                state.Address = dr["Address"].ToString();
                state.ContactNumber = dr["ContactNumber"].ToString();
                state.Email = dr["Email"].ToString();
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<Notification> getNotification(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords)
        {
            DataTable dtLead = DBMaster.getNotification(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch);
            List<Notification> stateList = new List<Notification>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new Notification();
                state.NotificationId = Convert.ToInt32(dr["NotificationId"]);
                state.NotificationType = dr["NotificationType"].ToString();
                state.NotificationText = dr["NotificationText"].ToString();
                state.NotificationDate = dr["NotificationDate"].ToString();              
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<UserEntity> getUser(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords)
        {
            DataTable dtLead = DBMaster.getUser(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch);
            List<UserEntity> stateList = new List<UserEntity>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new UserEntity();
                state.UserId = Convert.ToInt32(dr["UserId"]);
                state.UserName = dr["UserName"].ToString();
                state.Role = dr["Role"].ToString();          
            
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<UserEntity> getProduct(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords)
        {
            DataTable dtLead = DBMaster.getProduct(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch);
            List<UserEntity> stateList = new List<UserEntity>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new UserEntity();
                state.productId = Convert.ToInt32(dr["ProductId"]);
                state.productName = dr["ProductName"].ToString();
                state.UserName = dr["FirstName"].ToString();
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<UserEntity> getBankState(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords,string BankId)
        {
            DataTable dtLead = DBMaster.getBankState(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch,BankId);
            List<UserEntity> stateList = new List<UserEntity>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new UserEntity();
                state.productId = Convert.ToInt32(dr["CircleId"]);
                state.productName = dr["CircleName"].ToString();             
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<UserEntity> getBankCity(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string BankId)
        {
            DataTable dtLead = DBMaster.getBankCity(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, BankId);
            List<UserEntity> stateList = new List<UserEntity>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new UserEntity();
                state.productId = Convert.ToInt32(dr["ZoneId"]);
                state.productName = dr["ZoneName"].ToString();
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<UserEntity> getBankDistrict(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string BankId)
        {
            DataTable dtLead = DBMaster.getBankDistrict(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, BankId);
            List<UserEntity> stateList = new List<UserEntity>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new UserEntity();
                state.productId = Convert.ToInt32(dr["RegionId"]);
                state.productName = dr["RegionName"].ToString();
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<BankCorrespondence> getBcAllCorresponds(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords,string userId)
        {
            DataTable dtLead = DBMaster.getBcAllCorresponds(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, userId);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["BankCorrespondId"]);
                state.Name = dr["Name"].ToString();
                state.PhoneNumber1 = dr["PhoneNumber"].ToString();
                state.BCCode = dr["BCCode"].ToString();
                state.Qualification = dr["EducationalQualification"].ToString();
                if (dr["VillageCode"].ToString() == "")
                {
                    state.PLVillageDetail = dr["PLVillageDetail"].ToString();
                }
                else
                {
                    state.PLVillageDetail = dr["PLVillageDetail"].ToString() + "(" + dr["VillageCode"].ToString() + ")";
                }
               
                state.VillageCode = dr["VillageCode"].ToString();
                state.IsBlackListeds = dr["IsBlackListed"].ToString();

              
              
                
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }

        public static List<BankCorrespondence> getProspectiveBCRegistryList(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string userId)
        {
            DataTable dtLead = DBMaster.getProspectiveBCRegistryList(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, userId);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["ProspectiveBCRegistry"]);
                state.Name = dr["FirstName"].ToString();
                state.PhoneNumber1 = dr["PhoneNumber"].ToString();
                state.Qualification = dr["EducationalQualification"].ToString();

                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<Branch> GetBranch(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string userId, string BankId, string DistrictId, string iType)
        {
            DataTable dtLead = DBMaster.GetBranch(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, userId, BankId,  DistrictId,  iType);
            List<Branch> stateList = new List<Branch>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new Branch();
                state.BranchId = Convert.ToInt32(dr["BranchId"]);
                state.BranchName = dr["BranchName"].ToString();
                state.ContactNumber = dr["ContactNumber"].ToString();
                state.Address = dr["Address"].ToString();
                state.IFSCCode = dr["IFSCCode"].ToString();
             
                
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }

        public static List<BankCorrespondence> getFixedVsMbileBC(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string userId, string  states,string district,string  subdistrict,string village ,int type)
        {
            DataTable dtLead = DBMaster.getFixedVsMbileBC(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, userId, states, district, subdistrict, village,type);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {

               var state = new BankCorrespondence();
               state.State = dr["statename"].ToString();
                if(type==1)
                { 
                 state.District=dr["districtname"].ToString();
                }
                 if(type==2)
                 {
                     state.District = dr["districtname"].ToString();
                     state.Subdistrict = dr["subdistrictname"].ToString();
                 }
                if(type==3)
                {
                    state.District = dr["districtname"].ToString();
                    state.Subdistrict = dr["subdistrictname"].ToString();
                    state.Village = dr["villagename"].ToString();
                    
                }
                state.no_of_fixed = dr["no_of_fixed"].ToString();
                state.no_of_mobile = dr["no_of_Mobile"].ToString();
                state.no_of_both = dr["no_of_both"].ToString();
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<BankCorrespondence> getBCLocationWise(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string userId, string selState, string district,string subdistrict, string Village)
        {
            DataTable dtLead = DBMaster.getBCLocationWise(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, userId,selState,district,subdistrict, Village);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["BankCorrespondId"]);
                state.Name = dr["Name"].ToString();
                state.PhoneNumber1 = dr["PhoneNumber"].ToString();
                state.BCCode = dr["BCCode"].ToString();
                state.Qualification = dr["EducationalQualification"].ToString();
                state.PLVillageDetail = dr["PLVillageDetail"].ToString();
                state.IsBlackListeds = dr["IsBlackListed"].ToString();
                state.BCCode = dr["BCCode"].ToString();                
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<BankCorrespondence> searchState(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string stateid, string districtid, string subdistrictid)
        {
            DataTable dtLead = DBMaster.searchState(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, stateid, districtid, subdistrictid);
            List<BankCorrespondence> stateSearchList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var statesearch = new BankCorrespondence();
                statesearch.VillageCode = dr["VillageCode"].ToString();
                statesearch.VillageName = dr["VillageName"].ToString();
                statesearch.Subdistrict = dr["SubDistrictName"].ToString();
                statesearch.District = dr["DistrictName"].ToString();
                statesearch.State = dr["stateName"].ToString();
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateSearchList.Add(statesearch);
            }
            return stateSearchList;
        }
        public static List<BankCorrespondence> searchLog(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string bankId, string sheet, string date)
        {
            DataTable dtLog = DBMaster.searchLog(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, bankId, sheet, date);
            List<BankCorrespondence> logList = new List<BankCorrespondence>(dtLog.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLog.Rows)
            {
                var logsearch = new BankCorrespondence();

                logsearch.ExcelName = dr["ExcelName"].ToString();
                logsearch.Sheet = dr["sheetName"].ToString();
                logsearch.totalRecord = dr["totalRecord"].ToString();
                logsearch.UploadRecord = dr["UploadRecord"].ToString();
                int failed = Convert.ToInt32(dr["totalRecord"]) - Convert.ToInt32(dr["UploadRecord"]);
                logsearch.failedRecord = Convert.ToString(failed);
                logsearch.dateOfBirth = dr["createDate"].ToString();
                logsearch.userId = dr["userId"].ToString();
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                logList.Add(logsearch);
            }
            return logList;
        }
        public static List<BankCorrespondence> getBCbyCorporate(int iDisplayStart, int iDisplayLength, int iSortCol, string iSortDir, string sSearch, out int totalRecords, string userId, string corporateId, string stateid,string districtid, string subdistrictid ,string villageId)
        {
            DataTable dtLead = DBMaster.getBCbyCorporate(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, userId, corporateId, stateid,districtid,subdistrictid ,villageId);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["BankCorrespondId"]);
                state.Name = dr["Name"].ToString();
                state.PhoneNumber1 = dr["PhoneNumber"].ToString();
                state.AadharCard = dr["BCCODE"].ToString();
                state.Qualification = dr["EducationalQualification"].ToString();
                state.PLVillageDetail = dr["PLVillageDetail"].ToString();
                state.IsBlackListeds = dr["IsBlackListed"].ToString();

                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<BankCorrespondence> getBCByStatus(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string iType, string Status, string startDate, string endDate, string userId)
        {
            DataTable dtLead = DBMaster.getBCByStatus(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, iType, Status, startDate,endDate, userId);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["BankCorrespondId"]);
                state.Name = dr["Name"].ToString();
                state.PhoneNumber1 = dr["PhoneNumber"].ToString();
                state.BCCode = dr["BCCode"].ToString();
                state.Qualification = dr["EducationalQualification"].ToString();
                state.PLVillageDetail = dr["PLVillageDetail"].ToString();
                state.IsBlackListeds = dr["IsBlackListed"].ToString();
                
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }

        public static List<BankCorrespondence> getListAsOnDATE(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string startDate, string endDate, string userId)
        {
            DataTable dtLead = DBMaster.getListAsOnDATE(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, startDate, endDate, userId);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["BankCorrespondId"]);
                state.Name = dr["Name"].ToString();
                state.PhoneNumber1 = dr["PhoneNumber"].ToString();
                state.BCCode = dr["BCCode"].ToString();
                state.Qualification = dr["EducationalQualification"].ToString();
                state.PLVillageDetail = dr["PLVillageDetail"].ToString();
                state.IsBlackListeds = dr["IsBlackListed"].ToString();

                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<BankCorrespondence> getBcByBrnachFilter(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string bankId, string branchId, string userId,string statesId,string cityId,string districtId)
        {
            DataTable dtLead = DBMaster.getBcByBrnachFilter(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, bankId, branchId, userId, statesId, cityId, districtId);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["BankCorrespondId"]);
                state.Name = dr["Name"].ToString();
                state.PhoneNumber1 = dr["PhoneNumber"].ToString();
                state.BCCode = dr["BCCode"].ToString();
                state.Qualification = dr["EducationalQualification"].ToString();
                state.PLVillageDetail = dr["PLVillageDetail"].ToString();
                state.IsBlackListeds = dr["IsBlackListed"].ToString();
                
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        //public static List<BankCorrespondence> getBcByBankFilter(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string iType, string bankId, string branchId, string userId, string states, string city, string district)
        //{
        //    DataTable dtLead = DBMaster.getBcByBankFilter(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, iType, bankId, branchId, userId, states, city, district);
        //    List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
        //    totalRecords = 0;

        //    foreach (DataRow dr in dtLead.Rows)
        //    {
        //        var state = new BankCorrespondence();
        //        state.BankCorrespondId = Convert.ToInt32(dr["BankCorrespondId"]);
        //        state.Name = dr["Name"].ToString();
        //        state.PhoneNumber1 = dr["PhoneNumber"].ToString();
        //        state.BCCode = dr["BCCode"].ToString();
        //        state.Qualification = dr["EducationalQualification"].ToString();
        //        state.PLVillageDetail = dr["PLVillageDetail"].ToString();
        //        state.IsBlackListeds = dr["IsBlackListed"].ToString();

        //        totalRecords = Convert.ToInt32(dr["RowCnt"]);
        //        stateList.Add(state);
        //    }
        //    return stateList;
        //}

        public static List<BankCorrespondence> getBlackListedBc(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch,out int totalRecords, string bankid, string userId)
        {
            DataTable dtLead = DBMaster.getBlackListedBc( iDisplayStart,  iDisplayLength,  sortColumn,  sortDirection,  sSearch,   bankid,  userId);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["BankCorrespondId"]);
                state.Name = dr["Name"].ToString();
                state.BlacklistedDate = dr["BlackListedDate"].ToString();
                state.BlackListReason = dr["BlackListedReason"].ToString();            
                
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<BCProducts> getbcProduct(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string productId,string userid)
        {
            DataTable dtLead = DBMaster.getbcProduct(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, productId, userid);
            List<BCProducts> stateList = new List<BCProducts>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BCProducts();
                state.ProductId = Convert.ToInt32(dr["productId"]);
                state.ProductName = dr["productName"].ToString();
                state.BCId = Convert.ToInt32(dr["bccode"]);
                state.BcName = dr["Name"].ToString();
            
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }

        public static List<BankCorrespondence> getallocationbased(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, string StartDate,string EndDate,string UserId)
        {
            DataTable dtLead = DBMaster.getallocationbased(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch,   StartDate, EndDate, UserId);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["BankCorrespondId"]);
                state.Name = dr["Name"].ToString();
                state.BlacklistedDate = dr["CurrentStatusSince"].ToString();
                state.BlackListReason = dr["BankName"].ToString();            
                
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }

        public static List<BankCorrespondence> getBranchLastupdated(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, int iType, int bankId, string createdate)
        {
            DataTable dtLead = DBMaster.getBranchLastupdated(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, iType, bankId, createdate);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["BranchId"]);
                state.Name = dr["BranchName"].ToString();
                state.BlacklistedDate = dr["UpdatedOn"].ToString();              
                
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }

        public static List<BankCorrespondence> getAreaWiseAllocaiton(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords, int iType, int bankId, string createdate)
        {
            DataTable dtLead = DBMaster.getAreaWiseAllocaiton(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, iType, bankId, createdate);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["AreaId"]);
                state.Name = dr["BankCorrespondId"].ToString();
                state.BlacklistedDate = dr["CreatedOn"].ToString();                    
                
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        public static List<BankCorrespondence> getUnallocated(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, out int totalRecords,string userId)
        {
            DataTable dtLead = DBMaster.getUnallocated(iDisplayStart, iDisplayLength, sortColumn, sortDirection, sSearch, userId);
            List<BankCorrespondence> stateList = new List<BankCorrespondence>(dtLead.Rows.Count);
            totalRecords = 0;

            foreach (DataRow dr in dtLead.Rows)
            {
                var state = new BankCorrespondence();
                state.BankCorrespondId = Convert.ToInt32(dr["BankCorrespondId"]);
                state.Name = dr["Name"].ToString();
                state.PhoneNumber1 = dr["PhoneNumber"].ToString();
                state.BCCode = dr["BCCode"].ToString();
                state.Qualification = dr["EducationalQualification"].ToString();
                state.PLVillageDetail = dr["PLVillageDetail"].ToString();
                state.IsBlackListeds = dr["IsBlackListed"].ToString();
                
                totalRecords = Convert.ToInt32(dr["RowCnt"]);
                stateList.Add(state);
            }
            return stateList;
        }
        
    }
}
