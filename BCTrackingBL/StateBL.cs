﻿using BCEntities;
using BCTrackingDAL;
using System.Collections.Generic;

namespace BCTrackingBL
{
    public class StateBL
    {
        public List<State> GetStates(int? stateId) {
            SQLDataProvider dataProvider = new SQLDataProvider();
            return dataProvider.GetStates(stateId);
        }

        public int InsertUpdateState(State s, string operationMode, int? userId) {
            SQLDataProvider dataProvider = new SQLDataProvider();
            return dataProvider.InsertUpdateState(s, operationMode, userId);
        }
         public int InsertUpdateStates(BankStates state, string mode, int userId)
        {
            SQLDataProvider dataProvider = new SQLDataProvider();
            return dataProvider.AddState(state, mode, userId);
        }
         public int InsertUpdateBankCity(BankCity state, string mode, int userId)
         {
             SQLDataProvider dataProvider = new SQLDataProvider();
             return dataProvider.InsertUpdateBankCity(state, mode, userId);
         }
         public int InsertUpdateBankDistrict(DistrictList state, string mode, int userId)
         {
             SQLDataProvider dataProvider = new SQLDataProvider();
             return dataProvider.InsertUpdateBankDistrict(state, mode, userId);
         }
        
    }
    
}
