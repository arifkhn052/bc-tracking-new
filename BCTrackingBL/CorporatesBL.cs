﻿using BCTrackingDAL;
using System.Collections.Generic;
using BCEntities;

namespace BCTrackingBL
{
    public class CorporatesBL
    {
        SQLDataProvider provider;
        public List<Corporates> GetCorporates(int? corporateId) {
            provider = new SQLDataProvider();
            return provider.GetCorporates(corporateId);
        }

        public int InsertUpdateCorporates(Corporates corporate, string mode, int userId) {
            provider = new SQLDataProvider();
            return provider.InsertUpdateCorporates(corporate, mode, userId);
        }
    }
}
