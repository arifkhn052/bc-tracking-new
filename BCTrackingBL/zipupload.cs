﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System.Configuration;
using BCTrackingBL;
using BCEntities;
using BCTrackingServices;
using System.Text;
using Newtonsoft.Json;
using System.Collections;
using System.IO.Compression;
using Microsoft.Azure; //Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks; // Namespace for Blob storage types

namespace BCTrackingBL
{
    public class zipupload
    {
        public void uploadzip(string StoreBlob, string UserId, string blobName)
       {
             string FolderName;
           UserBL businessLogic = new UserBL();
           CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
           CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
           CloudBlobContainer container = blobClient.GetContainerReference("dev-raw");         
         
        

           // Retrieve reference to a blob named "myblob".
           CloudBlockBlob blockBlobs = container.GetBlockBlobReference(StoreBlob);
           MemoryStream blobms = new MemoryStream();
           blockBlobs.DownloadToStream(blobms);
           using (var zip = new ZipArchive(blobms))
           {
               //Each entry here represents an individual file or a folder

               foreach (var entry in zip.Entries)
               {
                   businessLogic.ZipStatusChange(UserId.ToString(), 1);
                   if (entry.Name == "")
                   {
                       FolderName = entry.FullName;
                   }
                   else
                   {

                       string fileName = entry.Name;
                       string[] strArray = fileName.Split('_');
                       string BCCode = strArray[0].ToString();
                       string fileType = strArray[1].ToString();
                       fileType = fileType.Substring(0, 5);

                       BankCorrespondentBL bc = new BankCorrespondentBL();
                       bc.SaveZipPath("", fileName, "", BCCode, fileType, UserId.ToString());

                       using (var stream = entry.Open())
                       {

                           CloudBlobContainer container11 = blobClient.GetContainerReference("dev-photos");
                           CloudBlockBlob blockBlob1 = container11.GetBlockBlobReference(fileName);                       
                           blockBlob1.UploadFromStream(stream);


                       }
                   }
               }
               businessLogic.ZipStatusChange(UserId.ToString(), 2);


           }

        }
    }
}
