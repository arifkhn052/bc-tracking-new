﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCTrackingDAL;
using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using DocumentFormat.OpenXml.Office2013.Drawing.ChartStyle;

namespace BCTrackingBL
{
    public class BankBL
    {
        SQLDataProvider provider;
        
        /// <summary>
        /// Perform operations on the given bankEntity on the DB.
        /// </summary>
        /// <param name="bankEntity">Information about the Bank being added to or updated in the DB</param>
        /// <param name="operationMode">The operation that we want the system to do</param>
        /// <param name="userId">Who is doing this operation</param>
        /// <returns></returns>
        public int InsertUpdateBank(Bank bankEntity, string operationMode, int userId)
        {
            int bankId = Constants.DEFAULTVALUE;
            try
            {
                provider = new SQLDataProvider();
                bankId = provider.InsertUpdateBanks(bankEntity, operationMode, userId);
                Utils.SetSessionValue(Constants.BANKSESSIONVAR, null);
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BankBL.InsertUpdateBank: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return bankId;
        }
        public int insertUpdateGroupProduct(ProductGroupName ProductGroupName, ProductGroup ProductGroup, int userId)
        {
            int bankId = Constants.DEFAULTVALUE;
            try
            {
                provider = new SQLDataProvider();
                bankId = provider.insertUpdateGroupProduct(ProductGroupName, ProductGroup, userId);
                Utils.SetSessionValue(Constants.BANKSESSIONVAR, null);
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BankBL.InsertUpdateBank: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return bankId;
        }

        public int InsertUpdateBrnach(Branch branchEntity, string operationMode, int userId)
        {
            int bankId = Constants.DEFAULTVALUE;
            try
            {
                provider = new SQLDataProvider();
                bankId = provider.InsertUpdateBrnach(branchEntity, userId, operationMode);
                Utils.SetSessionValue(Constants.BANKSESSIONVAR, null);
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BankBL.InsertUpdateBank: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return bankId;
        }
        public List<Bank> GetBanks(int? bankId,string userId)
        {
            provider = new SQLDataProvider();
            List<Bank> allBanks = null;
            allBanks = Utils.GetSessionValue(Constants.BANKSESSIONVAR) as List<Bank>;
            if (allBanks == null)
            {
                allBanks = provider.GetBanks(null, userId);
                Utils.SetSessionValue(Constants.BANKSESSIONVAR, allBanks);
            }

            return provider.GetBanks(bankId, userId);

        }

      


    }
}
