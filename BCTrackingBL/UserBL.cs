﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCEntities;
using BCTrackingDAL;
using System.Data;

namespace BCTrackingBL
{
    public class UserBL
    {
        public UserEntity AuthenticateUser(string userName, string password)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetAuthenticatedUser(userName, password);
        }
        public int UpdatePassword(string oldPassword, string newPassword, string userId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.UpdatePassword(oldPassword, newPassword, userId);
        }
        public List<ListAadhar> getCheckExistAdharno(string adharno)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getCheckExistAdharno(adharno);
        }
        public string getCheckbccode(string bccode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getCheckbccode(bccode);
        }
        public string getCheckAadharno(string adharno)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getCheckAadharno(adharno);
        }
         public string allocateBC(string bankid,string bcId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.allocateBC(bankid, bcId);
        }
      
        public int terminateBC(string adhaarNo, DateTime DOB, string reason,int userid)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.terminateBC(adhaarNo, DOB, reason, userid);
        }
       
        public UserEntity AuthenticateUserData(string userName)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetAuthenticatedUserData(userName);
        }

        public List<UserEntity> GetUsers(int? userId=null)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetUsers(userId);
        }
        public List<UserEntity> getUserMenu(int userId, string username)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getUserMenu(userId, username);
        }
        public List<UserEntity> getProducts(int productId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getProducts(productId);
        }
        public List<UserEntity> getUserProducts()
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getUserProducts();
        }

        public List<ProductGroupName> getGroupUserProducts()
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getGroupUserProducts();
        }
        public List<ProductGroupName> getGroupProducts(string groupId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getGroupProducts(groupId);
        }

        
        public List<Institute> getInstitue()
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getInstitue();
        }
        public List<Coursess> getCouse()
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getCouse();
        }
        public List<Branch> getBranch(string userId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getBranch(userId);
        }
        
        
        public int InsertUpdateUser(UserEntity users, string mode, int userId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.InsertUpdateUSers(users, mode, userId);
        }
        public string createToken(string userId,string password,string tokenId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.createToken(userId, password, tokenId);
        }
        public int getToken(string userId, string tokenid)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getToken(userId, tokenid);           
        }
        public int getBcValidate(string name,string DOB,string FatherName,string Category,string phn_NO)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getBcValidate(name, DOB, FatherName, Category, phn_NO);
        }
        public int getZipStatus(string userId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getZipStatus(userId);

        }
        public int getAadhaar(string adhaar)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getAadhaar(adhaar);
        }
        public int getBankReferenceNumber(string BankReferenceNumber)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getBankReferenceNumber(BankReferenceNumber);
        }
        public int getProductId(string ProductId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getProductId(ProductId);
        }
        
        public int getCorporate(string corporateid)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getCorporate(corporateid);
        }
        public int DeleteError(string UserId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.DeleteError(UserId);
        }
        public int fileStatus(string UserId, int status, string excelName)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.fileStatus(UserId, status, excelName);
        }
        public int zipStatus(string UserId, int status, string blobName)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.zipStatus(UserId, status, blobName);
        }
        public int fileStatusChange(string UserId, int status)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.fileStatusChange(UserId, status);
        }
        public int ZipStatusChange(string UserId, int status)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.ZipStatusChange(UserId, status);
        }
        
        public int SaveError(string UserId,string status)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.SaveError(UserId, status);
        }

     
        public string getStateDistrictIds(string VillageCode)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getStateDistrictIds(VillageCode);
        }
        public string getBankId(string VillageCode,string BankId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getBankId(VillageCode, BankId);
        }
        public string getBankIdForPreviousexp(string VillageCode, string BankId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getBankIdForPreviousexp(VillageCode, BankId);
        }
        
        public int GetAdharNo(string adhaar)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.GetAdharNo(adhaar);
        }
        public int getUserAviable(string userId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.getUserAviable(userId);


        }
        public int logout(string userId, string tokenid)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.logout(userId, tokenid);


        }
        public int insertUpdateProduct(UserEntity users, string mode, int userId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            return provider.insertUpdateProduct(users, mode, userId);
        }
        public void DeleteUsers(int userId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            provider.DeleteUsers(userId);
        }
        public void deleteProduct(int userId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            provider.deleteProduct(userId);
        }
        public void deleteGroupProduct(int userId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            provider.deleteGroupProduct(userId);
        }

        public void deleteBC(int userId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            provider.deleteBC(userId);
        }
        
        public void deleteCorporate(int corporateId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            provider.deleteCorporate(corporateId);
        }

        public void DeleteBank(int bankId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            provider.DeleteBank(bankId);
        }
        public void deleteState(int stateId)
        {
            SQLDataProvider provider;
            provider = new SQLDataProvider();
            provider.deleteState(stateId);
        }
        public List<State> GetAllStates()
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetAllStates();
        }
        
       public DataTable GetReportData(string stateId, string district, string subdistrict, string village, string type,string startDate,string endDate)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetReportData(stateId, district, subdistrict, village, type,startDate,endDate);
        }
        public List<District> GetDistrictById(string stateId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetDistrictById(stateId);
        }
        public List<BankCorrespondence> GetBCListByLatLong(string latitude, string longitude)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetBCListByLatLong(latitude, longitude);
        }
        public List<SubDistrict> GetSubDistrictById(string stateId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetSubDistrictById(stateId);
        }
        public List<Village> GetVillageBySubDistrictId(string stateId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetVillageBySubDistrictId(stateId);
        }

        


        
    }
}
