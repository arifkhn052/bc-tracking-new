﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bctrackingproject
{
    public class Address
    {
        public int Addressid { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string Subdistrict { get; set; }
        public string Area { get; set; }
        public string PinCode{ get; set; }


    }
}