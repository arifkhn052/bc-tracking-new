﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class BCProducts
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string BcName{ get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public int BCId { get; set; }
    }
    public class Products
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
      
    }
}
