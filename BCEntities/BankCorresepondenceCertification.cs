﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BCEntities
{
    public class BankCorresepondenceCertification
    {

        public DateTime DateOfPassing { get; set; }
        public string InstituteName { get; set; }
        public string Course { get; set; }
        public double Grade { get; set; }
    }
}
