﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BCEntities
{
    public class EducationQualifications
    {

        public int Id { get; set; }
        public string CourseName { get; set; }
        public DateTime YearOfPassing { get; set; }
        public decimal Percentage { get; set; }
        public string Grade{ get; set; }
        public string Institute { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
    }
}
