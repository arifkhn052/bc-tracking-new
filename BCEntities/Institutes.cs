﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class Institutes
    {
        public int InstituteId { get; set; }
        public string InstituteName { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public List<Courses> Courses { get; set; }
    }
    public class Institute
    {    
        public string InstituteName { get; set; }
      
    }
    public class Coursess
    {
        public string Course { get; set; }

    }

}
