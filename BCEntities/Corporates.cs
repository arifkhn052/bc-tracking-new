﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class Corporates
    {
        public int CorporateId { get; set; }
        public string CorporateName { get; set; }
        public string PanNo { get; set; }
        public string WebSite { get; set; }
        public string CreatedByName { get; set; }
        
        public string CorporateType { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public int BankCorrespondenceId { get; set; }
        public string ContactPerson { get; set; }
        public string ContactDesignation { get; set; }
        public int NoOfComplaint { get; set; }
    }
}
