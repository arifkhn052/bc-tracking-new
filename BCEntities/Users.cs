﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class UserEntity
    {
        public int UserId { get; set; }
        public int productId { get; set; }
        public string Role { get; set; }
        public string UserName { get; set; }

        public string RoleId { get; set; }
        public string menuState { get; set; }
        public string menuName { get; set; }
        public string menuIcon { get; set; }
        public string menulavel { get; set; }
        public int refid { get; set; }
        public int menuId { get; set; }
        public string groupProductId { get; set; }
        public string groupProductName { get; set; }

        public string productName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int BankId { get; set; }
        public string UserPassword { get; set; }
        public string state { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public List<RoleEntity> UserRoles { get; set; }
    }
    public class ProductGroupName
     {
        public int id { get; set; }
         public string GroupName { get; set; }
         public List<ProductGroup> ProductGrouplist { get; set; }
      

     }
    public class ProductGroup
     {
        public string GroupId { get; set; }
         public string ProductId { get; set; }
         public string ProductName { get; set; }
        

     }
}


