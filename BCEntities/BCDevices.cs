﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class BCDevices
    {
        public int DeviceId { get; set; }
        public string Device { get; set; }
        public string DeviceCode { get; set; }
        public string AadharCard { get; set; }
        public DateTime? GivenOn { get; set; }
        public string Given_On { get; set; }
        public int BCId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
    }
}
