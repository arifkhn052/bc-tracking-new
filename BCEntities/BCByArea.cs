﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class BCByArea
    {
        public string VillageCode { get; set; }
        public int BCCount { get; set; }
        public DateTime LastUpdateDate { get; set; }
    }
}
