﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace BCEntities
{
    public class BankCorrespondence
    {
        
        public int BankCorrespondId { get; set; }
        public string BCCode { get; set; }
        public string Name { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string referencePhoneNumber { get; set; }
        public string ImagePath { get; set; }
        public string Gender { get; set; }
        public DateTime? DOB{ get; set; }

        public string dateOfBirth { get; set; }
        public string FatherName { get; set; }
        public string SpouseName { get; set; }
        public string Category { get; set; }
        public string Handicap { get; set; }
        public string PhoneNumber1 { get; set; }
        public string BankName { get; set; }
        public string PhoneNumber2 { get; set; }
        public string PhoneNumber3 { get; set; }

        public string ContactPerson { get; set; }
        public string ContactDesignation { get; set; }
        public string NoofComplaint { get; set; }

        public string Email { get; set; }
        public string AadharCard { get; set; }
        public string PanCard { get; set; }
        public string VoterCard { get; set; }
        public string DriverLicense{ get; set; }
        public string NregaCard{ get; set; }
        public string RationCard { get; set; }

        public string State { get; set; }
        public string City { get; set; }
        public string Village { get; set; }        
        public string District { get; set; }
        public string Subdistrict { get; set; }
        public string productName { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public string Area { get; set; }
        public string PinCode { get; set; }
        public string AlternateOccupationType { get; set; }
        public string AlternateOccupationDetail { get; set; }
        public string UniqueIdentificationNumber { get; set; }
        public string BankReferenceNumber { get; set; }
        public string Qualification { get; set; }
        public string OtherQualification { get; set; }
        public Corporates Corporate { get; set; }
        public int CorporateId { get; set; }
        public bool isAllocated { get; set; }
        public DateTime? AppointmentDate { get; set; }

        public string Appointment_Date { get; set; }
        public string AllocationIFSCCode { get; set; }
        public int AllocationBankId { get; set; }
        public int AllocationBranchId { get; set; }
        public List<SsaDetails> SsaDetails { get; set; }
        public List<CommssionList> CommssionList { get; set; }
        public string BCType { get; set; }
        public int? WorkingDays { get; set; }
        public int? WorkingHours { get; set; }
        public int? terminateBc { get; set; }

        public string PLPostalAddress { get; set; }
        public string PLVillageCode { get; set; }
        public string PLVillageDetail { get; set; }
        public string PLTaluk { get; set; }
        public int PLDistrictId { get; set; }
        public int? PLStateId { get; set; }
        public string PLPinCode { get; set; }

        public string productId { get; set; }

        public double MinimumCashHandlingLimit { get; set; }
        public double MonthlyFixedRenumeration { get; set; }
        public double MonthlyVariableRenumeration { get; set; }


        public List<BCCertifications> Certifications { get; set; }
        public List<BCDevices> Devices { get; set; }
        public List<PreviousExperiene> PreviousExperience { get; set; }
        public List<ConnectivityDetails> ConnectivityDetails { get; set; }
        public List<OperationalAreas> OperationalAreas { get; set; }

      


        public List<BCProducts> Products { get; set; }
   
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public DateTime CurrentStatusSince { get; set; }
        public string Status { get; set; }
        public string StatusChangeReason { get; set; }

        public bool IsBlackListed { get; set; }
        public string IsBlackListeds { get; set; }
        public DateTime BlacklistDate { get; set; }
        public string BlacklistedDate { get; set; }
        public string BlackListReason { get; set; }
        public string BlackListApprovalNumber { get; set; }

        public int? StateId { get; set; }
        public int? DistrictId { get; set; }
        public int? SubDistrictId { get; set; }
        public string VillageCode { get; set; }
        public string VillageName { get; set; }
        public string Sheet { get; set; }
        public string ExcelName { get; set; }
        public string totalRecord { get; set; }
        public string UploadRecord { get; set; }
         public string failedRecord { get; set; }
        public string userId { get; set; }
        public string no_of_fixed { get; set; }
        public string no_of_mobile { get; set; }
        public string no_of_both { get; set; }
    }

    public class ListAadhar
    {
        public string Name { get; set; }
        public string Email { get; set; }
       

    }
}