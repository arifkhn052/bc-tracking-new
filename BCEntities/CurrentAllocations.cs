﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class CurrentAllocations
    {
        public int AllocationId { get; set; }
        public int BankId { get; set; }
        public int BranchId { get; set; }
        public DateTime AppointmentDate { get; set; }
        public int BankCorrespondenceId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public string BCType { get; set; }
        public int WorkingDays { get; set; }
        public int WorkingHours { get; set; }
        public string PLPostalAddress { get; set; }
        public string PLVillageCode { get; set; }
        public string PLVillageDetail { get; set; }
        public string PLTaluk { get; set; }
        public string PLDistrict { get; set; }
        public string PLState { get; set; }
        public string PLPinCode { get; set; }
        public List<ConnectivityDetails> ConnectivityProviders { get; set; }
        public decimal MinimumCashHandlingLimit { get; set; }
        public decimal MonthlyFixedRenumeration { get; set; }
        public decimal MonthlyVariableRenumeration { get; set; }
    }
}
