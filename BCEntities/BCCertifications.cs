﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BCEntities
{
    public class BCCertifications
    {
        public int CertificationId { get; set; }
        public DateTime DateOfPassing { get; set; }
        public string DateOf_Passing { get; set; }        
        public int InstituteId { get; set; }
        public string InstituteName { get; set; }

        public Institutes Institutes { get; set; }
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public Courses Courses { get; set; }
        public string Grade { get; set; }
        public string AadharCard { get; set; }
        public int BankCorresponenceId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
    }
}
