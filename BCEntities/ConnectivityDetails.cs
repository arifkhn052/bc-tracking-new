﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class ConnectivityDetails
    {
        public int ConnectivityId { get; set; }
        public string ConnectivityMode { get; set; }
        public string ConnectivityProvider { get; set; }
        public string ContactNumber { get; set; }
        public string AadharCard { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public int BCId { get; set; }
    }
    public class SsaDetails
    {

        public string bcid { get; set; }
        public string AadharCard { get; set; }
        public string Ssa { get; set; }
        public string State { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public string DistrictName { get; set; }
        public string DistrictCode { get; set; } 
        public string SubDistrictName { get; set; }
        public string SubDistrictCode { get; set; }        
        public string District { get; set; }
        public string SubDistrict { get; set; }
        public string Village { get; set; }

        
    }

    public class CommssionList
    {

        public string Month { get; set; }
        public string Year { get; set; }
        public string Commission1 { get; set; }
        public string Commission2 { get; set; }
        public string MonthName { get; set; }
        public string BankRefNo { get; set; }
      

    }
}

