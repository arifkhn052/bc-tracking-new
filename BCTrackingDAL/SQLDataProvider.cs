﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCEntities;
using BCTrackingServices;
using System.Data;
using System.Data.SqlClient;

namespace BCTrackingDAL
{
    public class SQLDataProvider
    {
        private SqlConnection conn = null;
        public int insertUpdateGroupProduct(ProductGroupName ProductGroupName, ProductGroup ProductGroup, int userId)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParams2 = new SpParamCollection();
          

            try
            {
                spParams.Add(new SpParam("@GroupId", ProductGroupName.id));
                spParams.Add(new SpParam("@GroupName", ProductGroupName.GroupName));
                spParams.Add(new SpParam("@UserId", userId));
                object newID = DBHelper.ExecProcScalar("SP_ProductGroupName", spParams);
                retVal = int.Parse(newID.ToString());


                spParams2.Add(new SpParam("@groupId", retVal));
                object newIDss = DBHelper.ExecProcScalar("SP_DELETEProductGroup", spParams2);
              

                List<ProductGroup> ProductGrp = ProductGroupName.ProductGrouplist;
                ProductGroup cr = new ProductGroup();
                for (int i = 0; i < ProductGrp.Count; i++)
                {
                    SpParamCollection spParams1 = new SpParamCollection();
                    spParams1.Add(new SpParam("@GroupId", retVal));
                    spParams1.Add(new SpParam("@ProductId", ProductGrp[i].ProductId));
                    spParams1.Add(new SpParam("@ProductName", ProductGrp[i].ProductName));
                    spParams1.Add(new SpParam("@UserId", userId));
                    object newIDa = DBHelper.ExecProcScalar("SP_ProductGroup", spParams1);
                    retVal = int.Parse(newID.ToString());

                }

            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdatesCorporates: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        public int AddState(BankStates state, string mode, int userId)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                if (mode == Constants.UPDATEMODE)
                {
                    spParams.Add(new SpParam("@bankid", state.bankid));
                }
                else
                {
                    spParams.Add(new SpParam("@bankid", 0));
                }

                spParams.Add(new SpParam("@StateName", state.StateName));
                spParams.Add(new SpParam("@UserId", userId));
                object newID = DBHelper.ExecProcScalar("SPCreateBankState", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdatesCorporates: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        public int InsertUpdateBankCity(BankCity state, string mode, int userId)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {


                spParams.Add(new SpParam("@StateId", state.StateId));
                spParams.Add(new SpParam("@CityName", state.CityName));
                spParams.Add(new SpParam("@UserId", userId));
                object newID = DBHelper.ExecProcScalar("spCreateBankCity", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdatesCorporates: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        public int InsertUpdateBankDistrict(DistrictList state, string mode, int userId)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {


                spParams.Add(new SpParam("@CityId", state.CityId));
                spParams.Add(new SpParam("@DistrictName", state.DistrictName));
                spParams.Add(new SpParam("@UserId", userId));
                object newID = DBHelper.ExecProcScalar("spCreateBankDistrict", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdatesCorporates: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        

        
        public UserEntity GetAuthenticatedUser(string userName, string password)
        {
            UserEntity user = null;

            try
            {
                DataSet dsUser = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@UserName", userName));
                spParams.Add(new SpParam("@password", password));

                DBHelper.ExecProcAndFillDataSet("AuthenticateUser", spParams, dsUser, conn);

                if (dsUser != null && dsUser.Tables.Count > 1)
                {
                    //////////////////////////// Pages///////////////////

                    DataTable dtUser = dsUser.Tables[0];
                    if (dtUser.Rows.Count > 0)
                    {
                        DataRow drUser = dtUser.Rows[0];

                        user = new UserEntity();
                        user.UserId = Utils.GetIntValue(drUser["UserId"]);
                        user.FirstName = Utils.GetStringValue(drUser["FirstName"]);
                        user.MiddleName = Utils.GetStringValue(drUser["MiddleName"]);
                        user.LastName = Utils.GetStringValue(drUser["LastName"]);
                        user.UserName = Utils.GetStringValue(drUser["UserName"]);


                    }

                    DataTable dtRoles = dsUser.Tables[1];
                    if (user != null)
                    {
                        if (dtRoles.Rows.Count > 0)
                        {
                            foreach (DataRow drRole in dtRoles.Rows)
                            {
                                RoleEntity role = new RoleEntity();
                                role.RoleId = Utils.GetIntValue(drRole["RoleId"]);
                                role.Role = Utils.GetStringValue(drRole["Role"]);

                                if (user.UserRoles == null)
                                    user.UserRoles = new List<RoleEntity>();

                                user.UserRoles.Add(role);
                            }


                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return user;
        }
        public int UpdatePassword(string oldPassword, string newPassword, string username)
        {
            int retVal=-1;

            try
            {
                SqlConnection conn = new SqlConnection(Constants.connString);
                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@oldPassword", oldPassword));
                spParams.Add(new SpParam("@newPassword", newPassword));
                spParams.Add(new SpParam("@username", username));

                object newID = DBHelper.ExecProcScalar("SPUpdatePassword", spParams);
                retVal = int.Parse(newID.ToString());

            }
            catch (Exception ex)
            {
                throw ex;

            }

            return retVal;
        }
        public UserEntity GetAuthenticatedUserData(string userName)
        {
            UserEntity user = null;

            try
            {
                DataSet dsUser = new DataSet();
                SqlConnection conn = new SqlConnection(Constants.connString);
                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@UserName", userName));
                DBHelper.ExecProcAndFillDataSet("getAuthenticateUserData", spParams, dsUser, conn);
                if (dsUser != null && dsUser.Tables.Count > 1)
                {
                    //////////////////////////// Pages///////////////////

                    DataTable dtUser = dsUser.Tables[0];
                    if (dtUser.Rows.Count > 0)
                    {
                        DataRow drUser = dtUser.Rows[0];

                        user = new UserEntity();
                        user.UserId = Utils.GetIntValue(drUser["UserId"]);
                        user.FirstName = Utils.GetStringValue(drUser["FirstName"]);
                        user.MiddleName = Utils.GetStringValue(drUser["MiddleName"]);
                        user.LastName = Utils.GetStringValue(drUser["LastName"]);
                        user.UserName = Utils.GetStringValue(drUser["UserName"]);
                        user.BankId = Utils.GetIntValue(drUser["BankId"]);


                    }

                    DataTable dtRoles = dsUser.Tables[1];
                    if (user != null)
                    {
                        if (dtRoles.Rows.Count > 0)
                        {
                            foreach (DataRow drRole in dtRoles.Rows)
                            {
                                RoleEntity role = new RoleEntity();
                                role.RoleId = Utils.GetIntValue(drRole["RoleId"]);
                                role.Role = Utils.GetStringValue(drRole["Role"]);

                                if (user.UserRoles == null)
                                    user.UserRoles = new List<RoleEntity>();

                                user.UserRoles.Add(role);
                            }


                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return user;
        }

        public int terminateBC(string adhaarNo, DateTime dateOfTerminate,string reason, int userid)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@adhaarNo", adhaarNo));
                spParams.Add(new SpParam("@dateOfTerminate", dateOfTerminate));
                spParams.Add(new SpParam("@reason", reason));
                spParams.Add(new SpParam("@userid", userid));                
                object newID = DBHelper.ExecProcScalar("spTerminateBc", spParams);
                retVal = int.Parse(newID.ToString());
                if (retVal == 1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }

        }
        public int getToken(string userId, string TokenId)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@tokenId", TokenId));
                object newID = DBHelper.ExecProcScalar("spGetToken", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }

        public int getBcValidate(string name, string DOB, string FatherName, string Category, string phn_NO)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@name", name));
                spParams.Add(new SpParam("@DOB", DOB));
                spParams.Add(new SpParam("@FatherName", FatherName));
                spParams.Add(new SpParam("@Category", Category));
                spParams.Add(new SpParam("@phn_NO", phn_NO));
                object newID = DBHelper.ExecProcScalar("SP_GETBCVALIDATE", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }
        
        public int getZipStatus(string userId)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@userId", userId));
                object newID = DBHelper.ExecProcScalar("spGetZip", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }

        

        public DataTable GetReportData(string stateId, string district, string subdistrict, string village, string type, string startDate, string endDate)
        {
            DataSet ds = new DataSet();
          //  SqlDataAdapter da;
            conn = new SqlConnection(Constants.connString);
            SpParamCollection spParams = new SpParamCollection();
            DataSet dsUsers = new DataSet();
            try
            {

                spParams.Add(new SpParam("@reportType", Convert.ToInt32(type)));
                spParams.Add(new SpParam("@startDate", startDate));
                spParams.Add(new SpParam("@endDate", endDate));
                spParams.Add(new SpParam("@state", stateId));
                spParams.Add(new SpParam("@district", district));
                spParams.Add(new SpParam("@subdistrict", subdistrict));
                spParams.Add(new SpParam("@village", village));
                DBHelper.ExecProcAndFillDataSet("sp_getAllocatedBCForReport", spParams, ds, conn);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int getAadhaar(string adhaar)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@AadharCard", adhaar));
                object newID = DBHelper.ExecProcScalar("spGetAdhaar", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }
        public int getBankReferenceNumber(string BankReferenceNumber)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@BankReferenceNumber", BankReferenceNumber));
                object newID = DBHelper.ExecProcScalar("spGetBankReferenceNumber", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }
        public int getProductId(string ProductId)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@ProductId", ProductId));
                object newID = DBHelper.ExecProcScalar("spgetProductId", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }
        
        
        public int getCorporate(string adhaar)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@CorporateId", adhaar));
                object newID = DBHelper.ExecProcScalar("spGetCorporateId", spParams);
                retVal = int.Parse(newID.ToString());
                if (retVal == 1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }

        }
        public int DeleteError(string UserId)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@UserId", UserId));
                object newID = DBHelper.ExecProcScalar("spDeleteError", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }

        public int SaveError(string UserId,string Error)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@UserId", UserId));
                spParams.Add(new SpParam("@Error", Error));
                object newID = DBHelper.ExecProcScalar("spSaveError", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }

        public int fileStatus(string UserId, int status, string excelName)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@UserId", UserId));
                spParams.Add(new SpParam("@status", status));
                spParams.Add(new SpParam("@excelName", excelName));                
                object newID = DBHelper.ExecProcScalar("spFileStatus", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }

        public int zipStatus(string UserId, int status,string blobName)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@UserId", UserId));
                spParams.Add(new SpParam("@status", status));
                spParams.Add(new SpParam("@zipName", blobName));
                object newID = DBHelper.ExecProcScalar("spzipStatus", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }

        
        public int fileStatusChange(string UserId, int status)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@UserId", UserId));
                spParams.Add(new SpParam("@status", status));
                object newID = DBHelper.ExecProcScalar("spFileChateStatus", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }

        public int ZipStatusChange(string UserId, int status)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@UserId", UserId));
                spParams.Add(new SpParam("@status", status));
                object newID = DBHelper.ExecProcScalar("spZipChateStatus", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }
        
        

        public string getStateDistrictIds(string VillageCode)
        {
            string retVal;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@VillageCode", VillageCode));
                object newID = DBHelper.ExecProcScalar("spGetStateDistrictIds", spParams);
                retVal = newID.ToString();
                return retVal;
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }

        public string getBankId(string branchCode, string BankId)
        {
           
            string retVal;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@BranchCode", branchCode));
                spParams.Add(new SpParam("@Bankids", BankId));
                object newID = DBHelper.ExecProcScalar("spGetBankId", spParams);
                retVal = newID.ToString();
                return retVal;
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }

        public string getBankIdForPreviousexp(string branchCode, string BankId)
        {
            //------------- for previous exp---------------
            string retVal;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@BranchCode", branchCode));
                spParams.Add(new SpParam("@Bankids", BankId));
                object newID = DBHelper.ExecProcScalar("spGetBankIdPreviousexp", spParams);
                retVal = newID.ToString();
                return retVal;
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }
        
        public int GetAdharNo(string adhaar)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@AadharCard", adhaar));
                object newID = DBHelper.ExecProcScalar("spGetAdharNo", spParams);
                retVal = int.Parse(newID.ToString());
                if (retVal == 1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }

        }
        public string getCheckAadharno(string adharno)
        {
            string retVal;

            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@adharno", adharno));
                object newID = DBHelper.ExecProcScalar("spgetCheckadhharforFiindBC", spParams);
                retVal = newID.ToString();
                return retVal;
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }

        }
        public List<ListAadhar> getCheckExistAdharno(string adharno)
        {

            List<ListAadhar> Listaadhar = null;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            DataSet adharBC = new DataSet();
            ListAadhar bc = null;
            try
            {
                spParams.Add(new SpParam("@adharno", adharno));
                // object newID = DBHelper.ExecProcScalar("spgetCheckadhharexistornot", spParams);
                DBHelper.ExecProcAndFillDataSet("spgetCheckadhharexistornot", spParams, adharBC, conn);
                if (adharBC != null && adharBC.Tables.Count > 0)
                {
                    //TABLE[0] - BCCORRES
                    DataTable dtBC = adharBC.Tables[0];
                    for (int cnt = 0; cnt < dtBC.Rows.Count; cnt++)
                    {
                        DataRow drBC = dtBC.Rows[cnt];

                        bc = new ListAadhar();

                      
                        if(Utils.GetStringValue(drBC["Name"])!="")
                        {
                              bc.Name = Utils.GetStringValue(drBC["Name"]);
                        }
                          if(Utils.GetStringValue(drBC["Email"])!="")
                        {
                               bc.Email = Utils.GetStringValue(drBC["Email"]);
                        }

                          if (Listaadhar == null)
                              Listaadhar = new List<ListAadhar>();

                        
                       
                        Listaadhar.Add(bc);
                    }
           

                }
                

                return Listaadhar;
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
              
        }
        
        public string getCheckbccode(string bccode)
        {
            string retVal ;

            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@bccode", bccode));
                object newID = DBHelper.ExecProcScalar("spgetCheckadhhar", spParams);
                retVal = newID.ToString();
                return retVal;
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }

        }

        public string allocateBC(string bankid, string bcId)
        {
            string retVal ;

            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@bankId", bankid));
                spParams.Add(new SpParam("@bcId", bcId));
                object newID = DBHelper.ExecProcScalar("spAllocateBank", spParams);
                retVal = newID.ToString();
                return retVal;
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }

        }
       
        
        public int getUserAviable(string userId)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@userId", userId));
                object newID = DBHelper.ExecProcScalar("spCheckUserAviable", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }

        
        public int logout(string userId, string TokenId)
        {
            int retVal = 0;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@tokenId", TokenId));
                object newID = DBHelper.ExecProcScalar("spDeleteToken", spParams);
                retVal = int.Parse(newID.ToString());
                if(retVal==1)
                {
                    return retVal;
                }
                else
                {
                    return retVal;
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in login: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            
        }
        

        public int ChangeBCStatus(BankCorrespondence bc, int userId, string mode) {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try {

                spParams.Add(new SpParam("@BcId", bc.BankCorrespondId));
                spParams.Add(new SpParam("@NewStatus", bc.Status));
                spParams.Add(new SpParam("@Reason", bc.StatusChangeReason));
                spParams.Add(new SpParam("@ChangeDate", bc.CurrentStatusSince));
                spParams.Add(new SpParam("@userId", userId));


                object newID = DBHelper.ExecProcScalar("sp_ChangeBCStatus", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in BlackListBankCorrespond: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally {
                conn.Close();
            }
            return retVal;
        }

        public List<UserEntity> GetUsers(int? userId = null)
        {
            List<UserEntity> AllUsers = null;
            UserEntity user = null;

            try
            {
                DataSet dsUsers = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@UserId", userId));

                DBHelper.ExecProcAndFillDataSet("sp_GetUser", spParams, dsUsers, conn);

                if (dsUsers != null && dsUsers.Tables.Count > 0)
                {
                    DataTable dtUser = dsUsers.Tables[0];
                    for (int cnt = 0; cnt < dtUser.Rows.Count; cnt++)
                    {
                        DataRow drUser = dtUser.Rows[cnt];

                        user = new UserEntity();

                        user.UserId = Utils.GetIntValue(drUser["UserId"]);
                        user.UserName = Utils.GetStringValue(drUser["UserName"]);
                        user.FirstName = Utils.GetStringValue(drUser["FirstName"]);
                        user.MiddleName = Utils.GetStringValue(drUser["MiddleName"]);
                        user.LastName = Utils.GetStringValue(drUser["LastName"]);
                        user.BankId = Utils.GetIntValue(drUser["BankId"]);
                        user.Phone = Utils.GetStringValue(drUser["Phone"]);
                        user.Email = Utils.GetStringValue(drUser["Email"]);
                        user.RoleId = Utils.GetStringValue(drUser["RoleId"]);


                        if (AllUsers == null)
                            AllUsers = new List<UserEntity>();

                        AllUsers.Add(user);

                    }


                    DataTable dtRoles = dsUsers.Tables[1];
                    if (AllUsers != null)
                    {
                        if (dtRoles.Rows.Count > 0)
                        {
                            foreach (DataRow drRole in dtRoles.Rows)
                            {
                                RoleEntity role = new RoleEntity();
                                int uId = Utils.GetIntValue(drRole["UserId"]);
                                role.RoleId = Utils.GetIntValue(drRole["RoleId"]);
                                role.Role = Utils.GetStringValue(drRole["Role"]);

                                user = AllUsers.FirstOrDefault(u => u.UserId == uId);
                                if (user != null)
                                {
                                    if (user.UserRoles == null)
                                        user.UserRoles = new List<RoleEntity>();

                                    user.UserRoles.Add(role);
                                }
                            }


                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;

            }

            return AllUsers;
        }


        public List<UserEntity> getUserMenu(int userId,string username)
        {
            List<UserEntity> AllUsers = null;
            UserEntity user = null;

            try {
                DataSet dsUsers = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@userRoleId", userId));
                spParams.Add(new SpParam("@username", username));
                
                DBHelper.ExecProcAndFillDataSet("sp_getUserState", spParams, dsUsers, conn);

                if (dsUsers != null && dsUsers.Tables.Count > 0) {
                    DataTable dtUser = dsUsers.Tables[0];
                    for (int cnt = 0; cnt < dtUser.Rows.Count; cnt++) {
                        DataRow drUser = dtUser.Rows[cnt];

                        user = new UserEntity();
                        user.menuId = Utils.GetIntValue(drUser["id"]);
                        user.state = Utils.GetStringValue(drUser["state"]);
                        user.menuName = Utils.GetStringValue(drUser["name"]);
                        user.menuIcon = Utils.GetStringValue(drUser["icon"]);
                        user.menulavel = Utils.GetStringValue(drUser["menulavel"]);
                        user.refid = Utils.GetIntValue(drUser["refid"]);
                        user.UserName = Utils.GetStringValue(drUser["username"]);
                        user.Role = Utils.GetStringValue(drUser["Role"]);
                        user.BankId = Utils.GetIntValue(drUser["BankId"]);  

                        if (AllUsers == null)
                            AllUsers = new List<UserEntity>();

                        AllUsers.Add(user);

                    }
               
                }

            }
            catch (Exception ex) {
                throw ex;

            }

            return AllUsers;
        }

        public List<UserEntity> getProducts(int productId)
        {
            List<UserEntity> AllUsers = null;
            UserEntity user = null;

            try {
                DataSet dsUsers = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@productId", productId));

                DBHelper.ExecProcAndFillDataSet("sp_getProductById", spParams, dsUsers, conn);

                if (dsUsers != null && dsUsers.Tables.Count > 0) {
                    DataTable dtUser = dsUsers.Tables[0];
                    for (int cnt = 0; cnt < dtUser.Rows.Count; cnt++) {
                        DataRow drUser = dtUser.Rows[cnt];

                        user = new UserEntity();
                        user.productId = Utils.GetIntValue(drUser["productId"]);
                        user.productName = Utils.GetStringValue(drUser["ProductName"]);  

                        if (AllUsers == null)
                            AllUsers = new List<UserEntity>();

                        AllUsers.Add(user);

                    }
               
                }

            }
            catch (Exception ex) {
                throw ex;

            }

            return AllUsers;
        }
        
        public List<ProductGroupName> getGroupUserProducts()
        {
            List<ProductGroupName> AllUsers = null;
            ProductGroupName user = null;

            try
            {
                DataSet dsUsers = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);
                SpParamCollection spParams = new SpParamCollection();
                DBHelper.ExecProcAndFillDataSet("sp_getgrouupProductAll", spParams, dsUsers, conn);

                if (dsUsers != null && dsUsers.Tables.Count > 0)
                {
                    DataTable dtUser = dsUsers.Tables[0];
                    for (int cnt = 0; cnt < dtUser.Rows.Count; cnt++)
                    {
                        DataRow drUser = dtUser.Rows[cnt];

                        user = new ProductGroupName();
                        user.id = Utils.GetIntValue(drUser["id"]);
                        user.GroupName = Utils.GetStringValue(drUser["GroupName"]);

                        if (AllUsers == null)
                            AllUsers = new List<ProductGroupName>();

                        AllUsers.Add(user);

                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;

            }

            return AllUsers;
        }

        public List<ProductGroupName> getGroupProducts(string groupId)
        {
            List<ProductGroupName> AllUsers = null;
            List<ProductGroup> ProductGroupAll = null;
            ProductGroupName user = null;
            ProductGroup users = null;

            try
            {
                DataSet dsUsers = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);
                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@groupId", groupId));
                DBHelper.ExecProcAndFillDataSet("sp_GetGroupuProductDetails", spParams, dsUsers, conn);

                if (dsUsers != null && dsUsers.Tables.Count > 0)
                {
                    DataTable dtUser = dsUsers.Tables[0];
                    for (int cnt = 0; cnt < dtUser.Rows.Count; cnt++)
                    {
                        DataRow drUser = dtUser.Rows[cnt];

                        user = new ProductGroupName();
                        user.id = Utils.GetIntValue(drUser["id"]);
                        user.GroupName = Utils.GetStringValue(drUser["GroupName"]);

                        if (AllUsers == null)
                            AllUsers = new List<ProductGroupName>();
                        AllUsers.Add(user);
                    }

                    DataTable dtUser1 = dsUsers.Tables[1];
                    for (int cnt = 0; cnt < dtUser1.Rows.Count; cnt++)
                    {
                        DataRow drUser = dtUser1.Rows[cnt];

                        users = new ProductGroup();
                        users.ProductId = Utils.GetStringValue(drUser["ProductId"]);
                        users.ProductName = Utils.GetStringValue(drUser["ProductName"]);

                        if (ProductGroupAll == null)
                            ProductGroupAll = new List<ProductGroup>();
                        ProductGroupAll.Add(users);
                      
                    }
                    user.ProductGrouplist = ProductGroupAll;

                }

            }
            catch (Exception ex)
            {
                throw ex;

            }

            return AllUsers;
        }
        public List<UserEntity> getUserProducts()
        {
            List<UserEntity> AllUsers = null;
            UserEntity user = null;

            try {
                DataSet dsUsers = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);
                SpParamCollection spParams = new SpParamCollection();
                DBHelper.ExecProcAndFillDataSet("sp_getProductAll", spParams, dsUsers, conn);

                if (dsUsers != null && dsUsers.Tables.Count > 0) {
                    DataTable dtUser = dsUsers.Tables[0];
                    for (int cnt = 0; cnt < dtUser.Rows.Count; cnt++) {
                        DataRow drUser = dtUser.Rows[cnt];

                        user = new UserEntity();
                        user.productId = Utils.GetIntValue(drUser["productId"]);
                        user.productName = Utils.GetStringValue(drUser["ProductName"]);  

                        if (AllUsers == null)
                            AllUsers = new List<UserEntity>();

                        AllUsers.Add(user);

                    }
               
                }

            }
            catch (Exception ex) {
                throw ex;

            }

            return AllUsers;
        }
        public List<Institute> getInstitue()
        {
            List<Institute> AllUsers = null;
            Institute user = null;

            try {
                DataSet dsUsers = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);
                SpParamCollection spParams = new SpParamCollection();
                DBHelper.ExecProcAndFillDataSet("spGetInstitue", spParams, dsUsers, conn);

                if (dsUsers != null && dsUsers.Tables.Count > 0) {
                    DataTable dtUser = dsUsers.Tables[0];
                    for (int cnt = 0; cnt < dtUser.Rows.Count; cnt++) {
                        DataRow drUser = dtUser.Rows[cnt];

                        user = new Institute();
                        user.InstituteName = Utils.GetStringValue(drUser["Institute"]);  

                        if (AllUsers == null)
                            AllUsers = new List<Institute>();

                        AllUsers.Add(user);

                    }
               
                }

            }
            catch (Exception ex) {
                throw ex;

            }

            return AllUsers;
        }
        public List<Coursess> getCouse()
        {
            List<Coursess> AllUsers = null;
            Coursess user = null;

            try
            {
                DataSet dsUsers = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);
                SpParamCollection spParams = new SpParamCollection();
                DBHelper.ExecProcAndFillDataSet("spGetCourse", spParams, dsUsers, conn);

                if (dsUsers != null && dsUsers.Tables.Count > 0)
                {
                    DataTable dtUser = dsUsers.Tables[0];
                    for (int cnt = 0; cnt < dtUser.Rows.Count; cnt++)
                    {
                        DataRow drUser = dtUser.Rows[cnt];

                        user = new Coursess();
                        user.Course = Utils.GetStringValue(drUser["Course"]);

                        if (AllUsers == null)
                            AllUsers = new List<Coursess>();

                        AllUsers.Add(user);

                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;

            }

            return AllUsers;
        }
        public List<Branch> getBranch(string userId)
        {
            List<Branch> AllUsers = null;
            Branch user = null;

            try
            {
                DataSet dsUsers = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);
                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@userId", userId));

                DBHelper.ExecProcAndFillDataSet("spGetBrnach", spParams, dsUsers, conn);

                if (dsUsers != null && dsUsers.Tables.Count > 0)
                {
                    DataTable dtUser = dsUsers.Tables[0];
                    for (int cnt = 0; cnt < dtUser.Rows.Count; cnt++)
                    {
                        DataRow drUser = dtUser.Rows[cnt];

                        user = new Branch();
                        user.BranchId = Utils.GetIntValue(drUser["BranchId"]);
                        user.BranchName = Utils.GetStringValue(drUser["BranchName"]);
                        user.IFSCCode = Utils.GetStringValue(drUser["IFSCCode"]);
                        user.Address = Utils.GetStringValue(drUser["Address"]);
                        user.ContactNumber = Utils.GetStringValue(drUser["ContactNumber"]);
                        user.Email = Utils.GetStringValue(drUser["Email"]);
                        user.BankId = Utils.GetIntValue(drUser["BankId"]);
                        user.PinCode = Utils.GetStringValue(drUser["PinCode"]);
                        user.RegionId = Utils.GetIntValue(drUser["RegionId"]);
                        user.StateId = Utils.GetIntValue(drUser["StateId"]);
                        user.DistrictId = Utils.GetIntValue(drUser["DistrictId"]);
                        user.TalukaId = Utils.GetIntValue(drUser["TalukaId"]);
                        user.CityId = Utils.GetIntValue(drUser["CityId"]);
                        user.VillageId = Utils.GetIntValue(drUser["VillageId"]);

                        if (AllUsers == null)
                            AllUsers = new List<Branch>();

                        AllUsers.Add(user);

                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;

            }

            return AllUsers;
        }
        
        public List<Notification> GetNotifications(int? notificationId) {
            List<Notification> allNotifications = null;
            Notification notification = null;

            try {
                DataSet dsNotifications = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@NotificationId", notificationId));

                DBHelper.ExecProcAndFillDataSet("sp_GetNotifications", spParams, dsNotifications, conn);

                if (dsNotifications != null && dsNotifications.Tables.Count > 0) {
                    DataTable dtBank = dsNotifications.Tables[0];
                    for (int cnt = 0; cnt < dtBank.Rows.Count; cnt++) {
                        DataRow drBank = dtBank.Rows[cnt];

                        notification = new Notification();

                        notification.NotificationId = Utils.GetIntValue(drBank["NotificationId"]);
                        notification.NotificationType = Utils.GetStringValue(drBank["NotificationType"]);
                        notification.NotificationText = Utils.GetStringValue(drBank["NotificationText"]);
                        notification.NotificationPriority = Utils.GetStringValue(drBank["NotificationPriority"]);
                        notification.NotificationDate = Utils.GetStringValue(drBank["NotificationDate"]);


                        if (allNotifications == null)
                            allNotifications = new List<Notification>();

                        allNotifications.Add(notification);

                    }


                }
            }
            catch (Exception ex) {
                throw ex;

            }

            return allNotifications;
        }


        public List<BankCorrespondence> GeBCpulicDetails(string bcId)
        {
            List<BankCorrespondence> allBCs = null;
            BankCorrespondence bc = null;
         //   BCCertifications bccer = null;
           // PreviousExperiene preexp = null;
        //    OperationalAreas area = null;
            //  SsaDetails ssd = null;
             //   BCDevices device = null;
            //ConnectivityDetails condet = null;
        //    BCProducts prod = null;

            try
            {
                DataSet dsBC = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@stateId", Convert.ToInt32(bcId)));

                DBHelper.ExecProcAndFillDataSet("sp_bcdetailspublic", spParams, dsBC, conn);

                if (dsBC != null && dsBC.Tables.Count > 0)
                {
                    //TABLE[0] - BCCORRES
                    DataTable dtBC = dsBC.Tables[0];
                    for (int cnt = 0; cnt < dtBC.Rows.Count; cnt++)
                    {
                        DataRow drBC = dtBC.Rows[cnt];

                        bc = new BankCorrespondence();

                        bc.BankCorrespondId = Utils.GetIntValue(drBC["BankCorrespondId"]);

                        bc.Name = Utils.GetStringValue(drBC["Name"]);
                        bc.PhoneNumber1 = Utils.GetStringValue(drBC["PhoneNumber"]);
                        bc.BankName = Utils.GetStringValue(drBC["bankName"]);
                        bc.State = Utils.GetStringValue(drBC["StateName"]);
                        bc.Village = Utils.GetStringValue(drBC["VillageName"]);
                        bc.District = Utils.GetStringValue(drBC["DistrictName"]);
                        bc.Subdistrict = Utils.GetStringValue(drBC["SubDistrictName"]);
                        bc.productName = Utils.GetStringValue(drBC["ProductName"]);


                        

                        if (allBCs == null)
                            allBCs = new List<BankCorrespondence>();

                        allBCs.Add(bc);



                    }  //for loop
                    return allBCs;

                    //////////////Set Datatable for certs
                    //////////////Loop over rows
                    ////////////// Create a certificate object
                    //////////////BankCorresponce bc =  allBCs.FirstOrDefault(b=>b.BankCorrespondId==)
                    ////////////// if (bc.Ce == null)
                    //////////////bc.Ce = new     
                    ////////////// bc .Add(certificate object)

                    //////////////TABLE [1] - BCDEVS
                    //////////////DataTable dtdevice = dsBC.Tables[2];
                    ////////////DataTable dtdevice = dsBC.Tables[1];
                    ////////////for (int cnt = 0; cnt < dtdevice.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drdevice = dtdevice.Rows[cnt];

                    ////////////    device = new BCDevices();

                    ////////////    device.DeviceId = Utils.GetIntValue(drdevice["DeviceId"]);
                    ////////////    device.Device = Utils.GetStringValue(drdevice["Device"]);
                    ////////////    device.GivenOn = Utils.GetDateTimeValue(drdevice["GivenOn"]);
                    ////////////    device.BCId = Utils.GetIntValue(drdevice["BCId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == device.BCId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.Devices == null)
                    ////////////            bc.Devices = new List<BCDevices>();
                    ////////////        bc.Devices.Add(device);
                    ////////////    }
                    ////////////}
                    //////////////TABLE[2] - PREVEXP
                    //////////////DataTable dtExp = dsBC.Tables[3];
                    ////////////DataTable dtExp = dsBC.Tables[2];
                    ////////////for (int cnt = 0; cnt < dtExp.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drExp = dtExp.Rows[cnt];
                    ////////////    preexp = new PreviousExperiene();

                    ////////////    preexp.ExperienceId = Utils.GetIntValue(drExp["ExperienceId"]);
                    ////////////    preexp.BankId = Utils.GetIntValue(drExp["BankId"]);
                    ////////////    preexp.BankName = Utils.GetStringValue(drExp["BankName"]);
                    ////////////    preexp.BranchName = Utils.GetStringValue(drExp["BranchName"]);
                    ////////////    preexp.Branchid = Utils.GetIntValue(drExp["Branchid"]);
                    ////////////    preexp.FromDate = Utils.GetDateTimeValue(drExp["FromDate"]);
                    ////////////    preexp.ToDate = Utils.GetDateTimeValue(drExp["ToDate"]);
                    ////////////    preexp.Reason = Utils.GetStringValue(drExp["Reasons"]);
                    ////////////    preexp.oBankName = Utils.GetStringValue(drExp["oBankName"]);
                    ////////////    preexp.oBranchName = Utils.GetStringValue(drExp["oBranchName"]);
                    ////////////    preexp.BankCorrespondenceId = Utils.GetIntValue(drExp["BankCorrespondenceId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == preexp.BankCorrespondenceId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.PreviousExperience == null)
                    ////////////            bc.PreviousExperience = new List<PreviousExperiene>();
                    ////////////        bc.PreviousExperience.Add(preexp);
                    ////////////    }
                    ////////////}

                    //////////////TABLE[3] - CERT
                    //////////////DataTable dtCer = dsBC.Tables[1];
                    ////////////DataTable dtCer = dsBC.Tables[3];
                    ////////////for (int cnt = 0; cnt < dtCer.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drCer = dtCer.Rows[cnt];
                    ////////////    bccer = new BCCertifications();

                    ////////////    bccer.CertificationId = Utils.GetIntValue(drCer["CertificationId"]);
                    ////////////    bccer.DateOfPassing = Utils.GetDateTimeValue(drCer["PassingDate"]);
                    ////////////    bccer.InstituteId = Utils.GetIntValue(drCer["InstituteId"]);
                    ////////////    bccer.CourseId = Utils.GetIntValue(drCer["CourseId"]);
                    ////////////    bccer.InstituteName = Utils.GetStringValue(drCer["InstituteName"]);
                    ////////////    bccer.CourseName = Utils.GetStringValue(drCer["CourseName"]);
                    ////////////    bccer.Grade = Utils.GetStringValue(drCer["Grade"]);
                    ////////////    bccer.BankCorresponenceId = Utils.GetIntValue(drCer["BankCorresponenceId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == bccer.BankCorresponenceId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.Certifications == null)
                    ////////////            bc.Certifications = new List<BCCertifications>();
                    ////////////        bc.Certifications.Add(bccer);
                    ////////////    }
                    ////////////}


                    //////////////TABLE[4] - CONNDET                  
                    //////////////DataTable dtcondet = dsBC.Tables[4];
                    ////////////DataTable dtcondet = dsBC.Tables[4];
                    ////////////for (int cnt = 0; cnt < dtcondet.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drcondet = dtcondet.Rows[cnt];
                    ////////////    condet = new ConnectivityDetails();

                    ////////////    condet.ConnectivityId = Utils.GetIntValue(drcondet["ConnectivityId"]);
                    ////////////    condet.ConnectivityMode = Utils.GetStringValue(drcondet["ConnectivityMode"]);
                    ////////////    condet.ConnectivityProvider = Utils.GetStringValue(drcondet["ConnectivityProvider"]);
                    ////////////    condet.ContactNumber = Utils.GetStringValue(drcondet["ContactNumber"]);
                    ////////////    condet.BCId = Utils.GetIntValue(drcondet["BCId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == condet.BCId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.ConnectivityDetails == null)
                    ////////////            bc.ConnectivityDetails = new List<ConnectivityDetails>();
                    ////////////        bc.ConnectivityDetails.Add(condet);
                    ////////////    }
                    ////////////}


                    //////////////TABLE[5] - OPRAREA
                    //////////////DataTable dtArea = dsBC.Tables[5];
                    ////////////DataTable dtArea = dsBC.Tables[5];
                    ////////////for (int cnt = 0; cnt < dtArea.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drArea = dtArea.Rows[cnt];
                    ////////////    area = new OperationalAreas();

                    ////////////    area.AreaId = Utils.GetIntValue(drArea["AreaId"]);
                    ////////////    area.VillageCode = Utils.GetStringValue(drArea["VillageCode"]);
                    ////////////    area.VillageDetail = Utils.GetStringValue(drArea["VillageDetail"]);
                    ////////////    area.BCId = Utils.GetIntValue(drArea["BCId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == area.BCId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.OperationalAreas == null)
                    ////////////            bc.OperationalAreas = new List<OperationalAreas>();
                    ////////////        bc.OperationalAreas.Add(area);
                    ////////////    }
                    ////////////}



                    //////////////TABLE[5] - OPRAREA
                    //////////////DataTable dtArea = dsBC.Tables[5];
                    ////////////DataTable dtSSADetails = dsBC.Tables[7];
                    ////////////for (int cnt = 0; cnt < dtSSADetails.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow dtSSADetailss = dtSSADetails.Rows[cnt];
                    ////////////    ssd = new SsaDetails();

                    ////////////    ssd.Ssa = Utils.GetStringValue(dtSSADetailss["ssa"]);
                    ////////////    ssd.Village = Utils.GetStringValue(dtSSADetailss["village"]);
                    ////////////    ssd.State = Utils.GetStringValue(dtSSADetailss["StateId"]);
                    ////////////    ssd.StateName = Utils.GetStringValue(dtSSADetailss["StateName"]);
                    ////////////    ssd.StateCode = Utils.GetStringValue(dtSSADetailss["StateCode"]);
                    ////////////    ssd.District = Utils.GetStringValue(dtSSADetailss["DistrictId"]);
                    ////////////    ssd.DistrictName = Utils.GetStringValue(dtSSADetailss["DistrictName"]);
                    ////////////    ssd.DistrictCode = Utils.GetStringValue(dtSSADetailss["DistrictCode"]);
                    ////////////    ssd.SubDistrict = Utils.GetStringValue(dtSSADetailss["SubDistrictId"]);
                    ////////////    ssd.SubDistrictName = Utils.GetStringValue(dtSSADetailss["SubDistrictName"]);
                    ////////////    ssd.SubDistrictCode = Utils.GetStringValue(dtSSADetailss["SubDistrictCode"]);
                    ////////////    ssd.bcid = Utils.GetStringValue(dtSSADetailss["bcid"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == Convert.ToInt32(ssd.bcid));
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.SsaDetails == null)
                    ////////////            bc.SsaDetails = new List<SsaDetails>();
                    ////////////        bc.SsaDetails.Add(ssd);
                    ////////////    }
                    ////////////}




                    //////////////TABLE[6] - BCPROD
                    //////////////DataTable dtprod = dsBC.Tables[6];
                    ////////////DataTable dtprod = dsBC.Tables[6];
                    ////////////for (int cnt = 0; cnt < dtprod.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drprod = dtprod.Rows[cnt];
                    ////////////    prod = new BCProducts();

                    ////////////    prod.ProductId = Utils.GetIntValue(drprod["ProductId"]);
                    ////////////    prod.ProductName = Utils.GetStringValue(drprod["ProductName"]);
                    ////////////    prod.BCId = Utils.GetIntValue(drprod["BCId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == prod.BCId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.Products == null)
                    ////////////            bc.Products = new List<BCProducts>();
                    ////////////        bc.Products.Add(prod);
                    ////////////    }
                    ////////////}

                    //TABLE[6] - BCPROD
                    //DataTable dtprod = dsBC.Tables[6];


                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return allBCs;
        }
     public List<BankCorrespondence> GetBankCorrespondentsbystate(string stateId, string district, string subdistrict, string village)
        {
            List<BankCorrespondence> allBCs = null;
            BankCorrespondence bc = null;
         //   BCCertifications bccer = null;
          //  PreviousExperiene preexp = null;
          //  OperationalAreas area = null;
         //   SsaDetails ssd = null;
         //   BCDevices device = null;
         //   ConnectivityDetails condet = null;
         //   BCProducts prod = null;

            try
            {
                DataSet dsBC = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@stateId", Convert.ToInt32(stateId)));
                spParams.Add(new SpParam("@district", Convert.ToInt32(district)));
                spParams.Add(new SpParam("@subdistrict", Convert.ToInt32(subdistrict)));
                spParams.Add(new SpParam("@village", Convert.ToInt32(village)));

                DBHelper.ExecProcAndFillDataSet("SP_GETBCSTATE", spParams, dsBC, conn);

                if (dsBC != null && dsBC.Tables.Count > 0)
                {
                    //TABLE[0] - BCCORRES
                    DataTable dtBC = dsBC.Tables[0];
                    for (int cnt = 0; cnt < dtBC.Rows.Count; cnt++)
                    {
                        DataRow drBC = dtBC.Rows[cnt];

                        bc = new BankCorrespondence();

                        bc.BankCorrespondId = Utils.GetIntValue(drBC["BankCorrespondId"]);

                        bc.Name = Utils.GetStringValue(drBC["Name"]);
                        bc.ImagePath = Utils.GetStringValue(drBC["ImagePath"]);
                        bc.Gender = Utils.GetStringValue(drBC["Gender"]);                      
                        bc.DOB = Utils.GetDateTimeValue(drBC["DOB"]);
                        bc.FatherName = Utils.GetStringValue(drBC["FatherName"]);
                        bc.SpouseName = Utils.GetStringValue(drBC["SpouseName"]);
                        bc.Category = Utils.GetStringValue(drBC["Category"]);

                        bc.Handicap = Utils.GetBoolValue(drBC["PhysicallyHandicap"]) ? "Yes" : "No";
                        bc.PhoneNumber1 = Utils.GetStringValue(drBC["PhoneNumber"]);
                        bc.PhoneNumber2 = Utils.GetStringValue(drBC["PhoneNumber1"]);
                        bc.PhoneNumber3 = Utils.GetStringValue(drBC["PhoneNumber2"]);
                        bc.ContactPerson = Utils.GetStringValue(drBC["ContactPerson"]);
                        bc.ContactDesignation = Utils.GetStringValue(drBC["ContactDesignation"]);
                        bc.NoofComplaint = Utils.GetStringValue(drBC["NoofComplaint"]);
                        bc.Email = Utils.GetStringValue(drBC["Email"]);
                        bc.AadharCard = Utils.GetStringValue(drBC["AadharCard"]);
                        bc.PanCard = Utils.GetStringValue(drBC["PanCard"]);
                        bc.VoterCard = Utils.GetStringValue(drBC["VotersIdCard"]);
                        bc.DriverLicense = Utils.GetStringValue(drBC["DriverLicense"]);
                        bc.NregaCard = Utils.GetStringValue(drBC["NREGACard"]);
                        bc.RationCard = Utils.GetStringValue(drBC["RationCard"]);
                        bc.State = Utils.GetStringValue(drBC["AddressState"]);
                        bc.City = Utils.GetStringValue(drBC["AddressCity"]);
                        bc.Village = Utils.GetStringValue(drBC["village"]);
                        bc.Village = Utils.GetStringValue(drBC["village"]);
                        bc.Latitude = Utils.GetStringValue(drBC["Latitude"]);
                        bc.Longitude = Utils.GetStringValue(drBC["Longitude"]);
                        bc.District = Utils.GetStringValue(drBC["AddressDistrict"]);
                        bc.Subdistrict = Utils.GetStringValue(drBC["AddressSubdistrict"]);
                        bc.Area = Utils.GetStringValue(drBC["AddressArea"]);
                        bc.PinCode = Utils.GetStringValue(drBC["PinCode"]);
                        bc.AlternateOccupationType = Utils.GetStringValue(drBC["AlternateOccupationType"]);
                        bc.AlternateOccupationDetail = Utils.GetStringValue(drBC["AlternateOccupationDetail"]);
                        bc.UniqueIdentificationNumber = Utils.GetStringValue(drBC["UniqueIdentificationNumber"]);
                        bc.BankReferenceNumber = Utils.GetStringValue(drBC["BankReferenceNumber"]);
                        bc.Qualification = Utils.GetStringValue(drBC["EducationalQualification"]);
                        bc.OtherQualification = Utils.GetStringValue(drBC["OtherQualification"]);
                        bc.isAllocated = Utils.GetBoolValue(drBC["IsAllocated"]);
                        bc.Status = Utils.GetStringValue(drBC["Status"]);
                        bc.CorporateId = Utils.GetIntValue(drBC["CorporateId"]);

                        bc.CreatedOn = Utils.GetDateTimeValue(drBC["CreatedOn"]);
                        bc.CreatedBy = Utils.GetIntValue(drBC["CreatedBy"]);
                        bc.terminateBc = Utils.GetIntValue(drBC["terminateBc"]);


                        bc.IsBlackListed = Utils.GetBoolValue(drBC["IsBlackListed"]);
                        bc.BlacklistDate = Utils.GetDateTimeValue(drBC["BlackListedDate"]);
                        bc.BlackListReason = Utils.GetStringValue(drBC["BlackListedReason"]);
                        bc.BlackListApprovalNumber = Utils.GetStringValue(drBC["BlackListedApprovalNumber"]);

                    
                            bc.AppointmentDate = Utils.GetDateTimeValue(drBC["AppointmentDate"]);
                            bc.AllocationIFSCCode = Utils.GetStringValue(drBC["AllocationIFSCCode"]);
                            bc.AllocationBankId = Utils.GetIntValue(drBC["AllocationBankId"]);
                            bc.AllocationBranchId = Utils.GetIntValue(drBC["AllocationBranchId"]);
                            bc.BCType = Utils.GetStringValue(drBC["BCType"]);
                            bc.WorkingDays = Utils.GetIntValue(drBC["WorkingDays"]);
                            bc.WorkingHours = Utils.GetIntValue(drBC["WorkingHours"]);
                            bc.PLPostalAddress = Utils.GetStringValue(drBC["PLPostalAddress"]);
                            bc.PLVillageCode = Utils.GetStringValue(drBC["PLVillageCode"]);
                            bc.PLVillageDetail = Utils.GetStringValue(drBC["PLVillageDetail"]);
                            bc.PLTaluk = Utils.GetStringValue(drBC["PLTaluk"]);
                            bc.PLDistrictId = Utils.GetIntValue(drBC["PLDistrictId"]);
                            bc.PLStateId = Utils.GetIntValue(drBC["PLStateId"]);
                            bc.PLPinCode = Utils.GetStringValue(drBC["PLPinCode"]);
                            bc.MinimumCashHandlingLimit = Utils.GetDoubleValue(drBC["MinimumCashHandlingLimit"]);
                            bc.MonthlyFixedRenumeration = Utils.GetDoubleValue(drBC["MonthlyFixedRenumeration"]);
                            bc.MonthlyVariableRenumeration = Utils.GetDoubleValue(drBC["MonthlyVariableRenumeration"]);



                        

                        if (allBCs == null)
                            allBCs = new List<BankCorrespondence>();

                        allBCs.Add(bc);



                    }  //for loop
                    return allBCs;

                    //////////////Set Datatable for certs
                    //////////////Loop over rows
                    ////////////// Create a certificate object
                    //////////////BankCorresponce bc =  allBCs.FirstOrDefault(b=>b.BankCorrespondId==)
                    ////////////// if (bc.Ce == null)
                    //////////////bc.Ce = new     
                    ////////////// bc .Add(certificate object)

                    //////////////TABLE [1] - BCDEVS
                    //////////////DataTable dtdevice = dsBC.Tables[2];
                    ////////////DataTable dtdevice = dsBC.Tables[1];
                    ////////////for (int cnt = 0; cnt < dtdevice.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drdevice = dtdevice.Rows[cnt];

                    ////////////    device = new BCDevices();

                    ////////////    device.DeviceId = Utils.GetIntValue(drdevice["DeviceId"]);
                    ////////////    device.Device = Utils.GetStringValue(drdevice["Device"]);
                    ////////////    device.GivenOn = Utils.GetDateTimeValue(drdevice["GivenOn"]);
                    ////////////    device.BCId = Utils.GetIntValue(drdevice["BCId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == device.BCId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.Devices == null)
                    ////////////            bc.Devices = new List<BCDevices>();
                    ////////////        bc.Devices.Add(device);
                    ////////////    }
                    ////////////}
                    //////////////TABLE[2] - PREVEXP
                    //////////////DataTable dtExp = dsBC.Tables[3];
                    ////////////DataTable dtExp = dsBC.Tables[2];
                    ////////////for (int cnt = 0; cnt < dtExp.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drExp = dtExp.Rows[cnt];
                    ////////////    preexp = new PreviousExperiene();

                    ////////////    preexp.ExperienceId = Utils.GetIntValue(drExp["ExperienceId"]);
                    ////////////    preexp.BankId = Utils.GetIntValue(drExp["BankId"]);
                    ////////////    preexp.BankName = Utils.GetStringValue(drExp["BankName"]);
                    ////////////    preexp.BranchName = Utils.GetStringValue(drExp["BranchName"]);
                    ////////////    preexp.Branchid = Utils.GetIntValue(drExp["Branchid"]);
                    ////////////    preexp.FromDate = Utils.GetDateTimeValue(drExp["FromDate"]);
                    ////////////    preexp.ToDate = Utils.GetDateTimeValue(drExp["ToDate"]);
                    ////////////    preexp.Reason = Utils.GetStringValue(drExp["Reasons"]);
                    ////////////    preexp.oBankName = Utils.GetStringValue(drExp["oBankName"]);
                    ////////////    preexp.oBranchName = Utils.GetStringValue(drExp["oBranchName"]);
                    ////////////    preexp.BankCorrespondenceId = Utils.GetIntValue(drExp["BankCorrespondenceId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == preexp.BankCorrespondenceId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.PreviousExperience == null)
                    ////////////            bc.PreviousExperience = new List<PreviousExperiene>();
                    ////////////        bc.PreviousExperience.Add(preexp);
                    ////////////    }
                    ////////////}

                    //////////////TABLE[3] - CERT
                    //////////////DataTable dtCer = dsBC.Tables[1];
                    ////////////DataTable dtCer = dsBC.Tables[3];
                    ////////////for (int cnt = 0; cnt < dtCer.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drCer = dtCer.Rows[cnt];
                    ////////////    bccer = new BCCertifications();

                    ////////////    bccer.CertificationId = Utils.GetIntValue(drCer["CertificationId"]);
                    ////////////    bccer.DateOfPassing = Utils.GetDateTimeValue(drCer["PassingDate"]);
                    ////////////    bccer.InstituteId = Utils.GetIntValue(drCer["InstituteId"]);
                    ////////////    bccer.CourseId = Utils.GetIntValue(drCer["CourseId"]);
                    ////////////    bccer.InstituteName = Utils.GetStringValue(drCer["InstituteName"]);
                    ////////////    bccer.CourseName = Utils.GetStringValue(drCer["CourseName"]);
                    ////////////    bccer.Grade = Utils.GetStringValue(drCer["Grade"]);
                    ////////////    bccer.BankCorresponenceId = Utils.GetIntValue(drCer["BankCorresponenceId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == bccer.BankCorresponenceId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.Certifications == null)
                    ////////////            bc.Certifications = new List<BCCertifications>();
                    ////////////        bc.Certifications.Add(bccer);
                    ////////////    }
                    ////////////}


                    //////////////TABLE[4] - CONNDET                  
                    //////////////DataTable dtcondet = dsBC.Tables[4];
                    ////////////DataTable dtcondet = dsBC.Tables[4];
                    ////////////for (int cnt = 0; cnt < dtcondet.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drcondet = dtcondet.Rows[cnt];
                    ////////////    condet = new ConnectivityDetails();

                    ////////////    condet.ConnectivityId = Utils.GetIntValue(drcondet["ConnectivityId"]);
                    ////////////    condet.ConnectivityMode = Utils.GetStringValue(drcondet["ConnectivityMode"]);
                    ////////////    condet.ConnectivityProvider = Utils.GetStringValue(drcondet["ConnectivityProvider"]);
                    ////////////    condet.ContactNumber = Utils.GetStringValue(drcondet["ContactNumber"]);
                    ////////////    condet.BCId = Utils.GetIntValue(drcondet["BCId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == condet.BCId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.ConnectivityDetails == null)
                    ////////////            bc.ConnectivityDetails = new List<ConnectivityDetails>();
                    ////////////        bc.ConnectivityDetails.Add(condet);
                    ////////////    }
                    ////////////}


                    //////////////TABLE[5] - OPRAREA
                    //////////////DataTable dtArea = dsBC.Tables[5];
                    ////////////DataTable dtArea = dsBC.Tables[5];
                    ////////////for (int cnt = 0; cnt < dtArea.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drArea = dtArea.Rows[cnt];
                    ////////////    area = new OperationalAreas();

                    ////////////    area.AreaId = Utils.GetIntValue(drArea["AreaId"]);
                    ////////////    area.VillageCode = Utils.GetStringValue(drArea["VillageCode"]);
                    ////////////    area.VillageDetail = Utils.GetStringValue(drArea["VillageDetail"]);
                    ////////////    area.BCId = Utils.GetIntValue(drArea["BCId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == area.BCId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.OperationalAreas == null)
                    ////////////            bc.OperationalAreas = new List<OperationalAreas>();
                    ////////////        bc.OperationalAreas.Add(area);
                    ////////////    }
                    ////////////}



                    //////////////TABLE[5] - OPRAREA
                    //////////////DataTable dtArea = dsBC.Tables[5];
                    ////////////DataTable dtSSADetails = dsBC.Tables[7];
                    ////////////for (int cnt = 0; cnt < dtSSADetails.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow dtSSADetailss = dtSSADetails.Rows[cnt];
                    ////////////    ssd = new SsaDetails();

                    ////////////    ssd.Ssa = Utils.GetStringValue(dtSSADetailss["ssa"]);
                    ////////////    ssd.Village = Utils.GetStringValue(dtSSADetailss["village"]);
                    ////////////    ssd.State = Utils.GetStringValue(dtSSADetailss["StateId"]);
                    ////////////    ssd.StateName = Utils.GetStringValue(dtSSADetailss["StateName"]);
                    ////////////    ssd.StateCode = Utils.GetStringValue(dtSSADetailss["StateCode"]);
                    ////////////    ssd.District = Utils.GetStringValue(dtSSADetailss["DistrictId"]);
                    ////////////    ssd.DistrictName = Utils.GetStringValue(dtSSADetailss["DistrictName"]);
                    ////////////    ssd.DistrictCode = Utils.GetStringValue(dtSSADetailss["DistrictCode"]);
                    ////////////    ssd.SubDistrict = Utils.GetStringValue(dtSSADetailss["SubDistrictId"]);
                    ////////////    ssd.SubDistrictName = Utils.GetStringValue(dtSSADetailss["SubDistrictName"]);
                    ////////////    ssd.SubDistrictCode = Utils.GetStringValue(dtSSADetailss["SubDistrictCode"]);
                    ////////////    ssd.bcid = Utils.GetStringValue(dtSSADetailss["bcid"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == Convert.ToInt32(ssd.bcid));
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.SsaDetails == null)
                    ////////////            bc.SsaDetails = new List<SsaDetails>();
                    ////////////        bc.SsaDetails.Add(ssd);
                    ////////////    }
                    ////////////}




                    //////////////TABLE[6] - BCPROD
                    //////////////DataTable dtprod = dsBC.Tables[6];
                    ////////////DataTable dtprod = dsBC.Tables[6];
                    ////////////for (int cnt = 0; cnt < dtprod.Rows.Count; cnt++)
                    ////////////{
                    ////////////    DataRow drprod = dtprod.Rows[cnt];
                    ////////////    prod = new BCProducts();

                    ////////////    prod.ProductId = Utils.GetIntValue(drprod["ProductId"]);
                    ////////////    prod.ProductName = Utils.GetStringValue(drprod["ProductName"]);
                    ////////////    prod.BCId = Utils.GetIntValue(drprod["BCId"]);

                    ////////////    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == prod.BCId);
                    ////////////    if (bc != null)
                    ////////////    {
                    ////////////        if (bc.Products == null)
                    ////////////            bc.Products = new List<BCProducts>();
                    ////////////        bc.Products.Add(prod);
                    ////////////    }
                    ////////////}

                    //TABLE[6] - BCPROD
                    //DataTable dtprod = dsBC.Tables[6];


                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return allBCs;
        }
        
        public List<BankCorrespondence> GetBankCorrespondence(int? BankCorrespondId)
        {
            List<BankCorrespondence> allBCs = null;
            BankCorrespondence bc = null;
            BCCertifications bccer = null;
            PreviousExperiene preexp = null;
            OperationalAreas area = null;
            SsaDetails ssd = null;
            BCDevices device = null;
            ConnectivityDetails condet = null;
            BCProducts prod = null;

            try
            {
                DataSet dsBC = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@BankCorrespondId", BankCorrespondId));

                DBHelper.ExecProcAndFillDataSet("sp_GetBankCorresponds", spParams, dsBC, conn);

                if (dsBC != null && dsBC.Tables.Count > 0)
                {
                    //TABLE[0] - BCCORRES
                    DataTable dtBC = dsBC.Tables[0];
                    for (int cnt = 0; cnt < dtBC.Rows.Count; cnt++)
                    {
                        DataRow drBC = dtBC.Rows[cnt];

                        bc = new BankCorrespondence();

                        bc.BankCorrespondId = Utils.GetIntValue(drBC["BankCorrespondId"]);

                        bc.Name = Utils.GetStringValue(drBC["Name"]);
                        bc.ImagePath = Utils.GetStringValue(drBC["ImagePath"]);
                        bc.Gender = Utils.GetStringValue(drBC["Gender"]);
                        bc.dateOfBirth = Utils.GetStringValue(drBC["dateOfBirth"]);
                        bc.DOB = Utils.GetDateTimeValue(drBC["DOB"]);
                        bc.FatherName = Utils.GetStringValue(drBC["FatherName"]);
                        bc.SpouseName = Utils.GetStringValue(drBC["SpouseName"]);
                        bc.Category = Utils.GetStringValue(drBC["Category"]);

                        bc.Handicap = Utils.GetBoolValue(drBC["PhysicallyHandicap"]) ? "Yes" : "No";
                        bc.PhoneNumber1 = Utils.GetStringValue(drBC["PhoneNumber"]);
                        bc.PhoneNumber2 = Utils.GetStringValue(drBC["PhoneNumber1"]);
                        bc.PhoneNumber3 = Utils.GetStringValue(drBC["PhoneNumber2"]);
                        bc.ContactPerson = Utils.GetStringValue(drBC["ContactPerson"]);
                        bc.ContactDesignation = Utils.GetStringValue(drBC["ContactDesignation"]);
                        bc.NoofComplaint = Utils.GetStringValue(drBC["NoofComplaint"]);
                        bc.Email = Utils.GetStringValue(drBC["Email"]);
                        bc.AadharCard = Utils.GetStringValue(drBC["AadharCard"]);
                        bc.PanCard = Utils.GetStringValue(drBC["PanCard"]);
                        bc.VoterCard = Utils.GetStringValue(drBC["VotersIdCard"]);
                        bc.DriverLicense = Utils.GetStringValue(drBC["DriverLicense"]);
                        bc.NregaCard = Utils.GetStringValue(drBC["NREGACard"]);
                        bc.RationCard = Utils.GetStringValue(drBC["RationCard"]);
                        bc.State = Utils.GetStringValue(drBC["AddressState"]);
                        bc.City = Utils.GetStringValue(drBC["AddressCity"]);
                        bc.Village = Utils.GetStringValue(drBC["village"]);
                        bc.Village = Utils.GetStringValue(drBC["village"]);
                        bc.Latitude = Utils.GetStringValue(drBC["Latitude"]);
                        bc.Longitude = Utils.GetStringValue(drBC["Longitude"]);
                        bc.District = Utils.GetStringValue(drBC["AddressDistrict"]);
                        bc.Subdistrict = Utils.GetStringValue(drBC["AddressSubdistrict"]);
                        bc.Area = Utils.GetStringValue(drBC["AddressArea"]);
                        bc.PinCode = Utils.GetStringValue(drBC["PinCode"]);
                        bc.AlternateOccupationType = Utils.GetStringValue(drBC["AlternateOccupationType"]);
                        bc.AlternateOccupationDetail = Utils.GetStringValue(drBC["AlternateOccupationDetail"]);
                        bc.UniqueIdentificationNumber = Utils.GetStringValue(drBC["UniqueIdentificationNumber"]);
                        bc.BankReferenceNumber = Utils.GetStringValue(drBC["BankReferenceNumber"]);
                        bc.Qualification = Utils.GetStringValue(drBC["EducationalQualification"]);
                        bc.OtherQualification = Utils.GetStringValue(drBC["OtherQualification"]);
                        bc.isAllocated = Utils.GetBoolValue(drBC["IsAllocated"]);
                        bc.Status = Utils.GetStringValue(drBC["Status"]);
                        bc.CorporateId = Utils.GetIntValue(drBC["CorporateId"]);

                        bc.CreatedOn = Utils.GetDateTimeValue(drBC["CreatedOn"]);
                        bc.CreatedBy = Utils.GetIntValue(drBC["CreatedBy"]);
                        bc.terminateBc = Utils.GetIntValue(drBC["terminateBc"]);


                        bc.IsBlackListed = Utils.GetBoolValue(drBC["IsBlackListed"]);
                        bc.BlacklistDate = Utils.GetDateTimeValue(drBC["BlackListedDate"]);
                        bc.BlackListReason = Utils.GetStringValue(drBC["BlackListedReason"]);
                        bc.BlackListApprovalNumber = Utils.GetStringValue(drBC["BlackListedApprovalNumber"]);

                        if (bc.isAllocated)
                        {
                            bc.AppointmentDate = Utils.GetDateTimeValue(drBC["AppointmentDate"]);
                            bc.AllocationIFSCCode = Utils.GetStringValue(drBC["AllocationIFSCCode"]);
                            bc.AllocationBankId = Utils.GetIntValue(drBC["AllocationBankId"]);
                            bc.AllocationBranchId = Utils.GetIntValue(drBC["AllocationBranchId"]);
                            bc.BCType = Utils.GetStringValue(drBC["BCType"]);
                            bc.WorkingDays = Utils.GetIntValue(drBC["WorkingDays"]);
                            bc.WorkingHours = Utils.GetIntValue(drBC["WorkingHours"]);
                            bc.PLPostalAddress = Utils.GetStringValue(drBC["PLPostalAddress"]);
                            bc.PLVillageCode = Utils.GetStringValue(drBC["PLVillageCode"]);
                            bc.PLVillageDetail = Utils.GetStringValue(drBC["PLVillageDetail"]);
                            bc.PLTaluk = Utils.GetStringValue(drBC["PLTaluk"]);
                            bc.PLDistrictId = Utils.GetIntValue(drBC["PLDistrictId"]);
                            bc.PLStateId = Utils.GetIntValue(drBC["PLStateId"]);
                            bc.PLPinCode = Utils.GetStringValue(drBC["PLPinCode"]);
                            bc.MinimumCashHandlingLimit = Utils.GetDoubleValue(drBC["MinimumCashHandlingLimit"]);
                            bc.MonthlyFixedRenumeration = Utils.GetDoubleValue(drBC["MonthlyFixedRenumeration"]);
                            bc.MonthlyVariableRenumeration = Utils.GetDoubleValue(drBC["MonthlyVariableRenumeration"]);



                        }

                        if (allBCs == null)
                            allBCs = new List<BankCorrespondence>();

                        allBCs.Add(bc);



                    }  //for loop


                    //Set Datatable for certs
                    //Loop over rows
                    // Create a certificate object
                    //BankCorresponce bc =  allBCs.FirstOrDefault(b=>b.BankCorrespondId==)
                    // if (bc.Ce == null)
                    //bc.Ce = new     
                    // bc .Add(certificate object)

                    //TABLE [1] - BCDEVS
                    //DataTable dtdevice = dsBC.Tables[2];
                    DataTable dtdevice = dsBC.Tables[1];
                    for (int cnt = 0; cnt < dtdevice.Rows.Count; cnt++)
                    {
                        DataRow drdevice = dtdevice.Rows[cnt];

                        device = new BCDevices();

                        device.DeviceId = Utils.GetIntValue(drdevice["DeviceId"]);
                        device.Device = Utils.GetStringValue(drdevice["Device"]);
                        device.GivenOn = Utils.GetDateTimeValue(drdevice["GivenOn"]);
                        device.BCId = Utils.GetIntValue(drdevice["BCId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == device.BCId);
                        if (bc != null)
                        {
                            if (bc.Devices == null)
                                bc.Devices = new List<BCDevices>();
                            bc.Devices.Add(device);
                        }
                    }
                    //TABLE[2] - PREVEXP
                    //DataTable dtExp = dsBC.Tables[3];
                    DataTable dtExp = dsBC.Tables[2];
                    for (int cnt = 0; cnt < dtExp.Rows.Count; cnt++)
                    {
                        DataRow drExp = dtExp.Rows[cnt];
                        preexp = new PreviousExperiene();

                        preexp.ExperienceId = Utils.GetIntValue(drExp["ExperienceId"]);
                        preexp.BankId = Utils.GetIntValue(drExp["BankId"]);
                        preexp.BankName = Utils.GetStringValue(drExp["BankName"]);
                        preexp.BranchName = Utils.GetStringValue(drExp["BranchName"]);
                        preexp.Branchid = Utils.GetIntValue(drExp["Branchid"]);
                        preexp.FromDate = Utils.GetDateTimeValue(drExp["FromDate"]);
                        preexp.ToDate = Utils.GetDateTimeValue(drExp["ToDate"]);
                        preexp.Reason = Utils.GetStringValue(drExp["Reasons"]);
                        preexp.oBankName = Utils.GetStringValue(drExp["oBankName"]);
                        preexp.oBranchName = Utils.GetStringValue(drExp["oBranchName"]);
                        preexp.BankCorrespondenceId = Utils.GetIntValue(drExp["BankCorrespondenceId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == preexp.BankCorrespondenceId);
                        if (bc != null)
                        {
                            if (bc.PreviousExperience == null)
                                bc.PreviousExperience = new List<PreviousExperiene>();
                            bc.PreviousExperience.Add(preexp);
                        }
                    }

                    //TABLE[3] - CERT
                    //DataTable dtCer = dsBC.Tables[1];
                    DataTable dtCer = dsBC.Tables[3];
                    for (int cnt = 0; cnt < dtCer.Rows.Count; cnt++)
                    {
                        DataRow drCer = dtCer.Rows[cnt];
                        bccer = new BCCertifications();

                        bccer.CertificationId = Utils.GetIntValue(drCer["CertificationId"]);
                        bccer.DateOfPassing = Utils.GetDateTimeValue(drCer["PassingDate"]);
                        bccer.InstituteId = Utils.GetIntValue(drCer["InstituteId"]);
                        bccer.CourseId = Utils.GetIntValue(drCer["CourseId"]);
                        bccer.InstituteName = Utils.GetStringValue(drCer["InstituteName"]);
                        bccer.CourseName = Utils.GetStringValue(drCer["CourseName"]);
                        bccer.Grade = Utils.GetStringValue(drCer["Grade"]);
                        bccer.BankCorresponenceId = Utils.GetIntValue(drCer["BankCorresponenceId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == bccer.BankCorresponenceId);
                        if (bc != null)
                        {
                            if (bc.Certifications == null)
                                bc.Certifications = new List<BCCertifications>();
                            bc.Certifications.Add(bccer);
                        }
                    }


                    //TABLE[4] - CONNDET                  
                    //DataTable dtcondet = dsBC.Tables[4];
                    DataTable dtcondet = dsBC.Tables[4];
                    for (int cnt = 0; cnt < dtcondet.Rows.Count; cnt++)
                    {
                        DataRow drcondet = dtcondet.Rows[cnt];
                        condet = new ConnectivityDetails();

                        condet.ConnectivityId = Utils.GetIntValue(drcondet["ConnectivityId"]);
                        condet.ConnectivityMode = Utils.GetStringValue(drcondet["ConnectivityMode"]);
                        condet.ConnectivityProvider = Utils.GetStringValue(drcondet["ConnectivityProvider"]);
                        condet.ContactNumber = Utils.GetStringValue(drcondet["ContactNumber"]);
                        condet.BCId = Utils.GetIntValue(drcondet["BCId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == condet.BCId);
                        if (bc != null)
                        {
                            if (bc.ConnectivityDetails == null)
                                bc.ConnectivityDetails = new List<ConnectivityDetails>();
                            bc.ConnectivityDetails.Add(condet);
                        }
                    }


                    //TABLE[5] - OPRAREA
                    //DataTable dtArea = dsBC.Tables[5];
                    DataTable dtArea = dsBC.Tables[5];
                    for (int cnt = 0; cnt < dtArea.Rows.Count; cnt++)
                    {
                        DataRow drArea = dtArea.Rows[cnt];
                        area = new OperationalAreas();

                        area.AreaId = Utils.GetIntValue(drArea["AreaId"]);
                        area.VillageCode = Utils.GetStringValue(drArea["VillageCode"]);
                        area.VillageDetail = Utils.GetStringValue(drArea["VillageDetail"]);
                        area.BCId = Utils.GetIntValue(drArea["BCId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == area.BCId);
                        if (bc != null)
                        {
                            if (bc.OperationalAreas == null)
                                bc.OperationalAreas = new List<OperationalAreas>();
                            bc.OperationalAreas.Add(area);
                        }
                    }



                    //TABLE[5] - OPRAREA
                    //DataTable dtArea = dsBC.Tables[5];
                    DataTable dtSSADetails = dsBC.Tables[7];
                    for (int cnt = 0; cnt < dtSSADetails.Rows.Count; cnt++)
                    {
                        DataRow dtSSADetailss = dtSSADetails.Rows[cnt];
                        ssd = new SsaDetails();

                        ssd.Ssa = Utils.GetStringValue(dtSSADetailss["ssa"]);
                        ssd.Village = Utils.GetStringValue(dtSSADetailss["village"]);
                        ssd.State = Utils.GetStringValue(dtSSADetailss["StateId"]);
                        ssd.StateName = Utils.GetStringValue(dtSSADetailss["StateName"]);
                        ssd.StateCode = Utils.GetStringValue(dtSSADetailss["StateCode"]);
                        ssd.District = Utils.GetStringValue(dtSSADetailss["DistrictId"]);
                        ssd.DistrictName = Utils.GetStringValue(dtSSADetailss["DistrictName"]);
                        ssd.DistrictCode = Utils.GetStringValue(dtSSADetailss["DistrictCode"]);
                        ssd.SubDistrict = Utils.GetStringValue(dtSSADetailss["SubDistrictId"]);
                        ssd.SubDistrictName = Utils.GetStringValue(dtSSADetailss["SubDistrictName"]);
                        ssd.SubDistrictCode = Utils.GetStringValue(dtSSADetailss["SubDistrictCode"]);
                        ssd.bcid = Utils.GetStringValue(dtSSADetailss["bcid"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == Convert.ToInt32(ssd.bcid));
                        if (bc != null)
                        {
                            if (bc.SsaDetails == null)
                                bc.SsaDetails = new List<SsaDetails>();
                            bc.SsaDetails.Add(ssd);
                        }
                    }




                    //TABLE[6] - BCPROD
                    //DataTable dtprod = dsBC.Tables[6];
                    DataTable dtprod = dsBC.Tables[6];
                    for (int cnt = 0; cnt < dtprod.Rows.Count; cnt++)
                    {
                        DataRow drprod = dtprod.Rows[cnt];
                        prod = new BCProducts();

                        prod.ProductId = Utils.GetIntValue(drprod["ProductId"]);
                        prod.ProductName = Utils.GetStringValue(drprod["ProductName"]);
                        prod.BCId = Utils.GetIntValue(drprod["BCId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == prod.BCId);
                        if (bc != null)
                        {
                            if (bc.Products == null)
                                bc.Products = new List<BCProducts>();
                            bc.Products.Add(prod);
                        }
                    }

                    //TABLE[7] - BCPROD
                    //DataTable commission = dsBC.Tables[6];
                    //DataTable dtcommission = dsBC.Tables[7];
                    //for (int cnt = 0; cnt < dtcommission.Rows.Count; cnt++)
                    //{
                    //    DataRow drdtcommission = dtcommission.Rows[cnt];
                    //    commissions = new BCProducts();

                    //    prod.ProductId = Utils.GetIntValue(drprod["ProductId"]);
                    //    prod.ProductName = Utils.GetStringValue(drprod["ProductName"]);
                    //    prod.BCId = Utils.GetIntValue(drprod["BCId"]);

                    //    bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == prod.BCId);
                    //    if (bc != null)
                    //    {
                    //        if (bc.Products == null)
                    //            bc.Products = new List<BCProducts>();
                    //        bc.Products.Add(prod);
                    //    }
                    //}

                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return allBCs;
        }

        public List<BankCorrespondence> GetprospectiveRegistry(int? Id)
        {
            List<BankCorrespondence> allBCs = null;
            BankCorrespondence bc = null;
            BCCertifications bccer = null;
            PreviousExperiene preexp = null;
            try
            {
                DataSet dsBC = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@ProspectiveBCRegistry", Id));

                DBHelper.ExecProcAndFillDataSet("sp_GetProspectiveBCRegistry", spParams, dsBC, conn);

                if (dsBC != null && dsBC.Tables.Count > 0)
                {
                    //TABLE[0] - Prospective BC Registry
                    DataTable dtBC = dsBC.Tables[0];
                    for (int cnt = 0; cnt < dtBC.Rows.Count; cnt++)
                    {
                        DataRow drBC = dtBC.Rows[cnt];

                        bc = new BankCorrespondence();

                        bc.BankCorrespondId = Utils.GetIntValue(drBC["ProspectiveBCRegistry"]);

                        bc.Name = Utils.GetStringValue(drBC["FirstName"]);
                        bc.ImagePath = Utils.GetStringValue(drBC["ImagePath"]);
                        bc.Gender = Utils.GetStringValue(drBC["Gender"]);
                        bc.dateOfBirth = Utils.GetStringValue(drBC["dateOfBirth"]);
                        bc.DOB = Utils.GetDateTimeValue(drBC["DOB"]);
                        bc.FatherName = Utils.GetStringValue(drBC["FatherName"]);
                        bc.SpouseName = Utils.GetStringValue(drBC["SpouseName"]);
                   //     bc.Category = Utils.GetStringValue(drBC["Category"]);

                    //    bc.Handicap = Utils.GetBoolValue(drBC["PhysicallyHandicap"]) ? "Yes" : "No";
                        bc.PhoneNumber1 = Utils.GetStringValue(drBC["PhoneNumber"]);
                        bc.PhoneNumber2 = Utils.GetStringValue(drBC["PhoneNumber1"]);
                        bc.PhoneNumber3 = Utils.GetStringValue(drBC["PhoneNumber2"]);
                        bc.ContactPerson = Utils.GetStringValue(drBC["PhoneNumber3"]);
                    //    bc.ContactDesignation = Utils.GetStringValue(drBC["ContactDesignation"]);
                    //    bc.NoofComplaint = Utils.GetStringValue(drBC["NoofComplaint"]);
                        bc.Email = Utils.GetStringValue(drBC["Email"]);
                        bc.AadharCard = Utils.GetStringValue(drBC["AadharCard"]);
                        bc.PanCard = Utils.GetStringValue(drBC["PanCard"]);
                        bc.VoterCard = Utils.GetStringValue(drBC["VotersIdCard"]);
                        bc.DriverLicense = Utils.GetStringValue(drBC["DriverLicense"]);
                        bc.NregaCard = Utils.GetStringValue(drBC["NREGACard"]);
                        bc.RationCard = Utils.GetStringValue(drBC["RationCard"]);
                        //bc.State = Utils.GetStringValue(drBC["AddressState"]);
                        //bc.City = Utils.GetStringValue(drBC["AddressCity"]);
                        //bc.Village = Utils.GetStringValue(drBC["village"]);
                        //bc.Village = Utils.GetStringValue(drBC["village"]);
                        //bc.Latitude = Utils.GetStringValue(drBC["Latitude"]);
                        //bc.Longitude = Utils.GetStringValue(drBC["Longitude"]);
                        //bc.District = Utils.GetStringValue(drBC["AddressDistrict"]);
                        //bc.Subdistrict = Utils.GetStringValue(drBC["AddressSubdistrict"]);
                        //bc.Area = Utils.GetStringValue(drBC["AddressArea"]);
                        //bc.PinCode = Utils.GetStringValue(drBC["PinCode"]);
                        bc.AlternateOccupationType = Utils.GetStringValue(drBC["AlternateOccupationType"]);
                        bc.AlternateOccupationDetail = Utils.GetStringValue(drBC["AlternateOccupationDetail"]);
                        bc.UniqueIdentificationNumber = Utils.GetStringValue(drBC["UniqueIdentificationNumber"]);
                        // bc.BankReferenceNumber = Utils.GetStringValue(drBC["BankReferenceNumber"]);
                        bc.Qualification = Utils.GetStringValue(drBC["EducationalQualification"]);
                        bc.OtherQualification = Utils.GetStringValue(drBC["OtherQualification"]);
                        //bc.isAllocated = Utils.GetBoolValue(drBC["IsAllocated"]);
                        //bc.Status = Utils.GetStringValue(drBC["Status"]);
                        //bc.CorporateId = Utils.GetIntValue(drBC["CorporateId"]);

                        bc.CreatedOn = Utils.GetDateTimeValue(drBC["CreatedOn"]);
                        bc.CreatedBy = Utils.GetIntValue(drBC["CreatedBy"]);
                       // bc.terminateBc = Utils.GetIntValue(drBC["terminateBc"]);


                        //bc.IsBlackListed = Utils.GetBoolValue(drBC["IsBlackListed"]);
                        //bc.BlacklistDate = Utils.GetDateTimeValue(drBC["BlackListedDate"]);
                        //bc.BlackListReason = Utils.GetStringValue(drBC["BlackListedReason"]);
                        //bc.BlackListApprovalNumber = Utils.GetStringValue(drBC["BlackListedApprovalNumber"]);

                        if (allBCs == null)
                            allBCs = new List<BankCorrespondence>();

                        allBCs.Add(bc);



                    }  //for loop

                    
                    //TABLE[2] - PREVEXP
                    //DataTable dtExp = dsBC.Tables[3];
                    DataTable dtExp = dsBC.Tables[1];
                    for (int cnt = 0; cnt < dtExp.Rows.Count; cnt++)
                    {
                        DataRow drExp = dtExp.Rows[cnt];
                        preexp = new PreviousExperiene();

                        preexp.ExperienceId = Utils.GetIntValue(drExp["ExperienceId"]);
                        preexp.BankId = Utils.GetIntValue(drExp["BankId"]);
                        preexp.BankName = Utils.GetStringValue(drExp["BankName"]);
                        preexp.BranchName = Utils.GetStringValue(drExp["BranchName"]);
                        preexp.Branchid = Utils.GetIntValue(drExp["Branchid"]);
                        preexp.FromDate = Utils.GetDateTimeValue(drExp["FromDate"]);
                        preexp.ToDate = Utils.GetDateTimeValue(drExp["ToDate"]);
                        preexp.Reason = Utils.GetStringValue(drExp["Reasons"]);
                        preexp.oBankName = Utils.GetStringValue(drExp["oBankName"]);
                        preexp.oBranchName = Utils.GetStringValue(drExp["oBranchName"]);
                        preexp.BankCorrespondenceId = Utils.GetIntValue(drExp["ProspectiveBCRegistryid"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == preexp.BankCorrespondenceId);
                        if (bc != null)
                        {
                            if (bc.PreviousExperience == null)
                                bc.PreviousExperience = new List<PreviousExperiene>();
                            bc.PreviousExperience.Add(preexp);
                        }
                    }

                    //TABLE[3] - CERT
                    //DataTable dtCer = dsBC.Tables[1];
                    DataTable dtCer = dsBC.Tables[2];
                    for (int cnt = 0; cnt < dtCer.Rows.Count; cnt++)
                    {
                        DataRow drCer = dtCer.Rows[cnt];
                        bccer = new BCCertifications();

                        bccer.CertificationId = Utils.GetIntValue(drCer["CertificationId"]);
                        bccer.DateOfPassing = Utils.GetDateTimeValue(drCer["PassingDate"]);
                        bccer.InstituteId = Utils.GetIntValue(drCer["InstituteId"]);
                        bccer.CourseId = Utils.GetIntValue(drCer["CourseId"]);
                        bccer.InstituteName = Utils.GetStringValue(drCer["InstituteName"]);
                        bccer.CourseName = Utils.GetStringValue(drCer["CourseName"]);
                        bccer.Grade = Utils.GetStringValue(drCer["Grade"]);
                        bccer.BankCorresponenceId = Utils.GetIntValue(drCer["ProspectiveBCRegistryid"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == bccer.BankCorresponenceId);
                        if (bc != null)
                        {
                            if (bc.Certifications == null)
                                bc.Certifications = new List<BCCertifications>();
                            bc.Certifications.Add(bccer);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return allBCs;
        }
        public List<BankCorrespondence> GetBankCorrespondencebyAdhaar(string adhaar)
        {
            List<BankCorrespondence> allBCs = null;
            BankCorrespondence bc = null;
            BCCertifications bccer = null;
            PreviousExperiene preexp = null;
            OperationalAreas area = null;
            BCDevices device = null;
            ConnectivityDetails condet = null;
            BCProducts prod = null;

            try
            {
                DataSet dsBC = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@adhaarNo", adhaar));

                DBHelper.ExecProcAndFillDataSet("sp_GetBankCorrespondsByAadhar", spParams, dsBC, conn);

                if (dsBC != null && dsBC.Tables.Count > 0)
                {
                    //TABLE[0] - BCCORRES
                    DataTable dtBC = dsBC.Tables[0];
                    for (int cnt = 0; cnt < dtBC.Rows.Count; cnt++)
                    {
                        DataRow drBC = dtBC.Rows[cnt];

                        bc = new BankCorrespondence();

                        bc.BankCorrespondId = Utils.GetIntValue(drBC["BankCorrespondId"]);

                        bc.Name = Utils.GetStringValue(drBC["Name"]);
                        bc.ImagePath = Utils.GetStringValue(drBC["ImagePath"]);
                        bc.Gender = Utils.GetStringValue(drBC["Gender"]);
                        bc.DOB = Utils.GetDateTimeValue(drBC["DOB"]);
                        bc.FatherName = Utils.GetStringValue(drBC["FatherName"]);
                        bc.SpouseName = Utils.GetStringValue(drBC["SpouseName"]);
                        bc.Category = Utils.GetStringValue(drBC["Category"]);

                        bc.Handicap = Utils.GetBoolValue(drBC["PhysicallyHandicap"]) ? "Yes" : "No";
                        bc.PhoneNumber1 = Utils.GetStringValue(drBC["PhoneNumber"]);
                        bc.PhoneNumber2 = Utils.GetStringValue(drBC["PhoneNumber1"]);
                        bc.PhoneNumber3 = Utils.GetStringValue(drBC["PhoneNumber2"]);
                        bc.Email = Utils.GetStringValue(drBC["Email"]);
                        bc.AadharCard = Utils.GetStringValue(drBC["AadharCard"]);
                        bc.PanCard = Utils.GetStringValue(drBC["PanCard"]);
                        bc.VoterCard = Utils.GetStringValue(drBC["VotersIdCard"]);
                        bc.DriverLicense = Utils.GetStringValue(drBC["DriverLicense"]);
                        bc.NregaCard = Utils.GetStringValue(drBC["NREGACard"]);
                        bc.RationCard = Utils.GetStringValue(drBC["RationCard"]);
                        bc.State = Utils.GetStringValue(drBC["AddressState"]);
                        bc.City = Utils.GetStringValue(drBC["AddressCity"]);
                        bc.District = Utils.GetStringValue(drBC["AddressDistrict"]);
                        bc.Subdistrict = Utils.GetStringValue(drBC["AddressSubdistrict"]);
                        bc.Area = Utils.GetStringValue(drBC["AddressArea"]);
                        bc.PinCode = Utils.GetStringValue(drBC["PinCode"]);
                        bc.AlternateOccupationType = Utils.GetStringValue(drBC["AlternateOccupationType"]);
                        bc.AlternateOccupationDetail = Utils.GetStringValue(drBC["AlternateOccupationDetail"]);
                        bc.UniqueIdentificationNumber = Utils.GetStringValue(drBC["UniqueIdentificationNumber"]);
                        bc.BankReferenceNumber = Utils.GetStringValue(drBC["BankReferenceNumber"]);
                        bc.Qualification = Utils.GetStringValue(drBC["EducationalQualification"]);
                        bc.OtherQualification = Utils.GetStringValue(drBC["OtherQualification"]);
                        bc.isAllocated = Utils.GetBoolValue(drBC["IsAllocated"]);
                        bc.Status = Utils.GetStringValue(drBC["Status"]);
                        bc.CorporateId = Utils.GetIntValue(drBC["CorporateId"]);

                        bc.CreatedOn = Utils.GetDateTimeValue(drBC["CreatedOn"]);
                        bc.CreatedBy = Utils.GetIntValue(drBC["CreatedBy"]);



                        bc.IsBlackListed = Utils.GetBoolValue(drBC["IsBlackListed"]);
                        bc.BlacklistDate = Utils.GetDateTimeValue(drBC["BlackListedDate"]);
                        bc.BlackListReason = Utils.GetStringValue(drBC["BlackListedReason"]);
                        bc.BlackListApprovalNumber = Utils.GetStringValue(drBC["BlackListedApprovalNumber"]);

                        if (bc.isAllocated)
                        {
                            bc.AppointmentDate = Utils.GetDateTimeValue(drBC["AppointmentDate"]);
                            bc.AllocationIFSCCode = Utils.GetStringValue(drBC["AllocationIFSCCode"]);
                            bc.AllocationBankId = Utils.GetIntValue(drBC["AllocationBankId"]);
                            bc.AllocationBranchId = Utils.GetIntValue(drBC["AllocationBranchId"]);
                            bc.BCType = Utils.GetStringValue(drBC["BCType"]);
                            bc.WorkingDays = Utils.GetIntValue(drBC["WorkingDays"]);
                            bc.WorkingHours = Utils.GetIntValue(drBC["WorkingHours"]);
                            bc.PLPostalAddress = Utils.GetStringValue(drBC["PLPostalAddress"]);
                            bc.PLVillageCode = Utils.GetStringValue(drBC["PLVillageCode"]);
                            bc.PLVillageDetail = Utils.GetStringValue(drBC["PLVillageDetail"]);
                            bc.PLTaluk = Utils.GetStringValue(drBC["PLTaluk"]);
                            bc.PLDistrictId = Utils.GetIntValue(drBC["PLDistrictId"]);
                            bc.PLStateId = Utils.GetIntValue(drBC["PLStateId"]);
                            bc.PLPinCode = Utils.GetStringValue(drBC["PLPinCode"]);
                            bc.MinimumCashHandlingLimit = Utils.GetDoubleValue(drBC["MinimumCashHandlingLimit"]);
                            bc.MonthlyFixedRenumeration = Utils.GetDoubleValue(drBC["MonthlyFixedRenumeration"]);
                            bc.MonthlyVariableRenumeration = Utils.GetDoubleValue(drBC["MonthlyVariableRenumeration"]);



                        }

                        if (allBCs == null)
                            allBCs = new List<BankCorrespondence>();

                        allBCs.Add(bc);



                    }  //for loop


                    //Set Datatable for certs
                    //Loop over rows
                    // Create a certificate object
                    //BankCorresponce bc =  allBCs.FirstOrDefault(b=>b.BankCorrespondId==)
                    // if (bc.Ce == null)
                    //bc.Ce = new     
                    // bc .Add(certificate object)

                    //TABLE [1] - BCDEVS
                    //DataTable dtdevice = dsBC.Tables[2];
                    DataTable dtdevice = dsBC.Tables[1];
                    for (int cnt = 0; cnt < dtdevice.Rows.Count; cnt++)
                    {
                        DataRow drdevice = dtdevice.Rows[cnt];

                        device = new BCDevices();

                        device.DeviceId = Utils.GetIntValue(drdevice["DeviceId"]);
                        device.Device = Utils.GetStringValue(drdevice["Device"]);
                        device.GivenOn = Utils.GetDateTimeValue(drdevice["GivenOn"]);
                        device.BCId = Utils.GetIntValue(drdevice["BCId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == device.BCId);
                        if (bc != null)
                        {
                            if (bc.Devices == null)
                                bc.Devices = new List<BCDevices>();
                            bc.Devices.Add(device);
                        }
                    }
                    //TABLE[2] - PREVEXP
                    //DataTable dtExp = dsBC.Tables[3];
                    DataTable dtExp = dsBC.Tables[2];
                    for (int cnt = 0; cnt < dtExp.Rows.Count; cnt++)
                    {
                        DataRow drExp = dtExp.Rows[cnt];
                        preexp = new PreviousExperiene();

                        preexp.ExperienceId = Utils.GetIntValue(drExp["ExperienceId"]);
                        preexp.BankId = Utils.GetIntValue(drExp["BankId"]);
                        preexp.Branchid = Utils.GetIntValue(drExp["Branchid"]);
                        preexp.BankName = Utils.GetStringValue(drExp["BankName"]);
                        preexp.BranchName = Utils.GetStringValue(drExp["BranchName"]);
                        preexp.FromDate = Utils.GetDateTimeValue(drExp["FromDate"]);
                        preexp.ToDate = Utils.GetDateTimeValue(drExp["ToDate"]);
                        preexp.Reason = Utils.GetStringValue(drExp["Reasons"]);
                        preexp.BankCorrespondenceId = Utils.GetIntValue(drExp["BankCorrespondenceId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == preexp.BankCorrespondenceId);
                        if (bc != null)
                        {
                            if (bc.PreviousExperience == null)
                                bc.PreviousExperience = new List<PreviousExperiene>();
                            bc.PreviousExperience.Add(preexp);
                        }
                    }

                    //TABLE[3] - CERT
                    //DataTable dtCer = dsBC.Tables[1];
                    DataTable dtCer = dsBC.Tables[3];
                    for (int cnt = 0; cnt < dtCer.Rows.Count; cnt++)
                    {
                        DataRow drCer = dtCer.Rows[cnt];
                        bccer = new BCCertifications();

                        bccer.CertificationId = Utils.GetIntValue(drCer["CertificationId"]);
                        bccer.DateOfPassing = Utils.GetDateTimeValue(drCer["PassingDate"]);
                        bccer.InstituteId = Utils.GetIntValue(drCer["InstituteId"]);
                        bccer.CourseId = Utils.GetIntValue(drCer["CourseId"]);
                        bccer.InstituteName = Utils.GetStringValue(drCer["InstituteName"]);
                        bccer.CourseName = Utils.GetStringValue(drCer["CourseName"]);
                        bccer.Grade = Utils.GetStringValue(drCer["Grade"]);
                        bccer.BankCorresponenceId = Utils.GetIntValue(drCer["BankCorresponenceId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == bccer.BankCorresponenceId);
                        if (bc != null)
                        {
                            if (bc.Certifications == null)
                                bc.Certifications = new List<BCCertifications>();
                            bc.Certifications.Add(bccer);
                        }
                    }


                    //TABLE[4] - CONNDET                  
                    //DataTable dtcondet = dsBC.Tables[4];
                    DataTable dtcondet = dsBC.Tables[4];
                    for (int cnt = 0; cnt < dtcondet.Rows.Count; cnt++)
                    {
                        DataRow drcondet = dtcondet.Rows[cnt];
                        condet = new ConnectivityDetails();

                        condet.ConnectivityId = Utils.GetIntValue(drcondet["ConnectivityId"]);
                        condet.ConnectivityMode = Utils.GetStringValue(drcondet["ConnectivityMode"]);
                        condet.ConnectivityProvider = Utils.GetStringValue(drcondet["ConnectivityProvider"]);
                        condet.ContactNumber = Utils.GetStringValue(drcondet["ContactNumber"]);
                        condet.BCId = Utils.GetIntValue(drcondet["BCId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == condet.BCId);
                        if (bc != null)
                        {
                            if (bc.ConnectivityDetails == null)
                                bc.ConnectivityDetails = new List<ConnectivityDetails>();
                            bc.ConnectivityDetails.Add(condet);
                        }
                    }


                    //TABLE[5] - OPRAREA
                    //DataTable dtArea = dsBC.Tables[5];
                    DataTable dtArea = dsBC.Tables[5];
                    for (int cnt = 0; cnt < dtArea.Rows.Count; cnt++)
                    {
                        DataRow drArea = dtArea.Rows[cnt];
                        area = new OperationalAreas();

                        area.AreaId = Utils.GetIntValue(drArea["AreaId"]);
                        area.VillageCode = Utils.GetStringValue(drArea["VillageCode"]);
                        area.VillageDetail = Utils.GetStringValue(drArea["VillageDetail"]);
                        area.BCId = Utils.GetIntValue(drArea["BCId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == area.BCId);
                        if (bc != null)
                        {
                            if (bc.OperationalAreas == null)
                                bc.OperationalAreas = new List<OperationalAreas>();
                            bc.OperationalAreas.Add(area);
                        }
                    }


                    //TABLE[6] - BCPROD
                    //DataTable dtprod = dsBC.Tables[6];
                    DataTable dtprod = dsBC.Tables[6];
                    for (int cnt = 0; cnt < dtprod.Rows.Count; cnt++)
                    {
                        DataRow drprod = dtprod.Rows[cnt];
                        prod = new BCProducts();

                        prod.ProductId = Utils.GetIntValue(drprod["ProductId"]);
                        prod.ProductName = Utils.GetStringValue(drprod["ProductName"]);
                        prod.BCId = Utils.GetIntValue(drprod["BCId"]);

                        bc = allBCs.FirstOrDefault(b => b.BankCorrespondId == prod.BCId);
                        if (bc != null)
                        {
                            if (bc.Products == null)
                                bc.Products = new List<BCProducts>();
                            bc.Products.Add(prod);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return allBCs;
        }

        public List<Bank> GetBanks(int? BankId, string userId)
        {
            List<Bank> allBanks = null;
            Bank bank = null;
            Branch branch = null;
            try {
                DataSet dsBank = new DataSet();
                SqlConnection conn = new SqlConnection(Constants.connString);
                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@BankId", BankId));
                spParams.Add(new SpParam("@userId", userId));
                DBHelper.ExecProcAndFillDataSet("sp_GetBank", spParams, dsBank, conn);
               
                if (dsBank != null && dsBank.Tables.Count > 0) {
                    DataTable dtBank = dsBank.Tables[0];
                    for (int cnt = 0; cnt < dtBank.Rows.Count; cnt++) {
                        DataRow drBank = dtBank.Rows[cnt];
                        bank = new Bank();
                        bank.BankId = Utils.GetIntValue(drBank["BankId"]);
                        bank.BankName = Utils.GetStringValue(drBank["BankName"]);
                        bank.Address = Utils.GetStringValue(drBank["Address"]);
                        bank.ContactNumber = Utils.GetStringValue(drBank["ContactNumber"]);
                        bank.Email = Utils.GetStringValue(drBank["Email"]);
                        if (allBanks == null)
                            allBanks = new List<Bank>();
                        allBanks.Add(bank);
                    }


                    DataTable dtCircle = dsBank.Tables[1];
                    Circle circ = null;
                    for (int cnt = 0; cnt < dtCircle.Rows.Count; cnt++) {
                        DataRow drcircle = dtCircle.Rows[cnt];
                        circ = new Circle();
                        circ.CircleId = Utils.GetIntValue(drcircle["CircleId"]);
                        circ.CircleName = Utils.GetStringValue(drcircle["CircleName"]);
                        circ.BankId = Utils.GetIntValue(drcircle["BankId"]);
                        bank = allBanks.FirstOrDefault(b => b.BankId == circ.BankId);
                        if (bank != null) {
                            if (bank.BankCircles == null)
                                bank.BankCircles = new List<Circle>();
                            bank.BankCircles.Add(circ);
                        }
                    }


                    DataTable dtZone = dsBank.Tables[2];
                    Zone zon = null;
                    for (int cnt = 0; cnt < dtZone.Rows.Count; cnt++) {
                        DataRow drzone = dtZone.Rows[cnt];
                        zon = new Zone();

                        zon.ZoneId = Utils.GetIntValue(drzone["ZoneId"]);
                        zon.ZoneName = Utils.GetStringValue(drzone["ZoneName"]);
                        zon.CircleId = Utils.GetIntValue(drzone["CircleId"]);
                        zon.CircleName = Utils.GetStringValue(drzone["CircleName"]);

                        Circle c = null;
                        foreach (Bank b in allBanks) {
                            if (b.BankCircles != null) {
                                c = b.BankCircles.FirstOrDefault(bc => bc.CircleId == zon.CircleId);
                                if (c != null) {
                                    if (c.CircleZones == null)
                                        c.CircleZones = new List<Zone>();
                                    c.CircleZones.Add(zon);
                                    break;
                                }
                            }
                        }
                    }


                    DataTable dtRegion = dsBank.Tables[3];
                    Region reg = null;
                    for (int cnt = 0; cnt < dtRegion.Rows.Count; cnt++) {
                        DataRow drRegion = dtRegion.Rows[cnt];
                        reg = new Region();

                        reg.RegionId = Utils.GetIntValue(drRegion["RegionId"]);
                        reg.RegionName = Utils.GetStringValue(drRegion["RegionName"]);
                        reg.ZoneId = Utils.GetIntValue(drRegion["ZoneId"]);
                        reg.ZoneName = Utils.GetStringValue(drRegion["ZoneName"]);
                        
                        Zone z = null;
                        bool found = false;
                        foreach (Bank b in allBanks) {
                            if (b.BankCircles != null) {
                                foreach (Circle c in b.BankCircles) {
                                    if (c.CircleZones != null) {
                                        z = c.CircleZones.FirstOrDefault(cz => cz.ZoneId == reg.ZoneId);
                                        if (z != null) {
                                            if (z.ZoneRegions == null)
                                                z.ZoneRegions = new List<Region>();
                                            z.ZoneRegions.Add(reg);
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (found)
                                break;
                        }

                    }


                    DataTable dtbranch = dsBank.Tables[4];
                    for (int cnt = 0; cnt < dtbranch.Rows.Count; cnt++) {
                        DataRow drbranch = dtbranch.Rows[cnt];
                        branch = new Branch();
                        branch.BranchId = Utils.GetIntValue(drbranch["BranchId"]);
                        branch.BranchCode = Utils.GetStringValue(drbranch["BranchCode"]);
                        branch.BranchName = Utils.GetStringValue(drbranch["BranchName"]);
                        branch.IFSCCode = Utils.GetStringValue(drbranch["IFSCCode"]);
                        branch.MICRCode = Utils.GetStringValue(drbranch["MICRCode"]);
                        branch.Address = Utils.GetStringValue(drbranch["Address"]);
                        branch.ContactNumber = Utils.GetStringValue(drbranch["ContactNumber"]);
                        branch.Email = Utils.GetStringValue(drbranch["Email"]);
                        branch.BankId = Utils.GetIntValue(drbranch["BankId"]);
                        branch.RegionId = Utils.GetIntValue(drbranch["RegionId"]);
                        branch.RegionName = Utils.GetStringValue(drbranch["RegionName"]);                        
                        branch.StateId = Utils.GetIntValue(drbranch["StateId"]);
                        branch.DistrictId = Utils.GetIntValue(drbranch["DistrictId"]);
                        branch.TalukaId = Utils.GetIntValue(drbranch["TalukaId"]);
                        branch.CityId = Utils.GetIntValue(drbranch["CityId"]);
                        branch.VillageId = Utils.GetIntValue(drbranch["VillageId"]);
                        branch.PinCode = Utils.GetStringValue(drbranch["PinCode"]);



                        bank = allBanks.FirstOrDefault(b => b.BankId == branch.BankId);
                        if (bank.Branches == null)
                            bank.Branches = new List<Branch>();
                        bank.Branches.Add(branch);

                        Region r = null;
                        bool found = false;
                        foreach (Bank b in allBanks) {
                            if (b.BankCircles != null) {
                                foreach (Circle c in b.BankCircles) {
                                    if (c.CircleZones != null) {
                                        foreach (Zone z in c.CircleZones) {
                                            if (z.ZoneRegions != null) {

                                                r = z.ZoneRegions.FirstOrDefault(zr => zr.RegionId == branch.RegionId);
                                                if (r != null) {
                                                    if (r.RegionBranches == null)
                                                        r.RegionBranches = new List<Branch>();
                                                    r.RegionBranches.Add(branch);
                                                    found = true;
                                                    break;
                                                }

                                            }
                                        }
                                    }
                                    if (found)
                                        break;
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                }
            }
            catch (Exception ex) {
                throw ex;
            }

            return allBanks;

        }

        public List<State> GetStates(int? stateId) {
            List<State> states = null;
            State state = null;
            District district = null;
            try {
                DataSet dataset_state = new DataSet();
                SqlConnection connection = new SqlConnection(Constants.connString);
                SpParamCollection paramCollection = new SpParamCollection();
                states = new List<State>();
                paramCollection.Add(new SpParam("@StateId", stateId));
                DBHelper.ExecProcAndFillDataSet("sp_GetStates", paramCollection, dataset_state);
                if (dataset_state != null & dataset_state.Tables.Count > 0) {

                    //populate states                   
                    DataTable stateTable = dataset_state.Tables[0];
                    states = new List<State>();
                    for (int i = 0; i < stateTable.Rows.Count; i++) {
                        state = new State();
                        DataRow stateRow = stateTable.Rows[i];
                        state.StateId = int.Parse(stateRow["StateId"].ToString());
                        state.StateName = stateRow["StateName"].ToString();
                        state.StateCode = stateRow["StateCode"].ToString();
                        states.Add(state);
                    }

                    //populate districts
                    DataTable districtTable = dataset_state.Tables[1];
                    foreach (State s in states) {
                        s.Districts = new List<District>();
                        for (int i = 0; i < districtTable.Rows.Count; i++) {
                            district = new District();
                            DataRow districtRow = districtTable.Rows[i];
                            district.DistrictId = int.Parse(districtRow["DistrictId"].ToString());
                            district.DistrictName = districtRow["DistrictName"].ToString();
                            district.StateId = int.Parse(districtRow["StateId"].ToString());
                            district.DistrictCode = districtRow["DistrictCode"].ToString();                            
                            if (s.StateId == district.StateId)
                                s.Districts.Add(district);
                        }
                    }

                    //populate cities
                    DataTable SubDistrictTable = dataset_state.Tables[2];
                    foreach (State s in states) {
                        foreach (District d in s.Districts) {
                            d.SubDistrict = new List<SubDistrict>();
                            for (int i = 0; i < SubDistrictTable.Rows.Count; i++)
                            {
                                DataRow cityRow = SubDistrictTable.Rows[i];
                                SubDistrict c = new SubDistrict();
                                c.SubDistrictId = int.Parse(cityRow["SubDistrictId"].ToString());
                                c.SubDistrictName = cityRow["SubDistrictName"].ToString();
                                c.SubDistrictCode = cityRow["SubDistrictCode"].ToString();
                                c.DistrictId = int.Parse(cityRow["DistrictId"].ToString());
                                if (c.DistrictId == d.DistrictId)
                                    d.SubDistrict.Add(c);                 
                            }
                        }
                    }

                    //Populate Talukas
                    //DataTable talukaTable = dataset_state.Tables[3];
                    //foreach (State s in states) {
                    //    foreach (District d in s.Districts) {
                    //        d.Talukas = new List<Taluka>();
                    //        for(int i = 0; i < talukaTable.Rows.Count; i++) {
                    //            DataRow talukaRow = talukaTable.Rows[i];
                    //            Taluka t = new Taluka();
                    //            t.DistrictId = int.Parse(talukaRow["DistrictId"].ToString());
                    //            t.TalukaName = talukaRow["TalukaName"].ToString();
                    //            t.TalukaId = int.Parse(talukaRow["TalukaId"].ToString());
                    //            if (t.DistrictId == d.DistrictId)
                    //                d.Talukas.Add(t);
                    //        }
                    //    }
                    //}

                    //Populate Villages
                    DataTable villageTable = dataset_state.Tables[3];
                    foreach (State s in states) {
                        foreach (District d in s.Districts) {
                            foreach (SubDistrict sd in d.SubDistrict)
                            {
                                d.Villages = new List<Village>();
                             for (int i = 0; i < villageTable.Rows.Count; i++) {
                                DataRow villageRow = villageTable.Rows[i];
                                Village t = new Village();
                                t.SubDistrictId = int.Parse(villageRow["SubDistrictId"].ToString());
                                t.VillageName = villageRow["VillageName"].ToString();
                                t.VillageId = int.Parse(villageRow["VillageId"].ToString());
                                t.VillageCode = villageRow["VillageCode"].ToString();
                                if (sd.SubDistrictId == t.SubDistrictId)
                                    d.Villages.Add(t);
                            }
                        }
                        }
                    }
                }
            }
            catch (Exception ex) {
                throw ex;
            }
            return states;
        }

        public List<Institutes> GetInstitutes(int? InstituteId) {
            List<Institutes> allInstitutes = null;
            Institutes institute = null;
            Courses course = null;
            try {
                DataSet dsInstitute = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@InstituteId", InstituteId));

                DBHelper.ExecProcAndFillDataSet("sp_GetInstitutesCourses", spParams, dsInstitute, conn);

                if (dsInstitute != null && dsInstitute.Tables.Count > 0) {
                    DataTable dtInstitute = dsInstitute.Tables[0];
                    for (int cnt = 0; cnt < dtInstitute.Rows.Count; cnt++) {
                        DataRow drInstitute = dtInstitute.Rows[cnt];
                        institute = new Institutes();

                        institute.InstituteId = Utils.GetIntValue(drInstitute["InstituteId"]);
                        institute.InstituteName = Utils.GetStringValue(drInstitute["InstituteName"]);

                        if (allInstitutes == null)
                            allInstitutes = new List<Institutes>();

                        allInstitutes.Add(institute);

                    }

                    DataTable dtcourse = dsInstitute.Tables[1];
                    for (int cnt = 0; cnt < dtcourse.Rows.Count; cnt++) {
                        DataRow drcourse = dtcourse.Rows[cnt];
                        course = new Courses();

                        course.CourseId = Utils.GetIntValue(drcourse["CourseId"]);
                        course.CourseName = Utils.GetStringValue(drcourse["CourseName"]);
                        course.InstituteId = Utils.GetIntValue(drcourse["InstituteId"]);


                        institute = allInstitutes.FirstOrDefault(b => b.InstituteId == course.InstituteId);
                        if (institute != null) {
                            if (institute.Courses == null)
                                institute.Courses = new List<Courses>();
                            institute.Courses.Add(course);
                        }
                    }
                }
            }
            catch (Exception ex) {
                throw ex;

            }

            return allInstitutes;

        }
        public List<Corporates> GetPanCard(string panno)
        {
            List<Corporates> allCorporates = null;
            Corporates corporate = null;
            try
            {
                DataSet dscorporate = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@panno", panno));

                DBHelper.ExecProcAndFillDataSet("sp_GetCorporatespanno", spParams, dscorporate, conn);

                if (dscorporate != null && dscorporate.Tables.Count > 0)
                {
                    DataTable dtcorporate = dscorporate.Tables[0];
                    for (int cnt = 0; cnt < dtcorporate.Rows.Count; cnt++)
                    {
                        DataRow drcorporate = dtcorporate.Rows[cnt];
                        corporate = new Corporates();

                        corporate.PanNo = Utils.GetStringValue(drcorporate["PanNo"]);


                        //TODO: Map BC to this corporate

                        if (allCorporates == null)
                            allCorporates = new List<Corporates>();

                        allCorporates.Add(corporate);

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return allCorporates;

        }
        public List<Corporates> GetCorporates(int? CorporateId)
        {
            List<Corporates> allCorporates = null;
            Corporates corporate = null;
            try
            {
                DataSet dscorporate = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@CorporateId", CorporateId));

                DBHelper.ExecProcAndFillDataSet("sp_GetCorporates", spParams, dscorporate, conn);

                if (dscorporate != null && dscorporate.Tables.Count > 0)
                {
                    DataTable dtcorporate = dscorporate.Tables[0];
                    for (int cnt = 0; cnt < dtcorporate.Rows.Count; cnt++)
                    {
                        DataRow drcorporate = dtcorporate.Rows[cnt];
                        corporate = new Corporates();

                        corporate.CorporateId = Utils.GetIntValue(drcorporate["CorporateId"]);
                        corporate.CorporateName = Utils.GetStringValue(drcorporate["CorporateName"]);
                        corporate.CorporateType = Utils.GetStringValue(drcorporate["CorporateType"]);
                        corporate.Address = Utils.GetStringValue(drcorporate["Address"]);
                        corporate.ContactNumber = Utils.GetStringValue(drcorporate["ContactNumber"]);
                        corporate.PanNo = Utils.GetStringValue(drcorporate["PanNo"]);
                        corporate.WebSite = Utils.GetStringValue(drcorporate["WebSite"]);
                        corporate.Email = Utils.GetStringValue(drcorporate["Email"]);
                        corporate.ContactPerson = Utils.GetStringValue(drcorporate["ContactPerson"]);
                        corporate.ContactDesignation = Utils.GetStringValue(drcorporate["ContactDesignation"]);
                        corporate.NoOfComplaint = Utils.GetIntValue(drcorporate["NoOfComplaint"]);

                        //TODO: Map BC to this corporate

                        if (allCorporates == null)
                            allCorporates = new List<Corporates>();

                        allCorporates.Add(corporate);

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return allCorporates;

        }
          public List<Bank> getbankRefNoDetail(string bankRefNo)
        {
            List<Bank> banks = new List<Bank>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();
                    DataSet resultSet = new DataSet();
                    param.Add(new SpParam("@BankReferenceNumber", bankRefNo));
                    DBHelper.ExecProcAndFillDataSet("sp_getbankRefNoDetail", param, resultSet);
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable drBank = resultSet.Tables[0];
                        foreach (DataRow row in drBank.Rows)
                        {
                            Bank bank = new Bank();
                            bank.BankReferenceNumber = row["BankReferenceNumber"].ToString();
                           

                            banks.Add(bank);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return banks;
        }
        public List<Bank> getIfscDetail(string ifsccode)
        {
            List<Bank> banks = new List<Bank>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();
                    DataSet resultSet = new DataSet();
                    param.Add(new SpParam("@ifsccode", ifsccode));
                    DBHelper.ExecProcAndFillDataSet("sp_bankviaIfsc", param, resultSet);
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable drBank = resultSet.Tables[0];
                        foreach (DataRow row in drBank.Rows)
                        {
                            Bank bank = new Bank();
                            bank.BankId = int.Parse(row["BankId"].ToString());
                            bank.BankName = row["bankname"].ToString();
                            bank.BranchId = int.Parse(row["branchid"].ToString());
                            bank.BranchName = row["branchname"].ToString();
                            bank.IfscCode = row["ifsccode"].ToString();

                            banks.Add(bank);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return banks;
        }

        public List<BankReport> getBankReport(string userid)
        {
            List<BankReport> reports = new List<BankReport>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();
                    DataSet resultSet = new DataSet();
                    param.Add(new SpParam("@userId", userid));
                    DBHelper.ExecProcAndFillDataSet("spGetBcCount", param, resultSet);
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable drBank = resultSet.Tables[0];
                        foreach (DataRow row in drBank.Rows)
                        {
                            BankReport bankReport = new BankReport();
                            bankReport.bcCount = row["bcCount"].ToString();
                            bankReport.bankCount = row["bankCount"].ToString();
                            bankReport.branchCount = row["branchCount"].ToString();
                            reports.Add(bankReport);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reports;
        }
        public List<FileStatus> getFileStatus(string userId)
        {
            List<FileStatus> banks = new List<FileStatus>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();
                    DataSet resultSet = new DataSet();
                    param.Add(new SpParam("@userId", userId));
                    DBHelper.ExecProcAndFillDataSet("getFileStatus", param, resultSet);
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable drBank = resultSet.Tables[0];
                        foreach (DataRow row in drBank.Rows)
                        {
                            FileStatus bank = new FileStatus();
                            bank.userId = int.Parse(row["userId"].ToString());
                            bank.fileStatus = row["fileStatus"].ToString();
                            bank.fileName = row["excelName"].ToString();
                          

                            banks.Add(bank);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return banks;
        }
        public List<FileStatus> getZipStatuss(string userId)
        {
            List<FileStatus> banks = new List<FileStatus>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();
                    DataSet resultSet = new DataSet();
                    param.Add(new SpParam("@userId", userId));
                    DBHelper.ExecProcAndFillDataSet("getZipsStatus", param, resultSet);
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable drBank = resultSet.Tables[0];
                        foreach (DataRow row in drBank.Rows)
                        {
                            FileStatus bank = new FileStatus();
                            bank.userId = int.Parse(row["userId"].ToString());
                            bank.fileStatus = row["fileStatus"].ToString();
                            bank.fileName = row["zipName"].ToString();
                          

                            banks.Add(bank);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return banks;
        }
        
        public List<FileStatus> getFileError(string userId)
        {
            List<FileStatus> banks = new List<FileStatus>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();
                    DataSet resultSet = new DataSet();
                    param.Add(new SpParam("@userId", userId));
                    DBHelper.ExecProcAndFillDataSet("getFileError", param, resultSet);
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable drBank = resultSet.Tables[0];
                        foreach (DataRow row in drBank.Rows)
                        {
                            FileStatus bank = new FileStatus();
                          
                            bank.Error = row["Error"].ToString();
                          

                            banks.Add(bank);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return banks;
        }

        

        
        public List<Products> GetProduct()
        {
            List<Products> products = new List<Products>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();            
                    DataSet resultSet = new DataSet();
                    DBHelper.ExecProcAndFillDataSet("sp_GetProduct", param, resultSet);                 
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable productsTable = resultSet.Tables[0];
                        foreach (DataRow row in productsTable.Rows)
                        {
                            Products product = new Products();
                            product.ProductId = int.Parse(row["ProductId"].ToString());
                            product.ProductName = row["ProductName"].ToString();
                       
                            products.Add(product);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return products;
        }
         public List<Circle> getBankState(string BankId)
        {
            List<Circle> products = new List<Circle>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();            
                    DataSet resultSet = new DataSet();                    
                         param.Add(new SpParam("@bankid", BankId));
                    DBHelper.ExecProcAndFillDataSet("SpGetCircel", param, resultSet);                 
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable productsTable = resultSet.Tables[0];
                        foreach (DataRow row in productsTable.Rows)
                        {
                            Circle product = new Circle();
                            product.CircleId = int.Parse(row["CircleId"].ToString());
                            product.CircleName = row["CircleName"].ToString();
                       
                            products.Add(product);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return products;
        }
         public List<Branch> getBankBranch(string BankId)
        {
            List<Branch> products = new List<Branch>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();            
                    DataSet resultSet = new DataSet();                    
                         param.Add(new SpParam("@bankid", BankId));
                         DBHelper.ExecProcAndFillDataSet("SpGetBrnachByBankId", param, resultSet);                 
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable productsTable = resultSet.Tables[0];
                        foreach (DataRow row in productsTable.Rows)
                        {
                            Branch product = new Branch();
                            product.BranchId = int.Parse(row["BranchId"].ToString());
                            product.BranchName = row["BranchName"].ToString();
                       
                            products.Add(product);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return products;
        }

         public List<Branch> getBankBranches(string distictid)
         {
             List<Branch> products = new List<Branch>();
             try
             {
                 using (conn = new SqlConnection(Constants.connString))
                 {
                     SpParamCollection param = new SpParamCollection();
                     DataSet resultSet = new DataSet();
                     param.Add(new SpParam("@distictid", distictid));
                     DBHelper.ExecProcAndFillDataSet("SpGetBrnachByDistrictId", param, resultSet);
                     if (resultSet.Tables.Count >= 1)
                     {
                         DataTable productsTable = resultSet.Tables[0];
                         foreach (DataRow row in productsTable.Rows)
                         {
                             Branch product = new Branch();
                             product.BranchId = int.Parse(row["BranchId"].ToString());
                             product.BranchName = row["BranchName"].ToString();

                             products.Add(product);
                         }
                     }
                 }
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             return products;
         }

         public List<Zone> GetCity(string StateId)
        {
            List<Zone> products = new List<Zone>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();            
                    DataSet resultSet = new DataSet();
                    param.Add(new SpParam("@StateId", StateId));
                    DBHelper.ExecProcAndFillDataSet("SpGetCity", param, resultSet);                 
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable productsTable = resultSet.Tables[0];
                        foreach (DataRow row in productsTable.Rows)
                        {
                            Zone product = new Zone();
                            product.ZoneId = int.Parse(row["ZoneId"].ToString());
                            product.ZoneName = row["ZoneName"].ToString();
                       
                            products.Add(product);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return products;
        }
         public List<Bank> getBanks()
        {
            List<Bank> banks = new List<Bank>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();
                    DataSet resultSet = new DataSet();

                    DBHelper.ExecProcAndFillDataSet("SpGetAllBanks", param, resultSet);
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable drBank = resultSet.Tables[0];
                        foreach (DataRow row in drBank.Rows)
                        {
                            Bank bank = new Bank();
                            bank.BankId = int.Parse(row["BankId"].ToString());
                            bank.BankName = row["BankName"].ToString();

                            banks.Add(bank);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return banks;
        }
        
         public List<Region> GetDistrict(string cityId)
        {
            List<Region> products = new List<Region>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();            
                    DataSet resultSet = new DataSet();
                    param.Add(new SpParam("@cityId", cityId));
                    DBHelper.ExecProcAndFillDataSet("SpGetDistrict", param, resultSet);                 
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable productsTable = resultSet.Tables[0];
                        foreach (DataRow row in productsTable.Rows)
                        {
                            Region product = new Region();
                            product.RegionId = int.Parse(row["RegionId"].ToString());
                            product.RegionName = row["RegionName"].ToString();
                       
                            products.Add(product);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return products;
        }
        
        public List<Bank> getBanks(string userid)
        {
            List<Bank> banks = new List<Bank>();
            try
            {
                using (conn = new SqlConnection(Constants.connString))
                {
                    SpParamCollection param = new SpParamCollection();            
                    DataSet resultSet = new DataSet();
                    param.Add(new SpParam("@userId", userid));
                    DBHelper.ExecProcAndFillDataSet("SpGetBanks", param, resultSet);                 
                    if (resultSet.Tables.Count >= 1)
                    {
                        DataTable drBank = resultSet.Tables[0];
                        foreach (DataRow row in drBank.Rows)
                        {
                            Bank bank = new Bank();
                            bank.BankId =  int.Parse(row["BankId"].ToString());
                            bank.BankName =  row["BankName"].ToString();

                            banks.Add(bank);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return banks;
        }

        
        public List<BCProducts> GetProducts(int? bcId, int? productId) {
            List<BCProducts> products = null;
            try {
                using (conn=new SqlConnection(Constants.connString)) {
                    SpParamCollection param = new SpParamCollection();
                    param.Add(new SpParam("@bcId", bcId));
                    param.Add(new SpParam("@productId", productId));
                    DataSet resultSet = new DataSet();

                  
                   DBHelper.ExecProcAndFillDataSet("sp_GetProducts", param, resultSet);
                    if (resultSet.Tables.Count >= 1) {
                        DataTable productsTable = resultSet.Tables[0];
                        foreach(DataRow row in productsTable.Rows) {
                            BCProducts product = new BCProducts();
                            product.ProductId = int.Parse(row["ProductId"].ToString());
                            product.ProductName = row["ProductName"].ToString();
                            product.BCId = int.Parse(row["BCId"].ToString());
                            products.Add(product);
                        }
                    }
                }
            }
            catch (Exception ex) {
                throw ex;
            }
            return products;
        }

        public int InsertUpdateBanks(Bank bank, string mode, int? userId) {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try {
                if (bank != null) {

                    int bankId = 0;

                    if (mode == Constants.UPDATEMODE)
                        bankId = bank.BankId;

                    spParams.Add(new SpParam("@BankId", bankId));
                    spParams.Add(new SpParam("@BankName", bank.BankName));
                    spParams.Add(new SpParam("@Address", bank.Address));
                    spParams.Add(new SpParam("@ContactNumber", bank.ContactNumber));
                    spParams.Add(new SpParam("@Email", bank.Email));
                    spParams.Add(new SpParam("@userId", userId));

                    if (bank.BankCircles != null) {
                        DataTable dtCircles = new DataTable();
                        dtCircles.Columns.Add("CircleName", typeof(string));
                        dtCircles.Columns.Add("BankId", typeof(int));

                        DataTable dtZones = new DataTable();
                        dtZones.Columns.Add("ZoneName", typeof(string));
                        dtZones.Columns.Add("CircleId", typeof(int));
                        dtZones.Columns.Add("CircleName", typeof(string));


                        DataTable dtRegions = new DataTable();
                        dtRegions.Columns.Add("RegionName", typeof(string));
                        dtRegions.Columns.Add("ZoneId", typeof(int));
                        dtRegions.Columns.Add("ZoneName", typeof(string));


                        DataTable dtBranches = new DataTable();
                        dtBranches.Columns.Add("BranchName", typeof(string));
                        dtBranches.Columns.Add("BranchCode", typeof(string));
                        dtBranches.Columns.Add("IFSCCode", typeof(string));
                        dtBranches.Columns.Add("MICRCode", typeof(string));
                        dtBranches.Columns.Add("Address", typeof(string));
                        dtBranches.Columns.Add("ContactNumber", typeof(string));
                        dtBranches.Columns.Add("Email", typeof(string));
                        dtBranches.Columns.Add("BankId", typeof(int));
                        dtBranches.Columns.Add("Region", typeof(string));
                        dtBranches.Columns.Add("RegionId", typeof(int));
                        dtBranches.Columns.Add("StateId", typeof(int));
                        dtBranches.Columns.Add("DistrictId", typeof(int));
                        dtBranches.Columns.Add("TalukaId", typeof(int));
                        dtBranches.Columns.Add("CityId", typeof(int));
                        dtBranches.Columns.Add("VillageId", typeof(int));
                        dtBranches.Columns.Add("PinCode", typeof(string));


                        foreach (Circle c in bank.BankCircles) {
                            DataRow row = dtCircles.NewRow();
                            row["CircleName"] = c.CircleName;
                            row["BankId"] = bankId;
                            dtCircles.Rows.Add(row);

                            if (c.CircleZones != null) {

                                foreach (Zone z in c.CircleZones) {
                                    DataRow drZones = dtZones.NewRow();
                                    drZones["ZoneName"] = z.ZoneName;
                                    drZones["CircleId"] = c.CircleId;
                                    drZones["CircleName"] = c.CircleName;
                                    dtZones.Rows.Add(drZones);

                                    if (z.ZoneRegions != null) {
                                        foreach (Region r in z.ZoneRegions) {
                                            DataRow drRegions = dtRegions.NewRow();
                                            drRegions["RegionName"] = r.RegionName;
                                            drRegions["ZoneId"] = z.ZoneId;
                                            drRegions["ZoneName"] = z.ZoneName;
                                            dtRegions.Rows.Add(drRegions);

                                            if (r.RegionBranches != null) {
                                                foreach (Branch b in r.RegionBranches) {
                                                    DataRow drBranches = dtBranches.NewRow();
                                                    drBranches["BranchName"] = b.BranchName;
                                                    drBranches["BranchCode"] = b.BranchCode;                                                    
                                                    drBranches["IFSCCode"] = b.IFSCCode;
                                                    drBranches["MICRCode"] = b.MICRCode;
                                                    drBranches["Address"] = b.Address;
                                                    drBranches["ContactNumber"] = b.ContactNumber;
                                                    drBranches["Email"] = b.Email;
                                                    drBranches["BankId"] = bank.BankId;
                                                    drBranches["Region"] = r.RegionName;
                                                    drBranches["RegionId"] = r.RegionId;
                                                    drBranches["StateId"] = b.StateId;
                                                    drBranches["DistrictId"] = b.DistrictId;
                                                    drBranches["TalukaId"] = b.TalukaId;
                                                    drBranches["CityId"] = b.CityId;
                                                    drBranches["VillageId"] = b.VillageId;
                                                    drBranches["PinCode"] = b.PinCode;
                                                    dtBranches.Rows.Add(drBranches);
                                                }

                                            }


                                        }

                                    }

                                }


                            }

                        }

                        spParams.Add(new SpParam("@Circles", dtCircles));
                        spParams.Add(new SpParam("@Zones", dtZones));
                        spParams.Add(new SpParam("@Regions", dtRegions));
                     //   spParams.Add(new SpParam("@Branches", dtBranches));

                        object newID = DBHelper.ExecProcScalar("sp_AddEditBank", spParams);
                        retVal = int.Parse(newID.ToString());

                        if (retVal>0)
                        {
                            //SpParamCollection sp_Paramss = new SpParamCollection();
                            //sp_Paramss.Add(new SpParam("@bankId", retVal));

                            //object newIDs = DBHelper.ExecProcScalar("spDeleteBrnach", sp_Paramss);
                        }
                      
                        foreach (DataRow row in dtBranches.Rows)
                        {
                            SpParamCollection sp_Params = new SpParamCollection();
                            sp_Params.Add(new SpParam("@BranchName", row["BranchName"].ToString()));
                            sp_Params.Add(new SpParam("@BranchCode", row["BranchCode"].ToString()));
                            sp_Params.Add(new SpParam("@IFSCCode", row["IFSCCode"].ToString()));
                            sp_Params.Add(new SpParam("@MICRCode", row["MICRCode"].ToString()));
                            sp_Params.Add(new SpParam("@Address", row["Address"].ToString()));
                            sp_Params.Add(new SpParam("@ContactNumber", row["ContactNumber"].ToString()));
                            sp_Params.Add(new SpParam("@Email", row["Email"].ToString()));
                            sp_Params.Add(new SpParam("@RegionName", row["Region"].ToString()));
                            sp_Params.Add(new SpParam("@StateId", row["StateId"].ToString()));
                            sp_Params.Add(new SpParam("@DistrictId", row["DistrictId"].ToString()));
                            sp_Params.Add(new SpParam("@TalukaId", row["TalukaId"].ToString()));                            
                            sp_Params.Add(new SpParam("@CityId", row["CityId"].ToString()));
                            sp_Params.Add(new SpParam("@VillageId", row["VillageId"].ToString()));
                            sp_Params.Add(new SpParam("@PinCode", row["PinCode"].ToString()));
                            sp_Params.Add(new SpParam("@CurrentBankId", retVal));
                            sp_Params.Add(new SpParam("@userId", userId));
                            
                            object newIDs = DBHelper.ExecProcScalar("spCreateBranch", sp_Params);
                        }


                    }
                }
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in InsertUpdateBank: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally {
                conn.Close();
            }
            return retVal;
        }
        public int InsertUpdateState(State state, string mode, int? userId)
        {
            int retVal = -1;
            try
            {
                conn = new SqlConnection(Constants.connString);
                conn.Open();
                if (state != null)
                {
                    int? stateId = int.MinValue;
                    if (mode == Constants.UPDATEMODE)
                    {
                        stateId = state.StateId;
                    }
                    SpParamCollection param = new SpParamCollection();
                    param.Add(new SpParam("@StateId", stateId));
                    param.Add(new SpParam("@StateName", state.StateName));
                    param.Add(new SpParam("@userId", userId));
                    if (state.Districts != null)
                    {
                        //Define your tables
                        DataTable dtDistricts = new DataTable();
                        dtDistricts.Columns.Add("DistrictCode", typeof(string));
                        dtDistricts.Columns.Add("DistrictName", typeof(string));
                        dtDistricts.Columns.Add("StateId", typeof(int));


                        DataTable dtSubDistrict = new DataTable();
                        dtSubDistrict.Columns.Add("SubDistrictCode", typeof(string));
                        dtSubDistrict.Columns.Add("SubDistrictName", typeof(string));
                        dtSubDistrict.Columns.Add("DistrictName", typeof(string));

                        //DataTable dtTalukas = new DataTable();
                        //dtTalukas.Columns.Add("TalukaName", typeof(string));
                        //dtTalukas.Columns.Add("DistrictName", typeof(string));

                        DataTable dtVillages = new DataTable();
                        dtVillages.Columns.Add("VillageCode", typeof(string));
                        dtVillages.Columns.Add("VillageName", typeof(string));
                        dtVillages.Columns.Add("subdistrictName", typeof(string));

                        //... And populate them                       
                        foreach (District d in state.Districts)
                        {
                            //DataRow districtsRow = dtDistricts.NewRow();                            
                            //districtsRow["DistrictName"] = d.DistrictName;
                            //districtsRow["DistrictCode"] = d.DistrictCode;
                            //districtsRow["StateId"] = state.StateId;
                            //dtDistricts.Rows.Add(districtsRow);

                            //if (d.Talukas != null) {
                            //    foreach (Taluka t in d.Talukas) {
                            //        DataRow talukasRow = dtTalukas.NewRow();
                            //        talukasRow["TalukaName"] = t.TalukaName;
                            //        talukasRow["DistrictName"] = t.DistrictName;
                            //        dtTalukas.Rows.Add(talukasRow);
                            //    }
                            // }
                            if (d.DistrictList != null)
                            {
                                foreach (DistrictList c in d.DistrictList)
                                {
                                    DataRow districtsRow = dtDistricts.NewRow();
                                    districtsRow["DistrictCode"] = c.DistrictCode;
                                    districtsRow["DistrictName"] = c.DistrictName;
                                    districtsRow["StateId"] = state.StateId;
                                    dtDistricts.Rows.Add(districtsRow);
                                }
                            }
                            if (d.SubDistrict != null)
                            {
                                foreach (SubDistrict c in d.SubDistrict)
                                {
                                    DataRow subDistrictRow = dtSubDistrict.NewRow();
                                    subDistrictRow["SubDistrictCode"] = c.SubDistrictCode;
                                    subDistrictRow["SubDistrictName"] = c.SubDistrictName;
                                    subDistrictRow["DistrictName"] = c.SubDistrictName;
                                    dtSubDistrict.Rows.Add(subDistrictRow);
                                }
                            }
                            if (d.Villages != null)
                            {
                                foreach (Village v in d.Villages)
                                {
                                    DataRow villagesRow = dtVillages.NewRow();
                                    villagesRow["VillageName"] = v.VillageName;
                                    villagesRow["VillageCode"] = v.VillageCode;
                                    villagesRow["subdistrictName"] = v.SubDistrictName;
                                    dtVillages.Rows.Add(villagesRow);
                                }
                            }
                        }

                        param.Add(new SpParam("@Districts", dtDistricts));
                        param.Add(new SpParam("@subdistict", dtSubDistrict));

                        param.Add(new SpParam("@Villages", dtVillages));
                        Object newId = DBHelper.ExecProcScalar("sp_AddEditStates", param);
                        retVal = int.Parse(newId.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateState: " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }

        //public int InsertUpdateState(State state, string mode, int? userId) {
        //    int retVal = -1;
        //    try {
        //        conn = new SqlConnection(Constants.connString);
        //        conn.Open();
        //        if (state != null) {
        //            int? stateId = int.MinValue;
        //            if (mode == Constants.UPDATEMODE) {
        //                stateId = state.StateId;
        //            }
        //            SpParamCollection param = new SpParamCollection();
        //            param.Add(new SpParam("@StateId",stateId));
        //            param.Add(new SpParam("@StateName",state.StateName));
        //            param.Add(new SpParam("@userId", userId));
        //            if (state.Districts != null) {
        //                //Define your tables
        //                DataTable dtDistricts = new DataTable();
        //                dtDistricts.Columns.Add("DistrictName", typeof(string));
        //                dtDistricts.Columns.Add("StateId", typeof(int));

        //                DataTable dtCities = new DataTable();
        //                dtCities.Columns.Add("CityName", typeof(string));
        //                dtCities.Columns.Add("DistrictName", typeof(string));

        //                DataTable dtTalukas = new DataTable();
        //                dtTalukas.Columns.Add("TalukaName", typeof(string));
        //                dtTalukas.Columns.Add("DistrictName", typeof(string));

        //                DataTable dtVillages = new DataTable();
        //                dtVillages.Columns.Add("VillageName", typeof(string));
        //                dtVillages.Columns.Add("TalukaName", typeof(string));
        //                dtVillages.Columns.Add("DistrictName", typeof(string));

        //                //... And populate them                       
        //                foreach (District d in state.Districts) {
        //                    DataRow districtsRow = dtDistricts.NewRow();                            
        //                    districtsRow["DistrictName"] = d.DistrictName;
        //                    districtsRow["StateId"] = state.StateId;
        //                    dtDistricts.Rows.Add(districtsRow);
        //                    if (d.Talukas != null) {
        //                        foreach (Taluka t in d.Talukas) {
        //                            DataRow talukasRow = dtTalukas.NewRow();
        //                            talukasRow["TalukaName"] = t.TalukaName;
        //                            talukasRow["DistrictName"] = t.DistrictName;
        //                            dtTalukas.Rows.Add(talukasRow);
        //                        }
        //                    }
        //                    if (d.Cities != null) {
        //                        foreach (City c in d.Cities) {
        //                            DataRow citiesRow = dtCities.NewRow();
        //                            citiesRow["CityName"] = c.CityName;
        //                            citiesRow["DistrictName"] = c.DistrictName;
        //                            dtCities.Rows.Add(citiesRow);
        //                        }
        //                    }
        //                    if (d.Villages != null) {
        //                        foreach (Village v in d.Villages) {
        //                            DataRow villagesRow = dtVillages.NewRow();
        //                            villagesRow["VillageName"] = v.VillageName;
        //                      //      villagesRow["TalukaName"] = v.TalukaName;
        //                        //    villagesRow["DistrictName"] = v.DistrictName;
        //                            dtVillages.Rows.Add(villagesRow);
        //                        }
        //                    }
        //                }

        //                param.Add(new SpParam("@Districts", dtDistricts));
        //                param.Add(new SpParam("@Cities", dtCities));
        //                param.Add(new SpParam("@Talukas", dtTalukas));
        //                param.Add(new SpParam("@Villages", dtVillages));
        //                Object newId = DBHelper.ExecProcScalar("sp_AddEditStates", param);
        //                retVal = int.Parse(newId.ToString());
        //            }
        //        }
        //    }
        //    catch (Exception ex) {
        //        BCTrackingLogger.Error("Error in InsertUpdateState: " + ex.Message + "<br/>" + ex.StackTrace);
        //        throw ex;
        //    }
        //    finally {
        //        conn.Close();
        //    }
        //    return retVal;
        //}

        public void DeleteBanks(int BankId) {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@BankId", BankId));
                DBHelper.ExecProcNonQuery("sp_DeleteBank", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in DeleteBank: " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }


        public int BlackListBankCorrespond(BankCorrespondence bc, int userId, string mode) {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try {
                bool blackList = true;
                if (mode == Constants.WHITELISTMODE)
                    blackList = false;

                spParams.Add(new SpParam("@BcId", bc.BankCorrespondId));
                spParams.Add(new SpParam("@BlackListDate", bc.BlacklistDate));
                spParams.Add(new SpParam("@Reason", bc.BlackListReason));
                spParams.Add(new SpParam("@ApprovalNumber", bc.BlackListApprovalNumber));
                spParams.Add(new SpParam("@BlackList", blackList));
                spParams.Add(new SpParam("@userId", userId));


                object newID = DBHelper.ExecProcScalar("sp_BlackListBC", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in BlackListBankCorrespond: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally {
                conn.Close();
            }
            return retVal;
        }

        public int InsertUpdateBrnach(Branch bc, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try {


                spParams.Add(new SpParam("@CurrentBankId", bc.BankId));
                spParams.Add(new SpParam("@BranchName", bc.BranchName));
                spParams.Add(new SpParam("@IFSCCode", bc.IFSCCode));
                spParams.Add(new SpParam("@Address", bc.Address));
                spParams.Add(new SpParam("@ContactNumber", bc.ContactNumber));
                spParams.Add(new SpParam("@StateId", bc.StateId));
                spParams.Add(new SpParam("@CityId", bc.CityId));
                spParams.Add(new SpParam("@DistrictId", bc.DistrictId));
                spParams.Add(new SpParam("@userId", userId));


                object newID = DBHelper.ExecProcScalar("Spcreatebranch", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in BlackListBankCorrespond: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally {
                conn.Close();
            }
            return retVal;
        }


        public int SaveZipPath(string FolderName, string fileName, string filePath, string BCCode, string fileType, string UserId)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {

                spParams.Add(new SpParam("@foldername", FolderName));
                spParams.Add(new SpParam("@filesname", fileName));
                spParams.Add(new SpParam("@filePath", filePath));
                spParams.Add(new SpParam("@aadharnumber", BCCode));
                spParams.Add(new SpParam("@fileType", fileType));
                spParams.Add(new SpParam("@UserId", UserId));
                
                object newID = DBHelper.ExecProcScalar("sp_zipUpload", spParams);



            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("got some error here: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        //public int InsertUpdateBankCorresponds(BankCorrespondence bc, int userId, string mode)
        //{
        //    int retVal = -1;
        //    conn = new SqlConnection(Constants.connString);
        //    conn.Open();
        //    SpParamCollection spParams = new SpParamCollection();
        //    SpParamCollection spParam = new SpParamCollection();

        //    try
        //    {
        //        if (mode == Constants.UPDATEMODE)
        //        {
        //            spParams.Add(new SpParam("@BankCorrespondId", bc.BankCorrespondId));
        //        }
        //        else
        //        {
        //            spParams.Add(new SpParam("@BankCorrespondId", 0));
        //        }

        //        spParams.Add(new SpParam("@userId", userId));
        //        spParams.Add(new SpParam("@Name", bc.Name));
        //        spParams.Add(new SpParam("@ImagePath", bc.ImagePath));
        //        spParams.Add(new SpParam("@Gender", bc.Gender));
        //        spParams.Add(new SpParam("@DOB", bc.DOB.Value));
        //        spParams.Add(new SpParam("@FatherName", bc.FatherName));
        //        spParams.Add(new SpParam("@SpouseName", bc.SpouseName));
        //        spParams.Add(new SpParam("@Category", bc.Category));
        //        spParams.Add(new SpParam("@PhysicallyHandicap", bc.Handicap == "No" ? false : true));
        //        spParams.Add(new SpParam("@PhoneNumber1", bc.PhoneNumber1));
        //        spParams.Add(new SpParam("@PhoneNumber2", bc.PhoneNumber2));
        //        spParams.Add(new SpParam("@PhoneNumber3", bc.PhoneNumber3));
        //        spParams.Add(new SpParam("@Email", bc.Email));
        //        spParams.Add(new SpParam("@AadharCard", bc.AadharCard));
        //        spParams.Add(new SpParam("@PanCard", bc.PanCard));
        //        spParams.Add(new SpParam("@VotersIdCard", bc.VoterCard));
        //        spParams.Add(new SpParam("@DriverLicense", bc.DriverLicense));
        //        spParams.Add(new SpParam("@NREGACard", bc.NregaCard));
        //        spParams.Add(new SpParam("@RationCard", bc.RationCard));
        //        spParams.Add(new SpParam("@AddressState", bc.State));
        //        spParams.Add(new SpParam("@AddressCity", bc.City));
        //        spParams.Add(new SpParam("@AddressDistrict", bc.District));
        //        spParams.Add(new SpParam("@AddressSubDistrict", bc.Subdistrict));
        //        spParams.Add(new SpParam("@Village", bc.Village));
        //        spParams.Add(new SpParam("@productId", bc.productId));

        //        spParams.Add(new SpParam("@Longitude", bc.Longitude));
        //        spParams.Add(new SpParam("@Latitude", bc.Latitude));

        //        spParams.Add(new SpParam("@AddressArea", bc.Area));
        //        spParams.Add(new SpParam("@PinCode", bc.PinCode));
        //        spParams.Add(new SpParam("@AlternateOccupationType", bc.AlternateOccupationType));
        //        spParams.Add(new SpParam("@AlternateOccupationDetail", bc.AlternateOccupationDetail));
        //        spParams.Add(new SpParam("@UniqueIdentificationNumber", bc.UniqueIdentificationNumber));
        //        spParams.Add(new SpParam("@BankReferenceNumber", bc.BankReferenceNumber));
        //        spParams.Add(new SpParam("@EducationalQualification", bc.Qualification));
        //        spParams.Add(new SpParam("@OtherQualification", bc.OtherQualification));
        //        spParams.Add(new SpParam("@CorporateId", bc.CorporateId));
        //        spParams.Add(new SpParam("@IsAllocated", bc.isAllocated));

        //        if (bc.isAllocated)
        //        {
        //            spParams.Add(new SpParam("@AppointmentDate", bc.AppointmentDate));
        //            spParams.Add(new SpParam("@AllocationIFSCCode", bc.AllocationIFSCCode));
        //            spParams.Add(new SpParam("@AllocationBankId", bc.AllocationBankId));
        //            spParams.Add(new SpParam("@AllocationBranchId", bc.AllocationBranchId));

        //            spParams.Add(new SpParam("@BCType", bc.BCType));
        //            spParams.Add(new SpParam("@WorkingDays", bc.WorkingDays));
        //            spParams.Add(new SpParam("@WorkingHours", bc.WorkingHours));

        //            spParams.Add(new SpParam("@PLPostalAddress", bc.PLPostalAddress));
        //            spParams.Add(new SpParam("@PLVillageCode", bc.PLVillageCode));
        //            spParams.Add(new SpParam("@PLVillageDetail", bc.PLVillageDetail));
        //            spParams.Add(new SpParam("@PLStateId", bc.PLStateId));
        //            spParams.Add(new SpParam("@PLDistrictId", bc.PLDistrictId));
        //            spParams.Add(new SpParam("@PLTaluk", bc.PLTaluk));
        //            spParams.Add(new SpParam("@PLPinCode", bc.PLPinCode));

        //            spParams.Add(new SpParam("@MinimumCashHandlingLimit", bc.MinimumCashHandlingLimit));
        //            spParams.Add(new SpParam("@MonthlyFixedRenumeration", bc.MonthlyFixedRenumeration));
        //            spParams.Add(new SpParam("@MonthlyVariableRenumeration", bc.MonthlyVariableRenumeration));


        //        }

        //        DataTable dtCertifications = new DataTable();
        //        //  dtCertifications.Columns.Add(new DataColumn("CertificationId", typeof(int)));
        //        dtCertifications.Columns.Add(new DataColumn("PassingDate", typeof(DateTime)));
        //        dtCertifications.Columns.Add(new DataColumn("Institute", typeof(string)));
        //        dtCertifications.Columns.Add(new DataColumn("Course", typeof(string)));
        //        dtCertifications.Columns.Add(new DataColumn("Grade", typeof(string)));
        //        if (bc.Certifications != null)
        //        {
        //            foreach (BCCertifications cer in bc.Certifications)
        //            {
        //                DataRow drCertifications = dtCertifications.NewRow();
        //                //drCertifications["CertificationId"] = cer.CertificationId;
        //                drCertifications["PassingDate"] = cer.DateOfPassing;
        //                drCertifications["Institute"] = cer.InstituteName;
        //                drCertifications["Course"] = cer.CourseName;
        //                drCertifications["Grade"] = cer.Grade;
        //                dtCertifications.Rows.Add(drCertifications);

        //                //spParam.Add(new SpParam("@institueName", cer.InstituteName));
        //                //spParam.Add(new SpParam("@courseName", cer.CourseName));
        //                //object newIDS = DBHelper.ExecProcScalar("spCreateInstitueCourse", spParam);
        //                //retVal = int.Parse(newIDS.ToString());
        //            }
        //        }
        //        spParams.Add(new SpParam("@AllCertifications", dtCertifications));


        //        DataTable dtPreviousExp = new DataTable();
        //        // dtPreviousExp.Columns.Add(new DataColumn("ExperienceId", typeof(int)));
        //        dtPreviousExp.Columns.Add(new DataColumn("BankId", typeof(int)));
        //        dtPreviousExp.Columns.Add(new DataColumn("BranchId", typeof(int)));
        //        dtPreviousExp.Columns.Add(new DataColumn("FromDate", typeof(DateTime)));
        //        dtPreviousExp.Columns.Add(new DataColumn("ToDate", typeof(DateTime)));
        //        dtPreviousExp.Columns.Add(new DataColumn("Reason", typeof(string)));
        //        if (bc.PreviousExperience != null)
        //        {
        //            foreach (PreviousExperiene exp in bc.PreviousExperience)
        //            {
        //                DataRow drPreviousExp = dtPreviousExp.NewRow();
        //                //  drPreviousExp["ExperienceId"] = exp.ExperienceId;
        //                drPreviousExp["BankId"] = exp.BankId;
        //                drPreviousExp["BranchId"] = exp.Branchid;
        //                drPreviousExp["FromDate"] = exp.FromDate;
        //                drPreviousExp["ToDate"] = exp.ToDate;
        //                drPreviousExp["Reason"] = exp.Reason;
        //                dtPreviousExp.Rows.Add(drPreviousExp);
        //            }
        //        }
        //        spParams.Add(new SpParam("@AllPreviousExperience", dtPreviousExp));


        //        DataTable dtOperationalAreas = new DataTable();
        //        //  dtOperationalAreas.Columns.Add(new DataColumn("AreaId", typeof(int)));
        //        dtOperationalAreas.Columns.Add(new DataColumn("VillageCode", typeof(string)));
        //        dtOperationalAreas.Columns.Add(new DataColumn("VillageDetails", typeof(string)));
        //        if (bc.OperationalAreas != null)
        //        {
        //            foreach (OperationalAreas oper in bc.OperationalAreas)
        //            {
        //                DataRow drOperationalAreas = dtOperationalAreas.NewRow();
        //                //        drOperationalAreas["AreaId"] = oper.AreaId;
        //                drOperationalAreas["VillageCode"] = oper.VillageCode;
        //                drOperationalAreas["VillageDetails"] = oper.VillageDetail;

        //                dtOperationalAreas.Rows.Add(drOperationalAreas);
        //            }
        //        }
        //        spParams.Add(new SpParam("@AllOperationalAreas", dtOperationalAreas));


        //        DataTable dtDevices = new DataTable();
        //        // dtDevices.Columns.Add(new DataColumn("DeviceId", typeof(int)));
        //        dtDevices.Columns.Add(new DataColumn("Device", typeof(string)));
        //        dtDevices.Columns.Add(new DataColumn("GivenOn", typeof(DateTime)));
        //        dtDevices.Columns.Add(new DataColumn("DeviceCode", typeof(string)));
                
        //        if (bc.Devices != null && bc.isAllocated)
        //        {
        //            foreach (BCDevices dev in bc.Devices)
        //            {
        //                DataRow drDevices = dtDevices.NewRow();
        //                //       drDevices["DeviceId"] = dev.DeviceId;
        //                drDevices["Device"] = dev.Device;
        //                drDevices["GivenOn"] = dev.GivenOn;
        //                drDevices["DeviceCode"] = dev.DeviceCode;                        
        //                dtDevices.Rows.Add(drDevices);
        //            }
        //        }
        //       spParams.Add(new SpParam("@AllDevice", dtDevices));



        //        DataTable dtConnectivityDetails = new DataTable();
        //        //   dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityId", typeof(int)));
        //        dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityMode", typeof(string)));
        //        dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityProvider", typeof(string)));
        //        dtConnectivityDetails.Columns.Add(new DataColumn("ContactNumber", typeof(string)));
        //        if (bc.ConnectivityDetails != null && bc.isAllocated)
        //        {
        //            foreach (ConnectivityDetails con in bc.ConnectivityDetails)
        //            {
        //                DataRow drConnectivityDetails = dtConnectivityDetails.NewRow();
        //                //      drConnectivityDetails["ConnectivityId"] = con.ConnectivityId;
        //                drConnectivityDetails["ConnectivityMode"] = con.ConnectivityMode;
        //                drConnectivityDetails["ConnectivityProvider"] = con.ConnectivityProvider;
        //                drConnectivityDetails["ContactNumber"] = con.ContactNumber;

        //                dtConnectivityDetails.Rows.Add(drConnectivityDetails);
        //            }
        //        }
        //        spParams.Add(new SpParam("@AllConnectivityDetails", dtConnectivityDetails));

        //        DataTable dtSSADetails = new DataTable();
        //        dtSSADetails.Columns.Add(new DataColumn("Ssa", typeof(string)));
        //        dtSSADetails.Columns.Add(new DataColumn("State", typeof(string)));
        //        dtSSADetails.Columns.Add(new DataColumn("District", typeof(string)));
        //        dtSSADetails.Columns.Add(new DataColumn("SubDistrict", typeof(string)));
        //        dtSSADetails.Columns.Add(new DataColumn("Village", typeof(string)));
        //        if (bc.SsaDetails != null)
        //        {
        //            foreach (SsaDetails con in bc.SsaDetails)
        //            {
        //                DataRow drSsaDetails = dtSSADetails.NewRow();
        //                drSsaDetails["Ssa"] = con.Ssa;
        //                drSsaDetails["State"] = con.State;
        //                drSsaDetails["District"] = con.District;
        //                drSsaDetails["SubDistrict"] = con.SubDistrict;
        //                drSsaDetails["Village"] = con.Village;

        //                dtSSADetails.Rows.Add(drSsaDetails);
        //            }
        //        }
        //        spParams.Add(new SpParam("@AllSsaDetails", dtSSADetails));

        //        DataTable dtProducts = new DataTable();
        //        //     dtProducts.Columns.Add(new DataColumn("ProductId", typeof(int)));
        //        dtProducts.Columns.Add(new DataColumn("ProductName", typeof(string)));
        //        if (bc.Products != null && bc.isAllocated)
        //        {
        //            foreach (BCProducts p in bc.Products)
        //            {
        //                DataRow drProduct = dtProducts.NewRow();
        //                //           drProduct["ProductId"] = p.ProductId;
        //                drProduct["ProductName"] = p.ProductName;

        //                dtProducts.Rows.Add(drProduct);
        //            }
        //        }
        //        spParams.Add(new SpParam("@Products", dtProducts));



        //        object newID = DBHelper.ExecProcScalar("sp_AddEditBankCorresponds", spParams);
        //        retVal = int.Parse(newID.ToString());


        //    }
        //    catch (Exception ex)
        //    {
        //        BCTrackingLogger.Error("Error in InsertUpdateBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);

        //        throw ex;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //    return retVal;
        //}


        public int InsertUpdateBankCorresponds(BankCorrespondence bc, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {
                if (mode == Constants.UPDATEMODE)
                {
                    spParams.Add(new SpParam("@BankCorrespondId", bc.BankCorrespondId));
                }
                else
                {
                    spParams.Add(new SpParam("@BankCorrespondId", 0));
                }

                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@Name", bc.Name));
                spParams.Add(new SpParam("@ImagePath", bc.ImagePath));
                spParams.Add(new SpParam("@Gender", bc.Gender));
                spParams.Add(new SpParam("@DOB", bc.DOB.Value));
                spParams.Add(new SpParam("@FatherName", bc.FatherName));
                spParams.Add(new SpParam("@SpouseName", bc.SpouseName));
                spParams.Add(new SpParam("@Category", bc.Category));
                spParams.Add(new SpParam("@PhysicallyHandicap", bc.Handicap == "No" ? false : true));
                spParams.Add(new SpParam("@PhoneNumber1", bc.PhoneNumber1));
                spParams.Add(new SpParam("@PhoneNumber2", bc.PhoneNumber2));
                spParams.Add(new SpParam("@PhoneNumber3", bc.PhoneNumber3));
                spParams.Add(new SpParam("@ContactPerson", bc.ContactPerson));
                spParams.Add(new SpParam("@ContactDesignation", bc.ContactDesignation));
                spParams.Add(new SpParam("@NoofComplaint", bc.NoofComplaint));
                spParams.Add(new SpParam("@Email", bc.Email));
                spParams.Add(new SpParam("@AadharCard", bc.AadharCard));
                spParams.Add(new SpParam("@PanCard", bc.PanCard));
                spParams.Add(new SpParam("@VotersIdCard", bc.VoterCard));
                spParams.Add(new SpParam("@DriverLicense", bc.DriverLicense));
                spParams.Add(new SpParam("@NREGACard", bc.NregaCard));
                spParams.Add(new SpParam("@RationCard", bc.RationCard));
                spParams.Add(new SpParam("@AddressState", bc.State));
                spParams.Add(new SpParam("@AddressCity", bc.City));
                spParams.Add(new SpParam("@AddressDistrict", bc.District));
                spParams.Add(new SpParam("@AddressSubDistrict", bc.Subdistrict));
                spParams.Add(new SpParam("@Village", bc.Village));
                spParams.Add(new SpParam("@productId", bc.productId));

                spParams.Add(new SpParam("@Longitude", bc.Longitude));
                spParams.Add(new SpParam("@Latitude", bc.Latitude));

                spParams.Add(new SpParam("@AddressArea", bc.Area));
                spParams.Add(new SpParam("@PinCode", bc.PinCode));
                spParams.Add(new SpParam("@AlternateOccupationType", bc.AlternateOccupationType));
                spParams.Add(new SpParam("@AlternateOccupationDetail", bc.AlternateOccupationDetail));
                spParams.Add(new SpParam("@UniqueIdentificationNumber", bc.UniqueIdentificationNumber));
                spParams.Add(new SpParam("@BankReferenceNumber", bc.BankReferenceNumber));
                spParams.Add(new SpParam("@EducationalQualification", bc.Qualification));
                spParams.Add(new SpParam("@OtherQualification", bc.OtherQualification));
                spParams.Add(new SpParam("@CorporateId", bc.CorporateId));
                spParams.Add(new SpParam("@IsAllocated", bc.isAllocated));

                if (bc.isAllocated)
                {
                    spParams.Add(new SpParam("@AppointmentDate", bc.AppointmentDate));
                    spParams.Add(new SpParam("@AllocationIFSCCode", bc.AllocationIFSCCode));
                    spParams.Add(new SpParam("@AllocationBankId", bc.AllocationBankId));
                    spParams.Add(new SpParam("@AllocationBranchId", bc.AllocationBranchId));

                    spParams.Add(new SpParam("@BCType", bc.BCType));
                    spParams.Add(new SpParam("@WorkingDays", bc.WorkingDays));
                    spParams.Add(new SpParam("@WorkingHours", bc.WorkingHours));

                    spParams.Add(new SpParam("@PLPostalAddress", bc.PLPostalAddress));
                    spParams.Add(new SpParam("@PLVillageCode", bc.PLVillageCode));
                    spParams.Add(new SpParam("@PLVillageDetail", bc.PLVillageDetail));
                    spParams.Add(new SpParam("@PLStateId", bc.PLStateId));
                    spParams.Add(new SpParam("@PLDistrictId", bc.PLDistrictId));
                    spParams.Add(new SpParam("@PLTaluk", bc.PLTaluk));
                    spParams.Add(new SpParam("@PLPinCode", bc.PLPinCode));

                    spParams.Add(new SpParam("@MinimumCashHandlingLimit", bc.MinimumCashHandlingLimit));
                    spParams.Add(new SpParam("@MonthlyFixedRenumeration", bc.MonthlyFixedRenumeration));
                    spParams.Add(new SpParam("@MonthlyVariableRenumeration", bc.MonthlyVariableRenumeration));


                }

                DataTable dtCertifications = new DataTable();
                //  dtCertifications.Columns.Add(new DataColumn("CertificationId", typeof(int)));
                dtCertifications.Columns.Add(new DataColumn("PassingDate", typeof(DateTime)));
                dtCertifications.Columns.Add(new DataColumn("Institute", typeof(string)));
                dtCertifications.Columns.Add(new DataColumn("Course", typeof(string)));
                dtCertifications.Columns.Add(new DataColumn("Grade", typeof(string)));
                if (bc.Certifications != null)
                {
                    foreach (BCCertifications cer in bc.Certifications)
                    {
                        DataRow drCertifications = dtCertifications.NewRow();
                        //drCertifications["CertificationId"] = cer.CertificationId;
                        drCertifications["PassingDate"] = cer.DateOfPassing;
                        drCertifications["Institute"] = cer.InstituteName;
                        drCertifications["Course"] = cer.CourseName;
                        drCertifications["Grade"] = cer.Grade;
                        dtCertifications.Rows.Add(drCertifications);

                        //spParam.Add(new SpParam("@institueName", cer.InstituteName));
                        //spParam.Add(new SpParam("@courseName", cer.CourseName));
                        //object newIDS = DBHelper.ExecProcScalar("spCreateInstitueCourse", spParam);
                        //retVal = int.Parse(newIDS.ToString());
                    }
                }
                spParams.Add(new SpParam("@AllCertifications", dtCertifications));


                DataTable dtPreviousExp = new DataTable();
                // dtPreviousExp.Columns.Add(new DataColumn("ExperienceId", typeof(int)));
                dtPreviousExp.Columns.Add(new DataColumn("BankId", typeof(int)));
                dtPreviousExp.Columns.Add(new DataColumn("BranchId", typeof(int)));
                dtPreviousExp.Columns.Add(new DataColumn("FromDate", typeof(DateTime)));
                dtPreviousExp.Columns.Add(new DataColumn("ToDate", typeof(DateTime)));
                dtPreviousExp.Columns.Add(new DataColumn("Reason", typeof(string)));
                dtPreviousExp.Columns.Add(new DataColumn("oBankName", typeof(string)));
                dtPreviousExp.Columns.Add(new DataColumn("oBranchName", typeof(string)));
                if (bc.PreviousExperience != null)
                {
                    foreach (PreviousExperiene exp in bc.PreviousExperience)
                    {
                        DataRow drPreviousExp = dtPreviousExp.NewRow();
                        //  drPreviousExp["ExperienceId"] = exp.ExperienceId;
                        drPreviousExp["BankId"] = exp.BankId;
                        drPreviousExp["BranchId"] = exp.Branchid;
                        drPreviousExp["FromDate"] = exp.FromDate;
                        drPreviousExp["ToDate"] = exp.ToDate;
                        drPreviousExp["Reason"] = exp.Reason;
                        drPreviousExp["oBankName"] = exp.oBankName;
                        drPreviousExp["oBranchName"] = exp.oBranchName;

                        dtPreviousExp.Rows.Add(drPreviousExp);

                    }
                }
                spParams.Add(new SpParam("@AllPreviousExperience", dtPreviousExp));


                DataTable dtOperationalAreas = new DataTable();
                //  dtOperationalAreas.Columns.Add(new DataColumn("AreaId", typeof(int)));
                dtOperationalAreas.Columns.Add(new DataColumn("VillageCode", typeof(string)));
                dtOperationalAreas.Columns.Add(new DataColumn("VillageDetails", typeof(string)));
                if (bc.OperationalAreas != null)
                {
                    foreach (OperationalAreas oper in bc.OperationalAreas)
                    {
                        DataRow drOperationalAreas = dtOperationalAreas.NewRow();
                        //        drOperationalAreas["AreaId"] = oper.AreaId;
                        drOperationalAreas["VillageCode"] = oper.VillageCode;
                        drOperationalAreas["VillageDetails"] = oper.VillageDetail;

                        dtOperationalAreas.Rows.Add(drOperationalAreas);
                    }
                }
                spParams.Add(new SpParam("@AllOperationalAreas", dtOperationalAreas));


                DataTable dtDevices = new DataTable();
                // dtDevices.Columns.Add(new DataColumn("DeviceId", typeof(int)));
                dtDevices.Columns.Add(new DataColumn("Device", typeof(string)));
                dtDevices.Columns.Add(new DataColumn("GivenOn", typeof(DateTime)));
                dtDevices.Columns.Add(new DataColumn("DeviceCode", typeof(string)));

                if (bc.Devices != null && bc.isAllocated)
                {
                    foreach (BCDevices dev in bc.Devices)
                    {
                        DataRow drDevices = dtDevices.NewRow();
                        //       drDevices["DeviceId"] = dev.DeviceId;
                        drDevices["Device"] = dev.Device;
                        drDevices["GivenOn"] = dev.GivenOn;
                        drDevices["DeviceCode"] = dev.DeviceCode;
                        dtDevices.Rows.Add(drDevices);
                    }
                }
                spParams.Add(new SpParam("@AllDevice", dtDevices));



                DataTable dtConnectivityDetails = new DataTable();
                //   dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityId", typeof(int)));
                dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityMode", typeof(string)));
                dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityProvider", typeof(string)));
                dtConnectivityDetails.Columns.Add(new DataColumn("ContactNumber", typeof(string)));
                if (bc.ConnectivityDetails != null && bc.isAllocated)
                {
                    foreach (ConnectivityDetails con in bc.ConnectivityDetails)
                    {
                        DataRow drConnectivityDetails = dtConnectivityDetails.NewRow();
                        //      drConnectivityDetails["ConnectivityId"] = con.ConnectivityId;
                        drConnectivityDetails["ConnectivityMode"] = con.ConnectivityMode;
                        drConnectivityDetails["ConnectivityProvider"] = con.ConnectivityProvider;
                        drConnectivityDetails["ContactNumber"] = con.ContactNumber;

                        dtConnectivityDetails.Rows.Add(drConnectivityDetails);
                    }
                }
                spParams.Add(new SpParam("@AllConnectivityDetails", dtConnectivityDetails));

                DataTable dtSSADetails = new DataTable();
                dtSSADetails.Columns.Add(new DataColumn("Ssa", typeof(string)));
                dtSSADetails.Columns.Add(new DataColumn("State", typeof(string)));
                dtSSADetails.Columns.Add(new DataColumn("District", typeof(string)));
                dtSSADetails.Columns.Add(new DataColumn("SubDistrict", typeof(string)));
                dtSSADetails.Columns.Add(new DataColumn("Village", typeof(string)));
                if (bc.SsaDetails != null)
                {
                    foreach (SsaDetails con in bc.SsaDetails)
                    {
                        DataRow drSsaDetails = dtSSADetails.NewRow();
                        drSsaDetails["Ssa"] = con.Ssa;
                        drSsaDetails["State"] = con.State;
                        drSsaDetails["District"] = con.District;
                        drSsaDetails["SubDistrict"] = con.SubDistrict;
                        drSsaDetails["Village"] = con.Village;

                        dtSSADetails.Rows.Add(drSsaDetails);
                    }
                }
                spParams.Add(new SpParam("@AllSsaDetails", dtSSADetails));

                //***************************************************************************************************************\\
                DataTable dtCommssionDetails = new DataTable();
                dtCommssionDetails.Columns.Add(new DataColumn("Month", typeof(int)));
                dtCommssionDetails.Columns.Add(new DataColumn("Year", typeof(string)));
                dtCommssionDetails.Columns.Add(new DataColumn("Commission1", typeof(string)));
                dtCommssionDetails.Columns.Add(new DataColumn("Commission2", typeof(string)));
                dtCommssionDetails.Columns.Add(new DataColumn("MonthName", typeof(string)));
                if (bc.CommssionList != null)
                {
                    foreach (CommssionList comm in bc.CommssionList)
                    {
                        DataRow drCommsionDetails = dtCommssionDetails.NewRow();
                        drCommsionDetails["Month"] = Convert.ToInt32(comm.Month);
                        drCommsionDetails["Year"] = comm.Year;
                        drCommsionDetails["Commission1"] = comm.Commission1;
                        drCommsionDetails["Commission2"] = comm.Commission2;
                        drCommsionDetails["MonthName"] = comm.MonthName;
                        dtCommssionDetails.Rows.Add(drCommsionDetails);
                    }
                }
                spParams.Add(new SpParam("@AllCommsionDetails", dtCommssionDetails));

                DataTable dtProducts = new DataTable();
                //     dtProducts.Columns.Add(new DataColumn("ProductId", typeof(int)));
                dtProducts.Columns.Add(new DataColumn("ProductName", typeof(string)));
                if (bc.Products != null && bc.isAllocated)
                {
                    foreach (BCProducts p in bc.Products)
                    {
                        DataRow drProduct = dtProducts.NewRow();
                        //           drProduct["ProductId"] = p.ProductId;
                        drProduct["ProductName"] = p.ProductName;

                        dtProducts.Rows.Add(drProduct);
                    }
                }
                spParams.Add(new SpParam("@Products", dtProducts));



                object newID = DBHelper.ExecProcScalar("sp_AddEditBankCorresponds", spParams);
                retVal = int.Parse(newID.ToString());
                SpParamCollection spsParams = new SpParamCollection();
                SpParamCollection spsSParams = new SpParamCollection();
                spsSParams.Add(new SpParam("@bcid", retVal));
                object newIDsa = DBHelper.ExecProcScalar("spDeleteCommssion", spsSParams);
                if (bc.CommssionList != null)
                {
                    foreach (CommssionList comm in bc.CommssionList)
                    {

                        spsParams.Add(new SpParam("@bcid", retVal));
                        spsParams.Add(new SpParam("@Month", comm.Month));
                        spsParams.Add(new SpParam("@Year", comm.Year));
                        spsParams.Add(new SpParam("@Commission1", comm.Commission1));
                        spsParams.Add(new SpParam("@Commission2", comm.Commission2));
                        spsParams.Add(new SpParam("@MonthName", comm.MonthName));

                        object newIDs = DBHelper.ExecProcScalar("spCreateCommssion", spsParams);
                        spsParams.Remove(new SpParam("@bcid", retVal));
                        spsParams.Remove(new SpParam("@Month", comm.Month));
                        spsParams.Remove(new SpParam("@Year", comm.Year));
                        spsParams.Remove(new SpParam("@Commission1", comm.Commission1));
                        spsParams.Remove(new SpParam("@Commission2", comm.Commission2));
                        spsParams.Remove(new SpParam("@MonthName", comm.MonthName));
                    }
                }


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        public int InsertUpdateProspectiveRegistry(BankCorrespondence bc, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {
                if (mode == Constants.UPDATEMODE)
                {
                    spParams.Add(new SpParam("@ProspectiveBCRegistry", bc.BankCorrespondId));
                }
                else
                {
                    spParams.Add(new SpParam("@ProspectiveBCRegistry", 0));
                }

                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@FName", bc.FName));
                spParams.Add(new SpParam("@LName", bc.LName));
                spParams.Add(new SpParam("@ImagePath", bc.ImagePath));
                spParams.Add(new SpParam("@Gender", bc.Gender));
                spParams.Add(new SpParam("@DOB", bc.DOB.Value));
                spParams.Add(new SpParam("@FatherName", bc.FatherName));
                spParams.Add(new SpParam("@SpouseName", bc.SpouseName));
              //  spParams.Add(new SpParam("@Category", bc.Category));
              //  spParams.Add(new SpParam("@PhysicallyHandicap", bc.Handicap == "No" ? false : true));
                spParams.Add(new SpParam("@PhoneNumber1", bc.PhoneNumber1));
                spParams.Add(new SpParam("@PhoneNumber2", bc.PhoneNumber2));
                spParams.Add(new SpParam("@PhoneNumber3", bc.PhoneNumber3));
                spParams.Add(new SpParam("@PhoneNumber4", bc.referencePhoneNumber));
            //    spParams.Add(new SpParam("@ContactPerson", bc.ContactPerson));
           //     spParams.Add(new SpParam("@ContactDesignation", bc.ContactDesignation));
           //     spParams.Add(new SpParam("@NoofComplaint", bc.NoofComplaint));
                spParams.Add(new SpParam("@Email", bc.Email));
                spParams.Add(new SpParam("@AadharCard", bc.AadharCard));
                spParams.Add(new SpParam("@PanCard", bc.PanCard));
                spParams.Add(new SpParam("@VotersIdCard", bc.VoterCard));
                spParams.Add(new SpParam("@DriverLicense", bc.DriverLicense));
                spParams.Add(new SpParam("@NREGACard", bc.NregaCard));
                spParams.Add(new SpParam("@RationCard", bc.RationCard));
             ///   spParams.Add(new SpParam("@AddressState", bc.State));
                //spParams.Add(new SpParam("@AddressCity", bc.City));
               // spParams.Add(new SpParam("@AddressDistrict", bc.District));
               // spParams.Add(new SpParam("@AddressSubDistrict", bc.Subdistrict));
               // spParams.Add(new SpParam("@Village", bc.Village));
               // spParams.Add(new SpParam("@productId", bc.productId));

              //  spParams.Add(new SpParam("@Longitude", bc.Longitude));
               // spParams.Add(new SpParam("@Latitude", bc.Latitude));

               // spParams.Add(new SpParam("@AddressArea", bc.Area));
               // spParams.Add(new SpParam("@PinCode", bc.PinCode));
                spParams.Add(new SpParam("@AlternateOccupationType", bc.AlternateOccupationType));
                spParams.Add(new SpParam("@AlternateOccupationDetail", bc.AlternateOccupationDetail));
                spParams.Add(new SpParam("@UniqueIdentificationNumber", bc.UniqueIdentificationNumber));
               // spParams.Add(new SpParam("@BankReferenceNumber", bc.BankReferenceNumber));
                spParams.Add(new SpParam("@EducationalQualification", bc.Qualification));
                spParams.Add(new SpParam("@OtherQualification", bc.OtherQualification));
              //  spParams.Add(new SpParam("@CorporateId", bc.CorporateId));
               // spParams.Add(new SpParam("@IsAllocated", bc.isAllocated));

                //if (bc.isAllocated)
                //{
                //    spParams.Add(new SpParam("@AppointmentDate", bc.AppointmentDate));
                //    spParams.Add(new SpParam("@AllocationIFSCCode", bc.AllocationIFSCCode));
                //    spParams.Add(new SpParam("@AllocationBankId", bc.AllocationBankId));
                //    spParams.Add(new SpParam("@AllocationBranchId", bc.AllocationBranchId));

                //    spParams.Add(new SpParam("@BCType", bc.BCType));
                //    spParams.Add(new SpParam("@WorkingDays", bc.WorkingDays));
                //    spParams.Add(new SpParam("@WorkingHours", bc.WorkingHours));

                //    spParams.Add(new SpParam("@PLPostalAddress", bc.PLPostalAddress));
                //    spParams.Add(new SpParam("@PLVillageCode", bc.PLVillageCode));
                //    spParams.Add(new SpParam("@PLVillageDetail", bc.PLVillageDetail));
                //    spParams.Add(new SpParam("@PLStateId", bc.PLStateId));
                //    spParams.Add(new SpParam("@PLDistrictId", bc.PLDistrictId));
                //    spParams.Add(new SpParam("@PLTaluk", bc.PLTaluk));
                //    spParams.Add(new SpParam("@PLPinCode", bc.PLPinCode));

                //    spParams.Add(new SpParam("@MinimumCashHandlingLimit", bc.MinimumCashHandlingLimit));
                //    spParams.Add(new SpParam("@MonthlyFixedRenumeration", bc.MonthlyFixedRenumeration));
                //    spParams.Add(new SpParam("@MonthlyVariableRenumeration", bc.MonthlyVariableRenumeration));


                //}

                DataTable dtCertifications = new DataTable();
                //  dtCertifications.Columns.Add(new DataColumn("CertificationId", typeof(int)));
                dtCertifications.Columns.Add(new DataColumn("PassingDate", typeof(DateTime)));
                dtCertifications.Columns.Add(new DataColumn("Institute", typeof(string)));
                dtCertifications.Columns.Add(new DataColumn("Course", typeof(string)));
                dtCertifications.Columns.Add(new DataColumn("Grade", typeof(string)));
                if (bc.Certifications != null)
                {
                    foreach (BCCertifications cer in bc.Certifications)
                    {
                        DataRow drCertifications = dtCertifications.NewRow();
                        //drCertifications["CertificationId"] = cer.CertificationId;
                        drCertifications["PassingDate"] = cer.DateOfPassing;
                        drCertifications["Institute"] = cer.InstituteName;
                        drCertifications["Course"] = cer.CourseName;
                        drCertifications["Grade"] = cer.Grade;
                        dtCertifications.Rows.Add(drCertifications);

                        //spParam.Add(new SpParam("@institueName", cer.InstituteName));
                        //spParam.Add(new SpParam("@courseName", cer.CourseName));
                        //object newIDS = DBHelper.ExecProcScalar("spCreateInstitueCourse", spParam);
                        //retVal = int.Parse(newIDS.ToString());
                    }
                }
                spParams.Add(new SpParam("@AllCertifications", dtCertifications));


                DataTable dtPreviousExp = new DataTable();
                // dtPreviousExp.Columns.Add(new DataColumn("ExperienceId", typeof(int)));
                dtPreviousExp.Columns.Add(new DataColumn("BankId", typeof(int)));
                dtPreviousExp.Columns.Add(new DataColumn("BranchId", typeof(int)));
                dtPreviousExp.Columns.Add(new DataColumn("FromDate", typeof(DateTime)));
                dtPreviousExp.Columns.Add(new DataColumn("ToDate", typeof(DateTime)));
                dtPreviousExp.Columns.Add(new DataColumn("Reason", typeof(string)));
                dtPreviousExp.Columns.Add(new DataColumn("oBankName", typeof(string)));
                dtPreviousExp.Columns.Add(new DataColumn("oBranchName", typeof(string)));
                if (bc.PreviousExperience != null)
                {
                    foreach (PreviousExperiene exp in bc.PreviousExperience)
                    {
                        DataRow drPreviousExp = dtPreviousExp.NewRow();
                        //  drPreviousExp["ExperienceId"] = exp.ExperienceId;
                        drPreviousExp["BankId"] = exp.BankId;
                        drPreviousExp["BranchId"] = exp.Branchid;
                        drPreviousExp["FromDate"] = exp.FromDate;
                        drPreviousExp["ToDate"] = exp.ToDate;
                        drPreviousExp["Reason"] = exp.Reason;
                        drPreviousExp["oBankName"] = exp.oBankName;
                        drPreviousExp["oBranchName"] = exp.oBranchName;

                        dtPreviousExp.Rows.Add(drPreviousExp);

                    }
                }
                spParams.Add(new SpParam("@AllPreviousExperience", dtPreviousExp));


                DataTable dtOperationalAreas = new DataTable();
                //  dtOperationalAreas.Columns.Add(new DataColumn("AreaId", typeof(int)));
                dtOperationalAreas.Columns.Add(new DataColumn("VillageCode", typeof(string)));
                dtOperationalAreas.Columns.Add(new DataColumn("VillageDetails", typeof(string)));
                if (bc.OperationalAreas != null)
                {
                    foreach (OperationalAreas oper in bc.OperationalAreas)
                    {
                        DataRow drOperationalAreas = dtOperationalAreas.NewRow();
                        //        drOperationalAreas["AreaId"] = oper.AreaId;
                        drOperationalAreas["VillageCode"] = oper.VillageCode;
                        drOperationalAreas["VillageDetails"] = oper.VillageDetail;

                        dtOperationalAreas.Rows.Add(drOperationalAreas);
                    }
                }
                spParams.Add(new SpParam("@AllOperationalAreas", dtOperationalAreas));


                DataTable dtDevices = new DataTable();
                // dtDevices.Columns.Add(new DataColumn("DeviceId", typeof(int)));
                dtDevices.Columns.Add(new DataColumn("Device", typeof(string)));
                dtDevices.Columns.Add(new DataColumn("GivenOn", typeof(DateTime)));
                dtDevices.Columns.Add(new DataColumn("DeviceCode", typeof(string)));

                if (bc.Devices != null && bc.isAllocated)
                {
                    foreach (BCDevices dev in bc.Devices)
                    {
                        DataRow drDevices = dtDevices.NewRow();
                        //       drDevices["DeviceId"] = dev.DeviceId;
                        drDevices["Device"] = dev.Device;
                        drDevices["GivenOn"] = dev.GivenOn;
                        drDevices["DeviceCode"] = dev.DeviceCode;
                        dtDevices.Rows.Add(drDevices);
                    }
                }
                spParams.Add(new SpParam("@AllDevice", dtDevices));



                DataTable dtConnectivityDetails = new DataTable();
                //   dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityId", typeof(int)));
                dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityMode", typeof(string)));
                dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityProvider", typeof(string)));
                dtConnectivityDetails.Columns.Add(new DataColumn("ContactNumber", typeof(string)));
                if (bc.ConnectivityDetails != null && bc.isAllocated)
                {
                    foreach (ConnectivityDetails con in bc.ConnectivityDetails)
                    {
                        DataRow drConnectivityDetails = dtConnectivityDetails.NewRow();
                        //      drConnectivityDetails["ConnectivityId"] = con.ConnectivityId;
                        drConnectivityDetails["ConnectivityMode"] = con.ConnectivityMode;
                        drConnectivityDetails["ConnectivityProvider"] = con.ConnectivityProvider;
                        drConnectivityDetails["ContactNumber"] = con.ContactNumber;

                        dtConnectivityDetails.Rows.Add(drConnectivityDetails);
                    }
                }
                spParams.Add(new SpParam("@AllConnectivityDetails", dtConnectivityDetails));

                DataTable dtSSADetails = new DataTable();
                dtSSADetails.Columns.Add(new DataColumn("Ssa", typeof(string)));
                dtSSADetails.Columns.Add(new DataColumn("State", typeof(string)));
                dtSSADetails.Columns.Add(new DataColumn("District", typeof(string)));
                dtSSADetails.Columns.Add(new DataColumn("SubDistrict", typeof(string)));
                dtSSADetails.Columns.Add(new DataColumn("Village", typeof(string)));
                if (bc.SsaDetails != null)
                {
                    foreach (SsaDetails con in bc.SsaDetails)
                    {
                        DataRow drSsaDetails = dtSSADetails.NewRow();
                        drSsaDetails["Ssa"] = con.Ssa;
                        drSsaDetails["State"] = con.State;
                        drSsaDetails["District"] = con.District;
                        drSsaDetails["SubDistrict"] = con.SubDistrict;
                        drSsaDetails["Village"] = con.Village;

                        dtSSADetails.Rows.Add(drSsaDetails);
                    }
                }
                spParams.Add(new SpParam("@AllSsaDetails", dtSSADetails));

                //DataTable dtProducts = new DataTable();
                ////     dtProducts.Columns.Add(new DataColumn("ProductId", typeof(int)));
                //dtProducts.Columns.Add(new DataColumn("ProductName", typeof(string)));
                //if (bc.Products != null && bc.isAllocated)
                //{
                //    foreach (BCProducts p in bc.Products)
                //    {
                //        DataRow drProduct = dtProducts.NewRow();
                //        //           drProduct["ProductId"] = p.ProductId;
                //        drProduct["ProductName"] = p.ProductName;

                //        dtProducts.Rows.Add(drProduct);
                //    }
                //}
                //spParams.Add(new SpParam("@Products", dtProducts));



                object newID = DBHelper.ExecProcScalar("Sp_addeditprospectiveRegistry", spParams);
                retVal = int.Parse(newID.ToString());


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        public int InsertUpdateBankCorrespondsexcel(BankCorrespondence bc, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {
                if (bc.BankCorrespondId==0)
                {
                 
                    spParams.Add(new SpParam("@BankCorrespondId", 0));
                }
                else
                {
                    spParams.Add(new SpParam("@BankCorrespondId", bc.BankCorrespondId));
                }
                
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@Name", bc.Name));
                spParams.Add(new SpParam("@ImagePath", bc.ImagePath));
                spParams.Add(new SpParam("@Gender", bc.Gender));
                spParams.Add(new SpParam("@DOB", bc.DOB.Value));
                spParams.Add(new SpParam("@dateOfBirth", bc.dateOfBirth));
                spParams.Add(new SpParam("@Appointment_Date", bc.Appointment_Date));                    
                spParams.Add(new SpParam("@FatherName", bc.FatherName));
                spParams.Add(new SpParam("@SpouseName", bc.SpouseName));
                spParams.Add(new SpParam("@Category", bc.Category));
                spParams.Add(new SpParam("@PhysicallyHandicap", bc.Handicap == "No" ? false : true));
                spParams.Add(new SpParam("@PhoneNumber1", bc.PhoneNumber1));
                spParams.Add(new SpParam("@PhoneNumber2", bc.PhoneNumber2));
                spParams.Add(new SpParam("@PhoneNumber3", bc.PhoneNumber3));
                spParams.Add(new SpParam("@Email", bc.Email));
                spParams.Add(new SpParam("@AadharCard", bc.AadharCard));
                spParams.Add(new SpParam("@PanCard", bc.PanCard));
                spParams.Add(new SpParam("@VotersIdCard", bc.VoterCard));
                spParams.Add(new SpParam("@DriverLicense", bc.DriverLicense));
                spParams.Add(new SpParam("@NREGACard", bc.NregaCard));
                spParams.Add(new SpParam("@RationCard", bc.RationCard));
                spParams.Add(new SpParam("@AddressState", bc.State));
                spParams.Add(new SpParam("@AddressCity", bc.City));
                spParams.Add(new SpParam("@AddressDistrict", bc.District));
                spParams.Add(new SpParam("@AddressSubDistrict", bc.Subdistrict));
                spParams.Add(new SpParam("@Village", bc.Village));
                spParams.Add(new SpParam("@productId", bc.productId));
                spParams.Add(new SpParam("@Latitude", bc.Latitude));
                spParams.Add(new SpParam("@Longitude", bc.Longitude));
                spParams.Add(new SpParam("@ContactPerson", bc.ContactPerson));
                spParams.Add(new SpParam("@ContactDesignation", bc.ContactDesignation));
                spParams.Add(new SpParam("@NoofComplaint", bc.NoofComplaint));

                spParams.Add(new SpParam("@AddressArea", bc.Area));
                spParams.Add(new SpParam("@PinCode", bc.PinCode));
                spParams.Add(new SpParam("@AlternateOccupationType", bc.AlternateOccupationType));
                spParams.Add(new SpParam("@AlternateOccupationDetail", bc.AlternateOccupationDetail));
                spParams.Add(new SpParam("@UniqueIdentificationNumber", bc.UniqueIdentificationNumber));
                spParams.Add(new SpParam("@BankReferenceNumber", bc.BankReferenceNumber));
                spParams.Add(new SpParam("@EducationalQualification", bc.Qualification));
                spParams.Add(new SpParam("@OtherQualification", bc.OtherQualification));
                spParams.Add(new SpParam("@CorporateId", bc.CorporateId));
                spParams.Add(new SpParam("@IsAllocated", bc.isAllocated));

                if (bc.isAllocated)
                {
                    spParams.Add(new SpParam("@AppointmentDate", bc.AppointmentDate));
                    spParams.Add(new SpParam("@AllocationIFSCCode", bc.AllocationIFSCCode));
                    spParams.Add(new SpParam("@AllocationBankId", bc.AllocationBankId));
                    spParams.Add(new SpParam("@AllocationBranchId", bc.AllocationBranchId));

                    spParams.Add(new SpParam("@BCType", bc.BCType));
                    spParams.Add(new SpParam("@WorkingDays", bc.WorkingDays));
                    spParams.Add(new SpParam("@WorkingHours", bc.WorkingHours));

                    spParams.Add(new SpParam("@PLPostalAddress", bc.PLPostalAddress));
                    spParams.Add(new SpParam("@PLVillageCode", bc.PLVillageCode));
                    spParams.Add(new SpParam("@PLVillageDetail", bc.PLVillageDetail));
                    spParams.Add(new SpParam("@PLStateId", bc.PLStateId));
                    spParams.Add(new SpParam("@PLDistrictId", bc.PLDistrictId));
                    spParams.Add(new SpParam("@PLTaluk", bc.PLTaluk));
                    spParams.Add(new SpParam("@PLPinCode", bc.PLPinCode));

                    spParams.Add(new SpParam("@MinimumCashHandlingLimit", bc.MinimumCashHandlingLimit));
                    spParams.Add(new SpParam("@MonthlyFixedRenumeration", bc.MonthlyFixedRenumeration));
                    spParams.Add(new SpParam("@MonthlyVariableRenumeration", bc.MonthlyVariableRenumeration));
                   


                }

                DataTable dtCertifications = new DataTable();
                //  dtCertifications.Columns.Add(new DataColumn("CertificationId", typeof(int)));
                dtCertifications.Columns.Add(new DataColumn("PassingDate", typeof(DateTime)));
                dtCertifications.Columns.Add(new DataColumn("Institute", typeof(string)));
                dtCertifications.Columns.Add(new DataColumn("Course", typeof(string)));
                dtCertifications.Columns.Add(new DataColumn("Grade", typeof(string)));
                if (bc.Certifications != null)
                {
                    foreach (BCCertifications cer in bc.Certifications)
                    {
                        DataRow drCertifications = dtCertifications.NewRow();
                        //drCertifications["CertificationId"] = cer.CertificationId;
                        drCertifications["PassingDate"] = cer.DateOfPassing;
                        drCertifications["Institute"] = cer.InstituteName;
                        drCertifications["Course"] = cer.CourseName;
                        drCertifications["Grade"] = cer.Grade;
                        dtCertifications.Rows.Add(drCertifications);

                        //spParam.Add(new SpParam("@institueName", cer.InstituteName));
                        //spParam.Add(new SpParam("@courseName", cer.CourseName));
                        //object newIDS = DBHelper.ExecProcScalar("spCreateInstitueCourse", spParam);
                        //retVal = int.Parse(newIDS.ToString());
                    }
                }
             //   spParams.Add(new SpParam("@AllCertifications", dtCertifications));


                DataTable dtPreviousExp = new DataTable();
                // dtPreviousExp.Columns.Add(new DataColumn("ExperienceId", typeof(int)));
                dtPreviousExp.Columns.Add(new DataColumn("BankId", typeof(int)));
                dtPreviousExp.Columns.Add(new DataColumn("BranchId", typeof(int)));
                dtPreviousExp.Columns.Add(new DataColumn("FromDate", typeof(DateTime)));
                dtPreviousExp.Columns.Add(new DataColumn("ToDate", typeof(DateTime)));
                dtPreviousExp.Columns.Add(new DataColumn("Reason", typeof(string)));
                if (bc.PreviousExperience != null)
                {
                    foreach (PreviousExperiene exp in bc.PreviousExperience)
                    {
                        DataRow drPreviousExp = dtPreviousExp.NewRow();
                        //  drPreviousExp["ExperienceId"] = exp.ExperienceId;
                        drPreviousExp["BankId"] = exp.BankId;
                        drPreviousExp["BranchId"] = exp.Branchid;
                        drPreviousExp["FromDate"] = exp.FromDate;
                        drPreviousExp["ToDate"] = exp.ToDate;
                        drPreviousExp["Reason"] = exp.Reason;
                        dtPreviousExp.Rows.Add(drPreviousExp);
                    }
                }
         //       spParams.Add(new SpParam("@AllPreviousExperience", dtPreviousExp));


                DataTable dtOperationalAreas = new DataTable();
                //  dtOperationalAreas.Columns.Add(new DataColumn("AreaId", typeof(int)));
                dtOperationalAreas.Columns.Add(new DataColumn("VillageCode", typeof(string)));
                dtOperationalAreas.Columns.Add(new DataColumn("VillageDetails", typeof(string)));
                if (bc.OperationalAreas != null)
                {
                    foreach (OperationalAreas oper in bc.OperationalAreas)
                    {
                        DataRow drOperationalAreas = dtOperationalAreas.NewRow();
                        //        drOperationalAreas["AreaId"] = oper.AreaId;
                        drOperationalAreas["VillageCode"] = oper.VillageCode;
                        drOperationalAreas["VillageDetails"] = oper.VillageDetail;

                        dtOperationalAreas.Rows.Add(drOperationalAreas);
                    }
                }
           //     spParams.Add(new SpParam("@AllOperationalAreas", dtOperationalAreas));


                DataTable dtDevices = new DataTable();
                // dtDevices.Columns.Add(new DataColumn("DeviceId", typeof(int)));
                dtDevices.Columns.Add(new DataColumn("Device", typeof(string)));
                dtDevices.Columns.Add(new DataColumn("GivenOn", typeof(DateTime)));
                if (bc.Devices != null && bc.isAllocated)
                {
                    foreach (BCDevices dev in bc.Devices)
                    {
                        DataRow drDevices = dtDevices.NewRow();
                        //       drDevices["DeviceId"] = dev.DeviceId;
                        drDevices["Device"] = dev.Device;
                        drDevices["GivenOn"] = dev.GivenOn;

                        dtDevices.Rows.Add(drDevices);
                    }
                }
           //     spParams.Add(new SpParam("@AllDevice", dtDevices));



                DataTable dtConnectivityDetails = new DataTable();
                //   dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityId", typeof(int)));
                dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityMode", typeof(string)));
                dtConnectivityDetails.Columns.Add(new DataColumn("ConnectivityProvider", typeof(string)));
                dtConnectivityDetails.Columns.Add(new DataColumn("ContactNumber", typeof(string)));
                if (bc.ConnectivityDetails != null && bc.isAllocated)
                {
                    foreach (ConnectivityDetails con in bc.ConnectivityDetails)
                    {
                        DataRow drConnectivityDetails = dtConnectivityDetails.NewRow();
                        //      drConnectivityDetails["ConnectivityId"] = con.ConnectivityId;
                        drConnectivityDetails["ConnectivityMode"] = con.ConnectivityMode;
                        drConnectivityDetails["ConnectivityProvider"] = con.ConnectivityProvider;
                        drConnectivityDetails["ContactNumber"] = con.ContactNumber;

                        dtConnectivityDetails.Rows.Add(drConnectivityDetails);
                    }
                }
         //       spParams.Add(new SpParam("@AllConnectivityDetails", dtConnectivityDetails));


                DataTable dtProducts = new DataTable();
                //     dtProducts.Columns.Add(new DataColumn("ProductId", typeof(int)));
                dtProducts.Columns.Add(new DataColumn("ProductName", typeof(string)));
                if (bc.Products != null && bc.isAllocated)
                {
                    foreach (BCProducts p in bc.Products)
                    {
                        DataRow drProduct = dtProducts.NewRow();
                        //           drProduct["ProductId"] = p.ProductId;
                        drProduct["ProductName"] = p.ProductName;

                        dtProducts.Rows.Add(drProduct);
                    }
                }
           //     spParams.Add(new SpParam("@Products", dtProducts));



                object newID = DBHelper.ExecProcScalar("sp_AddEditBankCorrespondsexcel", spParams);
                retVal = int.Parse(newID.ToString());


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in sp_AddEditBankCorrespondsexcel: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        public int InsertUpdateBCforExcelCertification(BCCertifications cer, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {

                spParams.Add(new SpParam("@AadharCard", cer.AadharCard));
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@DateOfPassing", cer.DateOfPassing));
                spParams.Add(new SpParam("@DateOf_Passing", cer.DateOf_Passing));
                spParams.Add(new SpParam("@InstituteName", cer.InstituteName));
                spParams.Add(new SpParam("@CourseName", cer.CourseName));
                spParams.Add(new SpParam("@Grade", cer.Grade));
                object newID = DBHelper.ExecProcScalar("sp_AddInserCertification", spParams);
                retVal = int.Parse(newID.ToString());


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }

        public int InsertUpdateBCforExcelRemuneration(CommssionList cer, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {

                spParams.Add(new SpParam("@AadharCard", cer.BankRefNo));
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@Month", cer.Month));
                spParams.Add(new SpParam("@MonthName", cer.MonthName));
                spParams.Add(new SpParam("@Year", cer.Year));
                spParams.Add(new SpParam("@Commission1", cer.Commission1));
                spParams.Add(new SpParam("@Commission2", cer.Commission2));
                object newID = DBHelper.ExecProcScalar("Sp_addinserRemuneration", spParams);
                retVal = int.Parse(newID.ToString());


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }

        
        public int InsertUpdateBCforExcelPreviousExperiene(PreviousExperiene exp, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {

                spParams.Add(new SpParam("@AadharCard", exp.AadharCard));   
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@BankId", exp.BankId));
                spParams.Add(new SpParam("@Branchid", exp.Branchid));
                spParams.Add(new SpParam("@oBankName", exp.BankName));
                spParams.Add(new SpParam("@oBranchName", exp.BranchName));
                spParams.Add(new SpParam("@FromDate", exp.FromDate));
                spParams.Add(new SpParam("@ToDate", exp.ToDate));
                spParams.Add(new SpParam("@From_Date", exp.From_Date));
                spParams.Add(new SpParam("@To_Date", exp.To_Date));
                spParams.Add(new SpParam("@Reasons", exp.Reason));
                object newID = DBHelper.ExecProcScalar("sp_AddInserPreviousExperience", spParams);
                retVal = int.Parse(newID.ToString());


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        public int InsertUpdateBCforExcelOperationalAreas(OperationalAreas ar, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@AadharCard", ar.AadharCard));   
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@VillageCode", ar.VillageCode));
                spParams.Add(new SpParam("@VillageDetail", ar.VillageDetail));

                object newID = DBHelper.ExecProcScalar("sp_AddInsertOperationAreas", spParams);
                retVal = int.Parse(newID.ToString());


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        public int InsertUpdateBCforExcelBCDevices(BCDevices ar, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@AadharCard", ar.AadharCard));   
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@Device", ar.Device));
                spParams.Add(new SpParam("@GivenOn", ar.GivenOn));
                spParams.Add(new SpParam("@Given_On", ar.Given_On));
                spParams.Add(new SpParam("@DeviceCode", ar.DeviceCode));
                
                object newID = DBHelper.ExecProcScalar("sp_AddInsertBCDevices", spParams);
                retVal = int.Parse(newID.ToString());


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        public int InsertUpdateBCforExcelConnectivityDetails(ConnectivityDetails ar, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@AadharCard", ar.AadharCard));   
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@ConnectivityMode", ar.ConnectivityMode));
                spParams.Add(new SpParam("@ConnectivityProvider", ar.ConnectivityProvider));
                spParams.Add(new SpParam("@ContactNumber", ar.ContactNumber));

                object newID = DBHelper.ExecProcScalar("sp_AddInsertConnectivityDetails", spParams);
                retVal = int.Parse(newID.ToString());


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }

        public int InsertUpdateBCforExcelSSADetails(SsaDetails ar, int userId, string mode)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            SpParamCollection spParam = new SpParamCollection();

            try
            {
                spParams.Add(new SpParam("@AadharCard", ar.AadharCard));   
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@Ssa", ar.Ssa));
                spParams.Add(new SpParam("@State", ar.State));
                spParams.Add(new SpParam("@District", ar.District));
                spParams.Add(new SpParam("@Village", ar.Village));
                spParams.Add(new SpParam("@subdistrict", ar.SubDistrict));

                object newID = DBHelper.ExecProcScalar("sp_AddInsertSSADetails", spParams);
                retVal = int.Parse(newID.ToString());


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        
        public void DeleteBankCorresponds(int BankCorrespondId) {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@BankCorrespondId", BankCorrespondId));
                DBHelper.ExecProcNonQuery("sp_DeleteBankCorresponds", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in DeleteBankCorresponds: " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }

        public int InsertUpdateBCProductOffered(BCProducts product, string mode) {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try {
                if (mode == Constants.UPDATEMODE) {
                    spParams.Add(new SpParam("@ProductId", product.ProductId));
                }
                else {
                    spParams.Add(new SpParam("@ProductId", 0));
                }

                spParams.Add(new SpParam("@ProductName", product.ProductName));
                //spParams.Add(new SpParam("@BCId", p));
                object newID = DBHelper.ExecProcScalar("sp_AddEditBCProductsOffered", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in InsertUpdateBCProductsOffered: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally {
                conn.Close();
            }
            return retVal;
        }

        public void DeleteBCProductOffered(int ProductId) {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@ProductId", ProductId));
                DBHelper.ExecProcNonQuery("sp_DeleteBCProductsOffered", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in DeleteBCProductsOffered: " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }

        public int InsertUpdateBranches(Branch branches, string mode) {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try {
                if (mode == Constants.UPDATEMODE) {
                    spParams.Add(new SpParam("@BranchId", branches.BranchId));
                }
                else {
                    spParams.Add(new SpParam("@BranchId", 0));
                }

                spParams.Add(new SpParam("@BranchName", branches.BranchName));
                spParams.Add(new SpParam("@IFSCCode", branches.IFSCCode));
                spParams.Add(new SpParam("@MICRCode", branches.MICRCode));
                spParams.Add(new SpParam("@Address", branches.Address));
                spParams.Add(new SpParam("@ContactNumber", branches.ContactNumber));
                spParams.Add(new SpParam("@Email", branches.Email));
                object newID = DBHelper.ExecProcScalar("sp_AddEditBranches", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in InsertUpdatesBranches: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally {
                conn.Close();
            }
            return retVal;
        }

        public void DeleteBranches(int BranchId) {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@BranchId", BranchId));
                DBHelper.ExecProcNonQuery("sp_DeleteBranches", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in DeleteBranches: " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }

        public int InsertUpdateCorporates(Corporates corporates, string mode, int userId) {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try {
                if (mode == Constants.UPDATEMODE) {
                    spParams.Add(new SpParam("@CorporateId", corporates.CorporateId));
                }
                else {
                    spParams.Add(new SpParam("@CorporateId", 0));
                }

                spParams.Add(new SpParam("@CorporateName", corporates.CorporateName));
                spParams.Add(new SpParam("@CorporateType", corporates.CorporateType));
                spParams.Add(new SpParam("@Address", corporates.Address));
                spParams.Add(new SpParam("@ContactNumber", corporates.ContactNumber));
                spParams.Add(new SpParam("@Email", corporates.Email));
                spParams.Add(new SpParam("@PanNo", corporates.PanNo));
                spParams.Add(new SpParam("@WebSite", corporates.WebSite));
                spParams.Add(new SpParam("@ContactPerson", corporates.ContactPerson));
                spParams.Add(new SpParam("@ContactDesignation", corporates.ContactDesignation));
                spParams.Add(new SpParam("@NoofComplaint", Convert.ToInt32(corporates.NoOfComplaint)));
                spParams.Add(new SpParam("@UserId", userId));
                object newID = DBHelper.ExecProcScalar("sp_AddEditCorporates", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in InsertUpdatesCorporates: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally {
                conn.Close();
            }
            return retVal;
        }

        public void DeleteCorporates(int CorporateId) {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@CorporateId", CorporateId));
                DBHelper.ExecProcNonQuery("sp_DeleteCorporates", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in DeleteCorporates: " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }

        //public int InsertUpdateCurrentAllocation(CurrentAllocations curalloctaions, string mode)
        //{
        //    int retVal = -1;
        //    conn = new SqlConnection(Constants.connString);
        //    conn.Open();
        //    SpParamCollection spParams = new SpParamCollection();

        //    try
        //    {
        //        if (mode == Constants.UPDATEMODE)
        //        {
        //            spParams.Add(new SpParam("@AllocationId", curalloctaions.AllocationId));
        //        }
        //        else
        //        {
        //            spParams.Add(new SpParam("@AllocationId", 0));
        //        }

        //        spParams.Add(new SpParam("@BankId", curalloctaions.BankId));
        //        spParams.Add(new SpParam("@BranchId",curalloctaions.BranchId));
        //        spParams.Add(new SpParam("@AppointmentDate", curalloctaions.AppointmentDate));
        //        spParams.Add(new SpParam("@BankCorrespondenceId",curalloctaions.BankCorrespondenceId));
        //        spParams.Add(new SpParam("@BCType", curalloctaions.BCType));
        //        spParams.Add(new SpParam("@WorkingDays",curalloctaions.WorkingDays));
        //        spParams.Add(new SpParam("@WorkingHours", curalloctaions.WorkingHours));
        //        spParams.Add(new SpParam("@PLPostalAddress", curalloctaions.PLPostalAddress));
        //        spParams.Add(new SpParam("@PLVillageCode", curalloctaions.PLVillageCode));
        //        spParams.Add(new SpParam("@PLVillageDetail", curalloctaions.PLVillageDetail));
        //        spParams.Add(new SpParam("@PLTaluk", curalloctaions.PLTaluk));
        //        spParams.Add(new SpParam("@PLDistrict", curalloctaions.PLDistrict));
        //        spParams.Add(new SpParam("@PLState", curalloctaions.PLState));
        //        spParams.Add(new SpParam("@PLPinCode", curalloctaions.PLPinCode));
        //        spParams.Add(new SpParam("@ConnectivityProvider", curalloctaions.ConnectivityProviders));
        //        spParams.Add(new SpParam("@MinimumCashHandlingLimit", curalloctaions.MinimumCashHandlingLimit));
        //        spParams.Add(new SpParam("@MonthlyFixedRenumeration", curalloctaions.MonthlyFixedRenumeration));
        //        spParams.Add(new SpParam("@MonthlyVariableRenumeration", curalloctaions.MonthlyVariableRenumeration));
        //        object newID = DBHelper.ExecProcScalar("sp_AddEditCurrentAllocation", spParams);
        //        retVal = int.Parse(newID.ToString());
        //    }
        //    catch (Exception ex)
        //    {
        //        BCTrackingLogger.Error("Error in InsertUpdateCurrentAllocation: " + ex.Message + "<br/>" + ex.StackTrace);

        //        throw ex;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //    return retVal;
        //}

        public void DeleteCurrentAllocation(int AllocationId) {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@AllocationId", AllocationId));
                DBHelper.ExecProcNonQuery("sp_DeleteCurrentAllocation", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in DeleteCurrentAllocation " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }

        public int InsertUpdateUSers(UserEntity users, string mode, int userid)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try
            {
                if (mode == Constants.UPDATEMODE)
                {
                    spParams.Add(new SpParam("@UserId", users.UserId));
                }
                else
                {
                    spParams.Add(new SpParam("@UserId", 0));
                }

                spParams.Add(new SpParam("@UserName", users.UserName));
                spParams.Add(new SpParam("@FirstName", users.FirstName));
                spParams.Add(new SpParam("@MiddleName", users.MiddleName));
                spParams.Add(new SpParam("@LastName", users.LastName));
                spParams.Add(new SpParam("@Phone", users.Phone));
                spParams.Add(new SpParam("@Email", users.Email));
                spParams.Add(new SpParam("@Password", users.UserPassword));
                spParams.Add(new SpParam("@BankId", users.BankId));
                spParams.Add(new SpParam("@Roles", users.UserRoles[0].RoleId));
                object newID = DBHelper.ExecProcScalar("sp_AddEditUsers", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in sp_AddEditUsers: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
        public string createToken(string userId, string password, string tokenId)
        {
            string retVal = "";
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();
            try
            {
                spParams.Add(new SpParam("@userId", userId));
                spParams.Add(new SpParam("@password", password));
                spParams.Add(new SpParam("@tokenId", tokenId));
                object newID = DBHelper.ExecProcScalar("spCreateToken", spParams);
                retVal = newID.ToString();
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in InsertUpdateUsers: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return retVal;
        }
    
        


      

        public int insertUpdateProduct(UserEntity users, string mode, int userid)
        {
            int retVal = -1;
            conn = new SqlConnection(Constants.connString);
            conn.Open();
            SpParamCollection spParams = new SpParamCollection();

            try {
                if (mode == Constants.UPDATEMODE) {
                    spParams.Add(new SpParam("@ProductId", users.productId));
                }
                else {
                    spParams.Add(new SpParam("@ProductId", 0));
                }

                spParams.Add(new SpParam("@ProductName", users.productName));
                spParams.Add(new SpParam("@LogginguserId", userid)); 
            
                object newID = DBHelper.ExecProcScalar("sp_AddEditProducts", spParams);
                retVal = int.Parse(newID.ToString());
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in InsertUpdateUsers: " + ex.Message + "<br/>" + ex.StackTrace);

                throw ex;
            }
            finally {
                conn.Close();
            }
            return retVal;
        }
        




        public void DeleteUsers(int UserId) {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@UserId", UserId));
                DBHelper.ExecProcNonQuery("sp_DeleteUsers", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in DeleteUsers " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }
        public void deleteProduct(int ProductId)
        {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@ProductId", ProductId));
                DBHelper.ExecProcNonQuery("sp_deleteProduct", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in DeleteUsers " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }

        public void deleteGroupProduct(int ProductId)
        {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@ProductId", ProductId));
                DBHelper.ExecProcNonQuery("sp_grpdeleteProduct", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in DeleteUsers " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }

        
        public void deleteBC(int ProductId)
        {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@BankCorrespondId", ProductId));
                DBHelper.ExecProcNonQuery("sp_deletebc", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in DeleteUsers " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }
        

        
        public void deleteCorporate(int corporateId)
        {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@corporateId", corporateId));
                DBHelper.ExecProcNonQuery("sp_DeleteCorporate", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in Delete Corporate " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }

        public void DeleteBank(int bankId)
        {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@BankId", bankId));
                DBHelper.ExecProcNonQuery("sp_DeleteBank", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in Delete Corporate " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }
        public void deleteState(int stateId)
        {
            SpParamCollection spParams = new SpParamCollection();
            try {
                spParams.Add(new SpParam("@stateId", stateId));
                DBHelper.ExecProcNonQuery("sp_DeleteState", spParams);
            }
            catch (Exception ex) {
                BCTrackingLogger.Error("Error in Delete Corporate " + ex.Message + "<br/>" + ex.StackTrace);
                throw ex;
            }
        }

        public List<State> GetAllStates()
        {
            List<State> states = null;
            State state = null;
          //  District district = null;
            try
            {
                DataSet dataset_state = new DataSet();
                SqlConnection connection = new SqlConnection(Constants.connString);
                SpParamCollection paramCollection = new SpParamCollection();
                states = new List<State>();

                DBHelper.ExecProcAndFillDataSet("sp_getState", paramCollection, dataset_state);
                if (dataset_state != null & dataset_state.Tables.Count > 0)
                {

                    //populate states                   
                    DataTable stateTable = dataset_state.Tables[0];
                    states = new List<State>();
                    for (int i = 0; i < stateTable.Rows.Count; i++)
                    {
                        state = new State();
                        DataRow stateRow = stateTable.Rows[i];
                        state.StateId = int.Parse(stateRow["StateId"].ToString());
                        state.StateName = stateRow["StateName"].ToString();
                        state.StateCode = stateRow["StateCode"].ToString();
                        states.Add(state);
                    }

                    //populate districts

                    return states;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return states;
        }
        public List<District> GetDistrictById(string stateId)
        {
            List<District> disctricts = null;

            District district = null;
            try
            {
                DataSet dataset_state = new DataSet();
                SqlConnection connection = new SqlConnection(Constants.connString);
                SpParamCollection paramCollection = new SpParamCollection();
                disctricts = new List<District>();
                paramCollection.Add(new SpParam("@stateid", int.Parse(stateId)));
                DBHelper.ExecProcAndFillDataSet("sp_getDistrict", paramCollection, dataset_state);
                if (dataset_state != null & dataset_state.Tables.Count > 0)
                {

                    //populate states                   
                    DataTable districtTable = dataset_state.Tables[0];
                    disctricts = new List<District>();
                    for (int i = 0; i < districtTable.Rows.Count; i++)
                    {
                        district = new District();

                        district = new District();
                        DataRow districtRow = districtTable.Rows[i];
                        district.DistrictId = int.Parse(districtRow["DistrictId"].ToString());
                        district.DistrictName = districtRow["DistrictName"].ToString();
                        district.StateId = int.Parse(districtRow["StateId"].ToString());
                        district.DistrictCode = districtRow["DistrictCode"].ToString();
                        disctricts.Add(district);
                    }

                    //populate districts


                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return disctricts;
        }

        public List<BankCorrespondence> GetBCListByLatLong(string latitude, string longitude)
        {
            List<BankCorrespondence> allBCs = null;
            BankCorrespondence bc = null;
            try
            {
                DataSet dsBC = new DataSet();

                SqlConnection conn = new SqlConnection(Constants.connString);


                SpParamCollection spParams = new SpParamCollection();
                spParams.Add(new SpParam("@latitude", latitude));
                spParams.Add(new SpParam("@longitude", longitude));

                DBHelper.ExecProcAndFillDataSet("sp_bcdetailspublic", spParams, dsBC, conn);

                if (dsBC != null && dsBC.Tables.Count > 0)
                {
                    //TABLE[0] - BCCORRES
                    DataTable dtBC = dsBC.Tables[0];
                    for (int cnt = 0; cnt < dtBC.Rows.Count; cnt++)
                    {
                        DataRow drBC = dtBC.Rows[cnt];

                        bc = new BankCorrespondence();

                        bc.BankCorrespondId = Utils.GetIntValue(drBC["BankCorrespondId"]);

                        bc.Name = Utils.GetStringValue(drBC["Name"]);
                        bc.PhoneNumber1 = Utils.GetStringValue(drBC["PhoneNumber"]);
                        bc.BankName = Utils.GetStringValue(drBC["bankName"]);
                        bc.State = Utils.GetStringValue(drBC["StateName"]);
                        bc.Village = Utils.GetStringValue(drBC["VillageName"]);
                        bc.District = Utils.GetStringValue(drBC["DistrictName"]);
                        bc.Subdistrict = Utils.GetStringValue(drBC["SubDistrictName"]);
                        bc.productName = Utils.GetStringValue(drBC["ProductName"]);




                        if (allBCs == null)
                            allBCs = new List<BankCorrespondence>();

                        allBCs.Add(bc);



                    }  //for loop
                    return allBCs;




                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return allBCs;
        }
        public List<SubDistrict> GetSubDistrictById(string districtId)
        {
            List<SubDistrict> subdisctricts = null;

            SubDistrict subdisctrict = null;
            try
            {
                DataSet dataset_state = new DataSet();
                SqlConnection connection = new SqlConnection(Constants.connString);
                SpParamCollection paramCollection = new SpParamCollection();
                subdisctricts = new List<SubDistrict>();
                paramCollection.Add(new SpParam("@districtId", int.Parse(districtId)));
                DBHelper.ExecProcAndFillDataSet("sp_getSubDistrict", paramCollection, dataset_state);
                if (dataset_state != null & dataset_state.Tables.Count > 0)
                {

                    //populate states                   
                    DataTable subdistrictTable = dataset_state.Tables[0];
                    subdisctricts = new List<SubDistrict>();
                    for (int i = 0; i < subdistrictTable.Rows.Count; i++)
                    {
                        subdisctrict = new SubDistrict();
                        DataRow districtRow = subdistrictTable.Rows[i];
                        subdisctrict.DistrictId = int.Parse(districtRow["DistrictId"].ToString());
                        subdisctrict.SubDistrictId = int.Parse(districtRow["SubDistrictId"].ToString());
                        subdisctrict.SubDistrictCode = districtRow["SubDistrictCode"].ToString();
                        subdisctrict.SubDistrictName = districtRow["SubDistrictName"].ToString();
                        subdisctricts.Add(subdisctrict);
                    }

                    //populate districts


                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return subdisctricts;
        }

        public List<Village> GetVillageBySubDistrictId(string SubDistrictId)
        {
            List<Village> villages = null;

            Village village = null;
            try
            {
                DataSet dataset_state = new DataSet();
                SqlConnection connection = new SqlConnection(Constants.connString);
                SpParamCollection paramCollection = new SpParamCollection();
                villages = new List<Village>();
                paramCollection.Add(new SpParam("@subDistrictId", int.Parse(SubDistrictId)));
                DBHelper.ExecProcAndFillDataSet("sp_getVillages", paramCollection, dataset_state);
                if (dataset_state != null & dataset_state.Tables.Count > 0)
                {

                    //populate states                   
                    DataTable subdistrictTable = dataset_state.Tables[0];
                    villages = new List<Village>();
                    for (int i = 0; i < subdistrictTable.Rows.Count; i++)
                    {
                        village = new Village();
                        DataRow districtRow = subdistrictTable.Rows[i];

                        village.SubDistrictId = int.Parse(districtRow["SubDistrictId"].ToString());
                        village.VillageCode = districtRow["VillageCode"].ToString();
                        village.VillageName = districtRow["VillageName"].ToString();
                        village.VillageId = int.Parse(districtRow["VillageId"].ToString());
                        villages.Add(village);
                    }

                    //populate districts


                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return villages;
        }
        
        

        
    }
}
