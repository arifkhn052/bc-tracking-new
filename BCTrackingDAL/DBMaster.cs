﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections;
using SBI.DAL;

namespace BCTrackingDAL
{
    public class DBMaster
    {
        public static DataTable ListofProposal(string UserId)
        {
            Hashtable hParam = new Hashtable();
            hParam.Add("@userId", UserId);
            return DBUtill.LoadDataTable("sp_ListOfBc", hParam);

        }
        public static DataTable getgroupProduct(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                DataTable dt = DBUtill.LoadDataTable("sp_GetGroupProductDynamic", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getState(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                DataTable dt = DBUtill.LoadDataTable("sp_getStateDyanamic", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getCorporate(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                DataTable dt = DBUtill.LoadDataTable("sp_getCorporateDynamic", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getBank(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch,string userId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                  hParam.Add("@userId", userId);
                DataTable dt = DBUtill.LoadDataTable("sp_getBankDynamic", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getNotification(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                DataTable dt = DBUtill.LoadDataTable("sp_NotificationDynamic", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getUser(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                DataTable dt = DBUtill.LoadDataTable("sp_GetUsersDynamic", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getProduct(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                DataTable dt = DBUtill.LoadDataTable("sp_GetProductDynamic", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }

        public static DataTable getBankState(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch,string BankId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@BankId", BankId);
                
                DataTable dt = DBUtill.LoadDataTable("sp_GetBankStateDynamic", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getBankCity(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string BankId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@BankId", BankId);

                DataTable dt = DBUtill.LoadDataTable("sp_GetBankCityDynamic", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getBankDistrict(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string BankId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@BankId", BankId);

                DataTable dt = DBUtill.LoadDataTable("sp_GetBankDistrictDynamic", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }




        public static DataTable getBCbyCorporate(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string userId, string corporateId, string stateid, string districtid, string subdistrictid, string villageId)
        {

            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@userId", userId);
                hParam.Add("@CorporateId", corporateId);
                hParam.Add("@districtid", districtid);
                hParam.Add("@stateid", stateid);
                hParam.Add("@subdistrictid", subdistrictid);
                hParam.Add("@villageid", villageId);
                DataTable dt = DBUtill.LoadDataTable("SpCorporateBC", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }

        public static DataTable getBcAllCorresponds(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string userId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@userId", userId);
                DataTable dt = DBUtill.LoadDataTable("sp_dynamicbccorresponds", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getProspectiveBCRegistryList(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string userId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                 hParam.Add("@userId", userId);
                 DataTable dt = DBUtill.LoadDataTable("sp_ProspectiveBCRegistry", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getFixedVsMbileBC(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string userId, string states, string district, string subdistrict, string village, int type)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@SortColumn", sortColumn);
                hParam.Add("@SortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@userId", userId);
                hParam.Add("@state", states);
                hParam.Add("@district", district);
                hParam.Add("@subdistrict", subdistrict);
             //   hParam.Add("@Village", village);
                hParam.Add("@type", type);
                DataTable dt = DBUtill.LoadDataTable("sp_mobilevsfixed", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getBCLocationWise(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string userId,string state,string district, string subdistrict, string Village)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@userId", userId);
                hParam.Add("@stateid", state);
                hParam.Add("@districtid", district);
                hParam.Add("@subdistrictid", subdistrict);
                hParam.Add("@Village", Village);

                DataTable dt = DBUtill.LoadDataTable("sp_BCLocationWise", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable searchLog(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string bankid, string sheet, string date)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@bankid", bankid);
                hParam.Add("@sheet", sheet);
                hParam.Add("@date",date);

                DataTable dt = DBUtill.LoadDataTable("sp_searchLog", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable searchState(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string stateid, string districtid,string subdistrictid)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@stateid", stateid);
                hParam.Add("@districtid", districtid);
                hParam.Add("@subdistrictid", subdistrictid);

                DataTable dt = DBUtill.LoadDataTable("sp_searchVillage", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }

        public static DataTable GetBranch(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string userId, string BankId, string DistrictId, string iType)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                 hParam.Add("@userId", userId);
                 hParam.Add("@BankIds", BankId);
                 hParam.Add("@DistrictId", DistrictId);
                 hParam.Add("@iType", iType);                
                 DataTable dt = DBUtill.LoadDataTable("SPGetBranchList", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        
        public static DataTable getBCByStatus(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string iType, string Status, string startDate, string endDate, string userId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@iType",Convert.ToInt32 (iType));
                hParam.Add("@Status", Status);
                hParam.Add("@createdate", startDate);
                hParam.Add("@endDate", endDate);
                hParam.Add("@userId", userId);
                
                DataTable dt = DBUtill.LoadDataTable("sp_BcByStatus", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getListAsOnDATE(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string startDate, string endDate, string userId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);             
                hParam.Add("@createdate", startDate);
                hParam.Add("@endDate", endDate);
                hParam.Add("@userId", userId);

                DataTable dt = DBUtill.LoadDataTable("spGetListAsOnDate", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }


        public static DataTable getBcByBrnachFilter(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string bankId, string branchId, string userId, string stateId, string cityId, string districtId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                
                hParam.Add("@bankId", bankId);
                hParam.Add("@branchId", branchId);
                hParam.Add("@userId", userId);
                hParam.Add("@stateId", stateId);
                hParam.Add("@districtId", districtId);
                hParam.Add("@cityId", cityId);
                
                DataTable dt = DBUtill.LoadDataTable("sp_byBrnachBC", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        //public static DataTable getBcByBankFilter(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string bankId, string branchId, string userId, string state, string city, string district)
        //{
        //    try
        //    {
        //        Hashtable hParam = new Hashtable();
        //        hParam.Add("@iDisplayStart", iDisplayStart);
        //        hParam.Add("@iDisplayLength", iDisplayLength);
        //        hParam.Add("@sortColumn", sortColumn);
        //        hParam.Add("@sortDirection", sortDirection);
        //        hParam.Add("@sSearch", sSearch);
             
        //        hParam.Add("@bankId", bankId);
        //        hParam.Add("@branchId", branchId);
        //        hParam.Add("@userId", userId);
        //        hParam.Add("@stateId", state);
        //        hParam.Add("@cityId", city);
        //        hParam.Add("@districtId", district);

        //        DataTable dt = DBUtill.LoadDataTable("sp_byBankBC", hParam);
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new DataTable();
        //    }
        //}

        public static DataTable getBlackListedBc(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string bankid, string userId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);

                hParam.Add("@bankid", bankid);
               
                hParam.Add("@userId", userId);
                DataTable dt = DBUtill.LoadDataTable("sp_blacklistedbc", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getbcProduct(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string productId, string userid)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@productId", productId);
                hParam.Add("@userId", userid);                
                DataTable dt = DBUtill.LoadDataTable("sp_getBcProduct", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }

        public static DataTable getallocationbased(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string StartDate, string EndDate, string UserId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@createdate", StartDate);
                hParam.Add("@EndDate", EndDate);
                hParam.Add("@userId", UserId);
                
                DataTable dt = DBUtill.LoadDataTable("sp_getallocationbasedlist", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getBranchLastupdated(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, int iType, int bankId, string createdate)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@iType", iType);
                hParam.Add("@bankId", bankId);
                hParam.Add("@createdate", createdate);
                DataTable dt = DBUtill.LoadDataTable("sp_getBrnachs", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
        public static DataTable getAreaWiseAllocaiton(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, int iType, int bankId, string createdate)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@iTYPE", iType);
                //hParam.Add("@bankid", bankId);
                hParam.Add("@createdate", createdate);
                DataTable dt = DBUtill.LoadDataTable("sp_getAreaWiseAllocation", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }


        public static DataTable getUnallocated(int iDisplayStart, int iDisplayLength, int sortColumn, string sortDirection, string sSearch, string userId)
        {
            try
            {
                Hashtable hParam = new Hashtable();
                hParam.Add("@iDisplayStart", iDisplayStart);
                hParam.Add("@iDisplayLength", iDisplayLength);
                hParam.Add("@sortColumn", sortColumn);
                hParam.Add("@sortDirection", sortDirection);
                hParam.Add("@sSearch", sSearch);
                hParam.Add("@userId", userId);
                DataTable dt = DBUtill.LoadDataTable("sp_dynamicbccorrespondsUnallocated", hParam);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
        }
          
    }
}
