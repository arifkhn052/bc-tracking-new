﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SBI.DAL
{
    public class DBUtill
    {
        #region Exposed Methods
        public static int ExecuteProc(string sqlProc, Hashtable parameters)
        {
            string[] paramNames = new string[parameters.Keys.Count];
            parameters.Keys.CopyTo(paramNames, 0);
            object[] paramValues = new object[parameters.Values.Count];
            parameters.Values.CopyTo(paramValues, 0);

            SqlConnection sConn = CreateDBConn();
            SqlCommand sCom = new SqlCommand();
            sCom.Connection = sConn;
            SqlParameter[] spram = CreateParameterList(paramNames, paramValues, paramNames.Count());
            foreach (SqlParameter sp in spram)
            {
                sCom.Parameters.Add(sp);
            }
            sCom.CommandText = sqlProc;
            sCom.CommandType = CommandType.StoredProcedure;

            sCom.CommandTimeout = 0;

            sConn.Open();
            int res = sCom.ExecuteNonQuery();
            sConn.Close();
            return res;
        }

        public static string ExecuteProcedure(string sqlProc, Hashtable parameters)
        {
            string res;
            try
            {
                string[] paramNames = new string[parameters.Keys.Count];
                parameters.Keys.CopyTo(paramNames, 0);
                object[] paramValues = new object[parameters.Values.Count];
                parameters.Values.CopyTo(paramValues, 0);

                SqlConnection sConn = CreateDBConn();
                SqlCommand sCom = new SqlCommand();
                sCom.Connection = sConn;
                SqlParameter[] spram = CreateParameterList(paramNames, paramValues, paramNames.Count());
                foreach (SqlParameter sp in spram)
                {
                    sCom.Parameters.Add(sp);
                }
                sCom.CommandText = sqlProc;
                sCom.CommandType = CommandType.StoredProcedure;

                sCom.CommandTimeout = 0;

                sConn.Open();
                 res = sCom.ExecuteScalar().ToString();
                sConn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }

        public static DataTable LoadDataTable(string sqlProc, Hashtable parameters)
        {
            DataTable dtRes = new DataTable();
            string[] paramNames = new string[parameters.Keys.Count];
            parameters.Keys.CopyTo(paramNames, 0);
            object[] paramValues = new object[parameters.Values.Count];
            parameters.Values.CopyTo(paramValues, 0);

            SqlConnection sConn = CreateDBConn();
            SqlCommand sCom = new SqlCommand();
            sCom.Connection = sConn;
            SqlParameter[] spram = CreateParameterList(paramNames, paramValues, paramNames.Count());
            foreach (SqlParameter sp in spram)
            {
                sCom.Parameters.Add(sp);
            }
            sCom.CommandText = sqlProc;
            sCom.CommandType = CommandType.StoredProcedure;

            sCom.CommandTimeout = 0;

            sConn.Open();
            SqlDataReader res = sCom.ExecuteReader();
            dtRes.Load(res);
            sConn.Close();

            return dtRes;
        }

        public static DataTable LoadDataTableFromQuery(string strQuery)
        {
            DataTable dtRes = new DataTable();
            SqlConnection sConn = CreateDBConn();
            SqlCommand cmd = new SqlCommand(strQuery, sConn);
            sConn.Open();

            // Create Data Adapter
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(dtRes);
            sConn.Close();
            da.Dispose();
            return dtRes;
        }

        public static DataSet LoadDataset(string sqlProc, Hashtable parameters)
        {
            DataSet dsRes = new DataSet();
            string[] paramNames = new string[parameters.Keys.Count];
            parameters.Keys.CopyTo(paramNames, 0);
            object[] paramValues = new object[parameters.Values.Count];
            parameters.Values.CopyTo(paramValues, 0);

            SqlConnection sConn = CreateDBConn();
            SqlCommand sCom = new SqlCommand();
            sCom.Connection = sConn;
            SqlParameter[] spram = CreateParameterList(paramNames, paramValues, paramNames.Count());
            foreach (SqlParameter sp in spram)
            {
                sCom.Parameters.Add(sp);
            }
            sCom.CommandText = sqlProc;
            sCom.CommandType = CommandType.StoredProcedure;

            sConn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(sCom);
            adapter.Fill(dsRes);
            sConn.Close();

            return dsRes;
        }

        public static void SqlBulkCopyOperation(DataTable dtNew, string destTableName, List<SqlBulkCopyColumnMapping> mapping = null)
        {
            SqlConnection sConn = CreateDBConn();
            SqlBulkCopy bulkCopy = new SqlBulkCopy(sConn);
            bulkCopy.DestinationTableName = destTableName;
            if (mapping != null)
            {
                foreach (SqlBulkCopyColumnMapping map in mapping)
                {
                    bulkCopy.ColumnMappings.Add(map);
                }
            }
            // Write from the source to the destination.
            sConn.Open();
            bulkCopy.WriteToServer(dtNew);
            sConn.Close();
        }
        #endregion

        #region Methods
        private static SqlConnection CreateDBConn()
        {
            SqlConnection sConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
            return sConn;
        }
      

        private static SqlParameter[] CreateParameterList(string[] paramNames, object[] paramValues, int arrLength)
        {
            SqlParameter[] parameterList = new SqlParameter[arrLength];
            for (int i = 0; i < paramNames.Length; i++)
            {
                if (paramValues[i] == null)
                {
                    paramValues[i] = DBNull.Value;
                }

                parameterList[i] = new SqlParameter(paramNames[i], paramValues[i]);
            }
            return parameterList;
        }
        #endregion

    }
}
