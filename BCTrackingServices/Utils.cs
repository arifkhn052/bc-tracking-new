﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BCTrackingServices

{
    public class Utils
    {
        public static int GetIntValue(object valueFromDB)
        {
            int retVal = Constants.DEFAULTVALUE;
            if (!CheckNull(valueFromDB))
            {
                if (!Int32.TryParse(valueFromDB.ToString(), out retVal))
                    retVal = Constants.DEFAULTVALUE;
            }
            return retVal;
        }

        public static double GetDoubleValue(object valueFromDB)
        {
            double retVal = 0;
            if (!CheckNull(valueFromDB))
            {
                if (!Double.TryParse(valueFromDB.ToString(), out retVal))
                    retVal = 0;
            }
            return retVal;
        }

        public static bool GetBoolValue(object valueFromDB)
        {
            bool retVal = false;
            if (!CheckNull(valueFromDB))
            {
                Boolean.TryParse(valueFromDB.ToString(), out retVal);
            }

            return retVal;
        }

        public static string GetStringValue(object valueFromDB)
        {
            string retVal = String.Empty;
            if (!CheckNull(valueFromDB))
            {

                retVal = valueFromDB.ToString().Trim().Replace("\"", "");
            }

            return retVal;
        }

        public static DateTime GetDateTimeValue(object valueFromDB)
        {
            DateTime retVal = new DateTime(1);
            if (!CheckNull(valueFromDB))
            {
                //string[] formats = { "MM/dd/yyyy", "MM-dd-yyyy", "dd-MM-yyyy", "dd-MM-yyyy HH:mm:ss", "dd/MM/yyyy HH:mm:ss", "MM/dd/yyyy HH:mm:ss" };
                //string[] formats = { "dd/MM/yyyy", "dd-MM-yyyy", "dd/MM/yyyy HH:mm:ss", "MM/dd/yyyy HH:mm:ss" };
                string[] formats = { "dd/MM/yyyy", "dd-MM-yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };


                try
                {
                    //if ((String)valueFromDB == String.Empty)
                    //    retVal = DateTime.Now;
                    //else
                         DateTime.TryParse(valueFromDB.ToString(), out retVal);

                }
                catch (Exception ex)
                {

                    //DateTime.TryParse(valueFromDB.ToString(), out retVal);
                    throw ex;
                }
                //DateTime.TryParse(valueFromDB.ToString(), out retVal);
            }

            return retVal;
        }

        public static bool CheckNull(object valueFromDB)
        {
            if (valueFromDB != null && valueFromDB != System.DBNull.Value)
                return false;
            else
                return true;
        }

        public static object GetSessionValue(string objectType)
        {
            object obj = null;

            if (HttpContext.Current.Session[objectType] != null)
                obj = HttpContext.Current.Session[objectType];

            return obj;

        }

        public static void SetSessionValue(string objectType, object valToSet)
        {

            HttpContext.Current.Session[objectType] = valToSet;
        }    
    }
}
