﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCTrackingServices
{
    public class Constants
    {

        public static string connString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        public const string UPDATEMODE = "Update";
        public const string ADDMODE = "Add";
        public const string userId = "Add";
     
        public const int DEFAULTVALUE = -1;
        public const string DEFAULTVALUEs = "";
        public const string DEFAULTCHOICE = "--Select--";

        public const string BANKMODE = "Bank";
        public const string USERMODE = "User";
        public const string BRANCHMODE = "Branchs";
        public const string userMenu = "userMenu";
        
        public const string MODE = "mode";
        public const string CORPORATEMODE = "Corporate";
        public const string NOTIFICATIONMODE = "Notification";

        public const string BLACKLISTMODE = "BlackList";
        public const string WHITELISTMODE = "WhiteList";
        public const string CHANGESTATUSMODE = "ChangeStatus";
        public const string STATESMODE = "state";

        public const string ALLOCATED = "Allocated";
        public const string UNALLOCATED = "Unallocated";

        public const string APPTDATEFROMFILTER = "AppointmentDateFrom";
        public const string APPTDATETOFILTER = "AppointmentDateTo";
        public const string CREATEDATEFROMFILTER = "CreateDateFrom";
        public const string CREATEDATETOFILTER = "CreateDateTo";
        public const string CREATEADHAARFILTER = "Createdatefilter";
        //<<<<<<< .mine
        public const string BANKID = "bankId";
        public const string BANKCIRCLEID = "bankCircleId";
        public const string BANKREGIONID = "bankRegionId";
        public const string BANKZONEID = "bankZoneId";
        public const string BRANCHCATEGORY = "branchCategory";
        public const string BRANCHID = "branchId";
        public const string BANKSSA = "bankSsa";
        public const string STATEID = "stateId";
//||||||| .r560
//=======
            
        public const string BCSESSIONVAR = "BCSessionVar";
        public const string BANKSESSIONVAR = "BankSessionVar";
        public const string STATESESSIONVAR = "StateSessionVar";
        public const string USERSESSIONID = "UserSessionId";
        public const string PINCODE = "pinCode";

        public const string ISBLACKLISTED = "IsBlackListed";
        public const string PRODUCTMODE = "product";
//>>>>>>> .r567

    }
}
