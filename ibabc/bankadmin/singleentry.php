<?php 
$breadcrumbs = [
	"Add Bank Correspondents" => "addbc.php",
	"Single Entry" => "singleentry.php"
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.min.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>
        
        <div class="example">
		    <form class="form-horizontal">
		        <br>
		        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Personal Details</h4> </div>
		        <hr>
		        <div class="form-group">
		            <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
		            <div class="col-sm-10">
		                <input type="text" class="form-control" id="inputEmail3" placeholder="Name">
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputPassword3" class="col-sm-2 control-label">Phone Number</label>
		            <div class="col-sm-10">
		                <input type="text" class="form-control" id="inputPassword3" placeholder="Phone Number">
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail4" class="col-sm-2 control-label">Email</label>
		            <div class="col-sm-10">
		                <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail7" class="col-sm-2 control-label">Date Of Birth</label>
		            <div class="col-sm-10">
		                <input type="date" class="form-control" id="inputEmail7" placeholder="Date of Birth">
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail5" class="col-sm-2 control-label">Aadhar Card</label>
		            <div class="col-sm-10">
		                <input type="text" class="form-control" id="inputEmail5" placeholder="Aadhar Card Number">
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Pan Card</label>
		            <div class="col-sm-10">
		                <input type="text" class="form-control" id="inputEmail6" placeholder="Pan Card Number">
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Address</label>
		            <div class="col-sm-10">
		                <textarea class="form-control" id="inputEmail6" placeholder="Enter Address"></textarea>
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Pin Code</label>
		            <div class="col-sm-10">
		                <input type="text" class="form-control" id="inputEmail6" placeholder="Pin Code">
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Alternate Occupation</label>
		            <div class="col-sm-10">
		                <textarea class="form-control" id="inputEmail6" placeholder="Alternate Occupation Detail"></textarea>
		            </div>
		        </div>
		        <br>
		        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Associated Bank Detail</h4> </div>
		        <hr>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">IFSC Code</label>
		            <div class="col-sm-10">
		                <input type="text" class="form-control" id="ifscCode" placeholder="IFSC Code">
		            </div>
		        </div>
		        <div class="form-group">
		            <span class="col-sm-12 text-center">--or--</label>
		        </div>
		        <div class="form-group">
		            <label for="bankName" class="col-sm-2 control-label">Bank Name</label>
		            <div class="col-sm-10">
		                <select class="form-control" id="bankName" disabled>
		                	<option value="0">Select</option>
		                    <option value="1">State Bank of India</option>
		                    <option value="12">Bank of Baroda</option>
		                    <option value="13">Axis Bank</option>
		                    <option value="14">Union Bank of India</option>
		                </select>
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Circle</label>
		            <div class="col-sm-10">
		                <select class="form-control" id="bankCircle">
		                	<option value="0">Select</option>
		                    <option class="dissolv" value="1">Navi Mumbai</option>
		                    <option class="dissolv" value="6">Thane</option>
		                    <option class="dissolv" value="9">Mumbai</option>
		                    <option class="dissolv" value="13">Pune</option>
		                    <option class="dissolv" value="16">Nanded</option>
		                    <option class="dissolv" value="18">Nashik</option>
		                    <option class="dissolv" value="21">Sindhudurg</option>
		                    <option class="dissolv" value="23">UDUPI</option>
		                    <option class="dissolv" value="26">Raigad</option>
		                    <option class="dissolv" value="28">Ahmednagar</option>
		                    <option class="dissolv" value="30">Aurangabad</option>
		                    <option class="dissolv" value="33">Ahmedabad</option>
		                    <option class="dissolv" value="3242">Varanasi</option>
		                </select>
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">State</label>
		            <div class="col-sm-10">
		                <select class="form-control"  id="bankState">
		                	<option value="0">Select</option>
		                    <option class="dissolv" value="2">Maharashtra</option>
		                </select>
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Zone</label>
		            <div class="col-sm-10">
		                <select class="form-control" id="bankZone">
		                	<option value="0">Select</option>
		                    <option class="dissolv" value="3">NMZ</option>
		                </select>
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Region</label>
		            <div class="col-sm-10">
		                <select class="form-control" id="bankRegion">
		                	<option value="0">Select</option>
		                    <option class="dissolv" value="4">Navi Mumbai</option>
		                </select>
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Category</label>
		            <div class="col-sm-10">
		                <select class="form-control" id="bankCategory">
		                	<option value="0">Select</option>
		                    <option class="dissolv" value="5">Urban</option>
		                    <option class="dissolv" value="32">Semi Urban</option>
		                </select>
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Branch</label>
		            <div class="col-sm-10">
		                <select class="form-control" id="bankBranch">
		                	<option value="0">Select</option>
		                    <option class="dissolv" value="5">Branch1</option>
		                    <option class="dissolv" value="32">branch2</option>
		                </select>
		            </div>
		        </div>
		        <br>
		        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Allotted Area details</h4> </div>
		        <hr>
		        <div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
				            <label for="inputEmail6" class="col-sm-2 control-label">Village Code</label>
				            <div class="col-sm-10">
		                		<input type="text" class="form-control" id="villageCode" placeholder="Enter Village Code">
				            </div>
				        </div>
				        <div class="form-group">
				            <label for="inputEmail6" class="col-sm-2 control-label">Village Detail</label>
				            <div class="col-sm-10">
				            	<textarea name="villageDetail" id="villageDetail" class="form-control" rows="4" disabled placeholder="Village Detail"></textarea>
				            </div>
				        </div>
				        <div class="form-group">
				            <div class="col-sm-3 col-sm-offset-9">
		                		<input type="button" class="pull-right btn btn-danger" id="addMoreButton" value="Add More">
				            </div>
				        </div>
					</div>
		        </div>

		        <div class="form-group">
		            <table id="example" class="display" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th>Village Code</th>
				                <th>Village Detail</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				    </table>
		        </div>

		        <br>
		        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Certification Details</h4> </div>
		        <hr>
		        <div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
				            <label for="inputEmail6" class="col-sm-2 control-label">Certification Date</label>
				            <div class="col-sm-10">
		                		<input type="date" class="form-control" id="certificateDate" placeholder="Enter Certification Date">
				            </div>
				        </div>
				        <div class="form-group">
				            <label for="inputEmail6" class="col-sm-2 control-label">Certification Authority</label>
				            <div class="col-sm-10">
				            	<select class="form-control" id="certificateAuth">
				                    <option class="dissolv" value="Authority 1">Authority 1</option>
				                    <option class="dissolv" value="Authority 2">Authority 2</option>
				                    <option class="dissolv" value="Authority 3">Authority 3</option>
				                </select>
				            </div>
				        </div>
				        <div class="form-group">
				            <div class="col-sm-3 col-sm-offset-9">
		                		<input type="button" class="pull-right btn btn-danger" id="addMoreButton2" value="Add More">
				            </div>
				        </div>
				    </div>
				</div>
				<div class="form-group">
		            <table id="example2" class="display" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th>Certification Date</th>
				                <th>Certification Authority</th>
				                <th>Action</th>
				            </tr>
				        </thead>
				    </table>
		        </div>
				<hr>
		        	
		        <div class="form-group center-block">
		            <div class="center-block">
		                <button type="submit" class="btn btn-default center-block">Submit</button>
		            </div>
		        </div>
		    </form>
		</div>


        <br>

    </div>
</body>
<script src="js/jquery-1.12.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/metro.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
	    var t = $('#example').DataTable({
	    	"paging":   false,
	        "ordering": false,
	        "info":     false,
	        "searching": false
	    });

	    var t2 = $('#example2').DataTable({
	    	"paging":   false,
	        "ordering": false,
	        "info":     false,
	        "searching": false
	    });
	 	
	 	$("#villageCode").on('blur', function() {
 			$("#villageDetail").text('village 1, district 1, state');
	 	}).on('focus', function() {
 			$("#villageDetail").text('');
	 	});

		$('#addMoreButton2').on( 'click', function () {
	        t2.row.add( [
	            $("#certificateDate").val(),
	            $("#certificateAuth").val(),
	            'delete'
	        ] ).draw( false );
	        $("#certificateDate").val("");
	        
	    } );
	    $('#addMoreButton').on( 'click', function () {
	        t.row.add( [
	            $("#villageCode").val(),
	            $("#villageDetail").text(),
	            'delete'
	        ] ).draw( false );
	        $("#villageCode").val("");
	        $("#villageDetail").text("");
	    } );

	    $("#ifscCode").on('blur', function() {
	    	$("#bankName").val("1");
	    	$("#bankCircle").val("1");
	    	$("#bankState").val("2");
	    	$("#bankZone").val("3");
	    	$("#bankRegion").val("4");
	    	$("#bankCategory").val("5");
	    	$("#bankBranch").val("5");
	    }).on('focus', function() {
	    	$("#bankName").val("0");
	    	$("#bankCircle").val("0");
	    	$("#bankState").val("0");
	    	$("#bankZone").val("0");
	    	$("#bankRegion").val("0");
	    	$("#bankCategory").val("0");
	    	$("#bankBranch").val("0");
	    });
 	} );
</script>

</html>
