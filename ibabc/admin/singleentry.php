<?php 
$breadcrumbs = [
	"Add Bank Correspondents" => "addbc.php",
	"Single Entry" => "singleentry.php"
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
    <link href="../assets/css/jquery.dataTables.min.css" rel="stylesheet">
	<script src="../assets/js/jquery-1.12.2.min.js"></script>

</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>
        
        <div class="example">
		    <form class="form-horizontal">
		        <br>
		        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Personal Details</h4> </div>
		        <hr>
		        <?php include("../includes/emptyForm/personal.php") ?>
		        <br>
		        <!-- <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Certification Details</h4> </div>
		        <hr> -->
		        <?php include("../includes/emptyForm/certification.php") ?>
		        
				

		        <br>
		        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Corporate Association</h4> </div>
		        <hr>
		        <?php include("../includes/emptyForm/association.php") ?>
		        
		        <br>

		        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Bank Correspondent Allocation</h4> </div>
		        <hr>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Allocated</label>
		            <div class="col-sm-10">
		                <label class="radio-inline">
						  <input type="radio" name="allocated" id="inlineRadio1" value="yes"> Yes
						</label>
						<label class="radio-inline">
						  <input type="radio" name="allocated" id="inlineRadio2" value="no"> No
						</label>
		            </div>
		        </div>
		        <div id="bcallocate" class="hidden">
			        <?php include("../includes/emptyForm/allocation.php") ?>
			        <br>
		        	<?php include("../includes/emptyForm/device.php") ?>
		        	<br>
		        	<?php include("../includes/emptyForm/services.php") ?>
		        </div>
		        
		        <div id="bcsponsor" class="hidden">
		        	<br>
		        	<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
			            <h4>Bank Correspondent Sponsored By</h4> </div>
			        <hr>
		        	<div class="form-group">
			            <label for="bankName" class="col-sm-2 control-label">Sponsored By</label>
			            <div class="col-sm-10">
		                	<input type="text" class="form-control" id="inputPassword3" placeholder="Sponsored By">
			            </div>
			        </div>
			        <br>
		        </div>
		        
				<hr>
		        <div class="form-group center-block">
		            <div class="center-block">
		                <button type="submit" class="btn btn-default center-block">Submit</button>
		            </div>
		        </div>
		    </form>
		</div>


        <br>

    </div>
</body>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {


	    $('input[type=radio][name=allocated]').change(function() {
	        if (this.value == 'yes') {
	            $("#bcallocate").removeClass('hidden');
	            $("#productOffered").removeClass('hidden');

	            $("#bcsponsor").addClass('hidden');
	        }
	        else if (this.value == 'no') {
	            $("#bcsponsor").removeClass('hidden');
	            $("#bcallocate").addClass('hidden');
	            $("#productOffered").addClass('hidden');
	        }
	    });
 	} );
</script>

</html>
