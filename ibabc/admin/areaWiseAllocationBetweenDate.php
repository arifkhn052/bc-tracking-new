<?php 
$breadcrumbs = [
	"Reports" => "reports.php",
	"Area Wise BC between Dates" => "areaWiseAllocationBetweenDate.php"
];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
    <link href="../assets/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>

        <div class="example">

			<form class="form-inline">
			  <div class="form-group">
			    <label for="exampleInputName2">Start Date</label>
			    <input type="date" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail2">End Date</label>
			    <input type="date" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com">
			  </div>
			  <div class="form-group">
                <label for="inputEmail6">Area Id</label>
                    <select class="form-control">
                        <option class="dissolv" value="1">Area 1</option>
                        <option class="dissolv" value="6">Area 2</option>
                        <option class="dissolv" value="9">Area 3</option>
                    </select>
            </div>
			  <button type="submit" class="btn btn-default">Generate Data</button>
			</form>
			<hr>
			<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

		</div>
        
        


        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>
	$(function () {
	    $('#container').highcharts({
	        chart: {
	            type: 'line'
	        },
	        title: {
	            text: 'BC Allotment in the Area 1'
	        },
	        xAxis: {
	            type: 'datetime',
	            dateTimeLabelFormats: { // don't display the dummy year
	                month: '%e. %b',
	                year: '%b'
	            },
	            title: {
	                text: 'Date'
	            }
	        },
	        yAxis: {
	            title: {
	                text: 'BC Count'
	            },
	            min: 0
	        },
	        tooltip: {
	            headerFormat: '<b>{series.name}</b><br>',
	            pointFormat: '{point.x:%e. %b}: {point.y:.0f} BC'
	        },

	        plotOptions: {
	            spline: {
	                marker: {
	                    enabled: true
	                }
	            }
	        },

	        series: [{
	            name: 'BC allocations 2012-2013',
	            // Define the data points. All series have a dummy year
	            // of 1970/71 in order to be compared on the same x axis. Note
	            // that in JavaScript, months start at 0 for January, 1 for February etc.
	            data: [
	                [Date.UTC(1970, 9, 21), 0],
	                [Date.UTC(1970, 11, 26), 2],
	                [Date.UTC(1970, 11, 29), 1],
	                [Date.UTC(1971, 0, 11), 0],
	                [Date.UTC(1971, 0, 26), 1],
	                [Date.UTC(1971, 1, 3), 2],
	                [Date.UTC(1971, 1, 11), 0],
	                [Date.UTC(1971, 4, 5), 1],
	                [Date.UTC(1971, 4, 19), 2],
	                [Date.UTC(1971, 5, 3), 1]
	            ]
	        }]
	    });
	});
</script>

</html>

