<?php 
$breadcrumbs = [
	"Notifications" => "notifications.php"
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
    <link href="../assets/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>
        
        <table id="example" class="display" cellspacing="0" width="100%">
	        <thead>
	            <tr>
                    <th>ID</th>
                    <th>Notification Type</th>
                    <th>Notification Text</th>
                    <th>Notification Date</th>
                    <th></th>
                    <th>Notification Priority</th>
	            </tr>
	        </thead>
            <tbody>
                <tr>
                    <td>N21231</td>
                    <td>New BC Allocated To SSA</td>
                    <td>BC: P12321 allocated to SSA: A1231</td>
                    <td>2012-2-21<td>
                    <td>Info</td>
                </tr>
                <tr>
                    <td>N212313</td>
                    <td>BC Blacklisted</td>
                    <td>BC: P12321 Blacklisted</td>
                    <td>2012-2-25<td>
                    <td>High</td>
                </tr>
            </tbody>
	    </table>
		        


        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
	    var t = $('#example').DataTable({});


 	} );
</script>

</html>
