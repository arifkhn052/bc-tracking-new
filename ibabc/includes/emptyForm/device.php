<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
    <h4>Device Details</h4> 
</div>
<hr>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Device Given On</label>
            <div class="col-sm-10">
        		<input type="date" class="form-control" id="deviceDate" placeholder="Enter Certification Date">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Device Name</label>
            <div class="col-sm-10">
            	<select class="form-control" id="deviceName">
                    <option class="dissolv" value="POS">POS</option>
                    <option class="dissolv" value="Micro ATM">Micro ATM</option>
                    <option class="dissolv" value="USSD Phones">USSD Phones</option>
                    <option class="dissolv" value="KIOSK">KIOSK</option>
                    <option class="dissolv" value="VSAT">VSAT</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-9">
        		<input type="button" class="pull-right btn btn-danger" id="addMoreButton20" value="Add More">
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <table id="example20" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Device Date</th>
                <th>Device Name</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<br>

<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
    <h4>Connectivity Detail</h4> </div>
<hr>
<div class="form-group">
    <label for="bankName" class="col-sm-2 control-label">Connectivity Provider</label>
    <div class="col-sm-10">
        <select class="form-control" id="">
        	<option value="0">Select</option>
            <option value="1">Landline</option>
            <option value="12">Mobile</option>
            <option value="13">USAT</option>
        </select>
    </div>
</div>

<script>
	$(function() {
		var t20 = $('#example20').DataTable({
	    	"paging":   false,
	        "ordering": false,
	        "info":     false,
	        "searching": false
	    });
	    $('#addMoreButton20').on( 'click', function () {
	        t20.row.add( [
	            $("#deviceDate").val(),
	            $("#deviceName").val(),
	            'delete'
	        ] ).draw( false );
	        $("#deviceDate").val("");
	        $("#deviceName").val("0");
	        
	    } );
	});
</script>