<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail3" placeholder="Name">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail7" class="col-sm-2 control-label">Upload image</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" id="inputEmail7" placeholder="Upload Image">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail5" class="col-sm-2 control-label">Gender</label>
    <div class="col-sm-10">
        <select class="form-control" id="">
            <option value="0">Select</option>
            <option value="1">Male</option>
            <option value="12">Female</option>
            <option value="13">Trans-Gender</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail7" class="col-sm-2 control-label">Date Of Birth</label>
    <div class="col-sm-10">
        <input type="date" class="form-control" id="inputEmail7" placeholder="Date of Birth">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail5" class="col-sm-2 control-label">Father Name</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail5" placeholder="Father Name">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail5" class="col-sm-2 control-label">Spouse Name</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail5" placeholder="Spouse Name">
    </div>
</div>

<div class="form-group">
    <label for="inputEmail5" class="col-sm-2 control-label">Category</label>
    <div class="col-sm-10">
        <select class="form-control" id="">
            <option value="0">Select</option>
            <option value="1">General</option>
            <option value="12">OBC</option>
            <option value="13">SC</option>
            <option value="13">ST</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail5" class="col-sm-2 control-label">Physically Handicapped</label>
    <div class="col-sm-10">
        <label class="radio-inline">
          <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Yes
        </label>
        <label class="radio-inline">
          <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> No
        </label>
    </div>
</div>
<div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Phone Number</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputPassword3" placeholder="Phone Number">
    </div>
    <div class="col-sm-10 col-sm-offset-2" style="padding-top: 5px;">
        <input type="text" class="form-control" id="inputPassword3" placeholder="Phone Number 2">
    </div>
    <div class="col-sm-10 col-sm-offset-2" style="padding-top: 5px;">
        <input type="text" class="form-control" id="inputPassword3" placeholder="Phone Number 3">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail4" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
        <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail5" class="col-sm-2 control-label">Aadhar Card</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail5" placeholder="Aadhar Card Number">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Pan Card</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail6" placeholder="Pan Card Number">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Voters Id Card</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail6" placeholder="Voters Id Number">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Drivers Licence</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail6" placeholder="Drivers Licence Number">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">NREGA Card</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail6" placeholder="NREGA Card Number">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Ration Card</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail6" placeholder="Ration Card Number">
    </div>
</div>

<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Address State</label>
    <div class="col-sm-10">
        <select class="form-control" id="">
        	<option value="0">Select</option>
            <option class="dissolv" value="1">Navi Mumbai</option>
            <option class="dissolv" value="6">Thane</option>
            <option class="dissolv" value="9">Mumbai</option>
            <option class="dissolv" value="13">Pune</option>
            <option class="dissolv" value="16">Nanded</option>
            <option class="dissolv" value="18">Nashik</option>
            <option class="dissolv" value="21">Sindhudurg</option>
            <option class="dissolv" value="23">UDUPI</option>
            <option class="dissolv" value="26">Raigad</option>
            <option class="dissolv" value="28">Ahmednagar</option>
            <option class="dissolv" value="30">Aurangabad</option>
            <option class="dissolv" value="33">Ahmedabad</option>
            <option class="dissolv" value="3242">Varanasi</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Address City</label>
    <div class="col-sm-10">
        <select class="form-control" id="">
        	<option value="0">Select</option>
            <option class="dissolv" value="1">Navi Mumbai</option>
            <option class="dissolv" value="6">Thane</option>
            <option class="dissolv" value="9">Mumbai</option>
            <option class="dissolv" value="13">Pune</option>
            <option class="dissolv" value="16">Nanded</option>
            <option class="dissolv" value="18">Nashik</option>
            <option class="dissolv" value="21">Sindhudurg</option>
            <option class="dissolv" value="23">UDUPI</option>
            <option class="dissolv" value="26">Raigad</option>
            <option class="dissolv" value="28">Ahmednagar</option>
            <option class="dissolv" value="30">Aurangabad</option>
            <option class="dissolv" value="33">Ahmedabad</option>
            <option class="dissolv" value="3242">Varanasi</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Address District</label>
    <div class="col-sm-10">
        <select class="form-control" id="">
        	<option value="0">Select</option>
            <option class="dissolv" value="1">Navi Mumbai</option>
            <option class="dissolv" value="6">Thane</option>
            <option class="dissolv" value="9">Mumbai</option>
            <option class="dissolv" value="13">Pune</option>
            <option class="dissolv" value="16">Nanded</option>
            <option class="dissolv" value="18">Nashik</option>
            <option class="dissolv" value="21">Sindhudurg</option>
            <option class="dissolv" value="23">UDUPI</option>
            <option class="dissolv" value="26">Raigad</option>
            <option class="dissolv" value="28">Ahmednagar</option>
            <option class="dissolv" value="30">Aurangabad</option>
            <option class="dissolv" value="33">Ahmedabad</option>
            <option class="dissolv" value="3242">Varanasi</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Address Sub District</label>
    <div class="col-sm-10">
        <select class="form-control" id="">
        	<option value="0">Select</option>
            <option class="dissolv" value="1">Navi Mumbai</option>
            <option class="dissolv" value="6">Thane</option>
            <option class="dissolv" value="9">Mumbai</option>
            <option class="dissolv" value="13">Pune</option>
            <option class="dissolv" value="16">Nanded</option>
            <option class="dissolv" value="18">Nashik</option>
            <option class="dissolv" value="21">Sindhudurg</option>
            <option class="dissolv" value="23">UDUPI</option>
            <option class="dissolv" value="26">Raigad</option>
            <option class="dissolv" value="28">Ahmednagar</option>
            <option class="dissolv" value="30">Aurangabad</option>
            <option class="dissolv" value="33">Ahmedabad</option>
            <option class="dissolv" value="3242">Varanasi</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Address Area</label>
    <div class="col-sm-10">
        <textarea class="form-control" id="inputEmail6" placeholder="Enter Address Area"></textarea>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Pin Code</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail6" placeholder="Pin Code">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Alternate Occupation Type</label>
    <div class="col-sm-10">
        <select class="form-control" id="">
            <option value="0">Select</option>
            <option class="dissolv" value="1">Government</option>
            <option class="dissolv" value="6">Public Sector</option>
            <option class="dissolv" value="9">Private</option>
            <option class="dissolv" value="13">Self Employed</option>
            <option class="dissolv" value="16">Any Other</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Alternate Occupation Detail</label>
    <div class="col-sm-10">
        <textarea class="form-control" id="inputEmail6" placeholder="Alternate Occupation Detail"></textarea>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Unique Identification Number</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail6" placeholder="Unique Identification Number">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Bank Reference Number</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail6" placeholder="Bank Reference Number">
    </div>
</div>