<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
    <h4>BC Certification</h4> 
</div>
<hr>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Date of Passing</label>
            <div class="col-sm-10">
        		<input type="date" class="form-control" id="certificateDate" placeholder="Enter Certification Date">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Institute Name</label>
            <div class="col-sm-10">
            	<select class="form-control" id="certificateAuth">
                    <option class="dissolv" value="Authority 1">Authority 1</option>
                    <option class="dissolv" value="Authority 2">Authority 2</option>
                    <option class="dissolv" value="Authority 3">Authority 3</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Course</label>
            <div class="col-sm-10">
                <select class="form-control" id="certificationCourse">
                    <option class="dissolv" value="course 1">course 1</option>
                    <option class="dissolv" value="course 2">course 2</option>
                    <option class="dissolv" value="course 3">course 3</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Grades</label>
            <div class="col-sm-10">
                <input type="number" name="grade" id="certificationGrade" class="form-control" placeholder="Grade">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-9">
        		<input type="button" class="pull-right btn btn-danger" id="addMoreButton2" value="Add More">
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <table id="example2" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Certification Date</th>
                <th>Institute</th>
                <th>Course</th>
                <th>Grades</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<br>
<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
    <h4>Educational Qualification</h4> 
</div>
<hr>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Educational Qualification</label>
            <div class="col-sm-10">
                <select class="form-control" id="educatuteAuth">
                    <option class="dissolv" value="SSC">SSC</option>
                    <option class="dissolv" value="HSC">HSC</option>
                    <option class="dissolv" value="Graduate">Graduate</option>
                    <option class="dissolv" value="Any Other">Any Other</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">If other qualification</label>
            <div class="col-sm-10">
                <textarea class="form-control" id="inputEmail6" placeholder="Other Qualification"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
    <h4>Previous Experience as BC (if any)</h4> 
</div>
<hr>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="bankName" class="col-sm-2 control-label">Bank Name</label>
            <div class="col-sm-10">
                <select class="form-control" id="bankName">
                    <option value="0">Select</option>
                    <option value="1">State Bank of India</option>
                    <option value="12">Bank of Baroda</option>
                    <option value="13">Axis Bank</option>
                    <option value="14">Union Bank of India</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Branch</label>
            <div class="col-sm-10">
                <select class="form-control" id="bankBranch">
                    <option value="0">Select</option>
                    <option class="dissolv" value="5">Branch1</option>
                    <option class="dissolv" value="32">branch2</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">From Date</label>
            <div class="col-sm-10">
                <input type="date" class="form-control" id="fromDate" placeholder="Enter From Date">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">To Date</label>
            <div class="col-sm-10">
                <input type="date" class="form-control" id="toDate" placeholder="Enter To Date">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Reasons</label>
            <div class="col-sm-10">
                <textarea class="form-control" id="inputEmail6" placeholder="Reasons"></textarea>
            </div>
        </div>
    </div>
</div>

<script>
	$(function() {
		var t2 = $('#example2').DataTable({
	    	"paging":   false,
	        "ordering": false,
	        "info":     false,
	        "searching": false
	    });

	    $('#addMoreButton2').on( 'click', function () {
	        t2.row.add( [
	            $("#certificateDate").val(),
                $("#certificateAuth").val(),
                $("#certificationCourse").val(),
	            $("#certificationGrade").val(),
	            'delete'
	        ] ).draw( false );
	        $("#certificateDate").val("");
	        $("#certificateGrade").val("");
	    } );
	});
</script>