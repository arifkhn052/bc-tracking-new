<br>
<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
    <h4>Associated Bank Detail</h4> </div>
<hr>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Appointment Date</label>
    <div class="col-sm-10">
        <input type="date" class="form-control" id="fromDate" placeholder="Enter Appointment Date">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">IFSC Code</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="ifscCode" placeholder="IFSC Code" value="SBIN1231212">
    </div>
</div>
<div class="form-group">
    <span class="col-sm-12 text-center">--or--</span>
</div>
<div class="form-group">
    <label for="bankName" class="col-sm-2 control-label">Bank Name</label>
    <div class="col-sm-10">
        <select class="form-control" id="bankName" disabled value="12">
        	<option value="0">Select</option>
            <option value="1" selected>State Bank of India</option>
            <option value="12">Bank of Baroda</option>
            <option value="13">Axis Bank</option>
            <option value="14">Union Bank of India</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Branch</label>
    <div class="col-sm-10">
        <select class="form-control" id="bankBranch" disabled value="32">
        	<option value="0">Select</option>
            <option class="dissolv" value="5" selected>Branch1</option>
            <option class="dissolv" value="32">branch2</option>
        </select>
    </div>
</div>
<br>
<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
    <h4>BC Type and Workings</h4> </div>
<hr>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Type of BC</label>
            <div class="col-sm-10">
                <select class="form-control" id="bctype">
                    <option value="0">Select</option>
                    <option class="dissolv" value="Individual">Individual</option>
                    <option class="dissolv" value="Corporate">Corporate</option>
                    <option class="dissolv" value="Fixed" selected>Fixed</option>
                    <option class="dissolv" value="Mobile">Mobile</option>
                    <option class="dissolv" value="Both">Both</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Working Days</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" id="workingDays" placeholder="Working Days" value="5">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Working Hours</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" id="workingHours" placeholder="Working Hours" value="9">
            </div>
        </div>
    </div>
</div>
<br>
<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
    <h4>Primary Location (For Fixed BC)</h4> </div>
<hr>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Postal Address</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="postalAddress" placeholder="Postal Address" value="132131">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Village Code</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="villageCode1" placeholder="Enter Village Code" value="dqw112313">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Village Detail</label>
            <div class="col-sm-10">
                <textarea name="villageDetail" id="villageDetail1" class="form-control" rows="4" disabled placeholder="Village Detail"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="bankName" class="col-sm-2 control-label">Taluk</label>
            <div class="col-sm-10">
                <select class="form-control" id="stateArea">
                    <option value="0">Select</option>
                    <option value="1">Taluk 1</option>
                    <option value="12" selected>Taluk</option>
                    <option value="13">Axis Taluk</option>
                    <option value="14">Union Taluk</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">District</label>
            <div class="col-sm-10">
                <select class="form-control" id="cityArea">
                    <option value="0">Select</option>
                    <option class="dissolv" value="1">NDistrict</option>
                    <option class="dissolv" value="6">TDistrict</option>
                    <option class="dissolv" value="9" selected>MDistrict</option>
                    <option class="dissolv" value="13">District</option>
                    <option class="dissolv" value="16">District</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">State</label>
            <div class="col-sm-10">
                <select class="form-control" id="locationArea">
                    <option value="0">Select</option>
                    <option class="dissolv" value="1">Navi Mumbai</option>
                    <option class="dissolv" value="6">Thane</option>
                    <option class="dissolv" value="9" selected>Mumbai</option>
                    <option class="dissolv" value="13">Pune</option>
                    <option class="dissolv" value="16">Nanded</option>
                    <option class="dissolv" value="18">Nashik</option>
                    <option class="dissolv" value="21">Sindhudurg</option>
                    <option class="dissolv" value="23">UDUPI</option>
                    <option class="dissolv" value="26">Raigad</option>
                    <option class="dissolv" value="28">Ahmednagar</option>
                    <option class="dissolv" value="30">Aurangabad</option>
                    <option class="dissolv" value="33">Ahmedabad</option>
                    <option class="dissolv" value="3242">Varanasi</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail6" class="col-sm-2 control-label">Pin Code</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="postalAddress" placeholder="Pin Code" value="123123">
            </div>
        </div>
    </div>
</div>
<br>

<br>
<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
    <h4>Areas of Operation</h4> </div>
<hr>

<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Village Code</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="villageCode" placeholder="Enter Village Code">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail6" class="col-sm-2 control-label">Village Detail</label>
    <div class="col-sm-10">
        <textarea name="villageDetail" id="villageDetail" class="form-control" rows="4" disabled placeholder="Village Detail"></textarea>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3 col-sm-offset-9">
        <input type="button" class="pull-right btn btn-danger" id="addMoreButton5" value="Add More">
    </div>
</div>


<div class="form-group">
    <table id="example5" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Village Code</th>
                <th>Village Detail</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<script>
	$(function() {
	    var t = $('#example').DataTable({
	    	"paging":   false,
	        "ordering": false,
	        "info":     false,
	        "searching": false
	    });
        var t5 = $('#example5').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "searching": false
        });
        
        $("#villageCode").on('blur', function() {
            $("#villageDetail").text('village 1, district 1, state');
        }).on('focus', function() {
            $("#villageDetail").text('');

        });

        $('#addMoreButton5').on( 'click', function () {
            t2.row.add( [
                $("#villageCode").val(),
                $("#villageDetail").text(),
                'delete'
            ] ).draw( false );
            $("#villageCode").val("");
            $("#villageDetail").text("");
        } );

	    

	 	/*$("#villageCode").on('blur', function() {
 			$("#villageDetail").text('village 1, district 1, state');
	 	}).on('focus', function() {
 			$("#villageDetail").text('');
	 	});*/

		

	    
	    $('#addMoreButton').on( 'click', function () {
	        t.row.add( [
	            $("#stateArea option:selected").text(),
	            $("#cityArea option:selected").text(),
	            $("#locationArea option:selected").text(),
	            $("#districtArea option:selected").text(),
	            $("#codeArea option:selected").text(),
	            $("#typeArea option:selected").text(),
	            'delete'
	        ] ).draw( false );
	        $("#stateArea").val("0");
            $("#cityArea").val("0");
            $("#locationArea").val("0");
            $("#districtArea").val("0");
            $("#codeArea").val("0");
            $("#typeArea").val("0");	    
        } );

	    $("#ifscCode").on('blur', function() {
	    	$("#bankName").val("1");
	    	$("#bankCircle").val("1");
	    	$("#bankState").val("2");
	    	$("#bankZone").val("3");
	    	$("#bankRegion").val("4");
	    	$("#bankCategory").val("5");
	    	$("#bankBranch").val("5");
	    }).on('focus', function() {
	    	$("#bankName").val("0");
	    	$("#bankCircle").val("0");
	    	$("#bankState").val("0");
	    	$("#bankZone").val("0");
	    	$("#bankRegion").val("0");
	    	$("#bankCategory").val("0");
	    	$("#bankBranch").val("0");
	    });
	});
</script>