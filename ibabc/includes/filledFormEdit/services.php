<br>
<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
    <h4>Products/Services Offered</h4> </div>
<hr>
<div class="form-group">
    <label for="bankName" class="col-sm-2 control-label">Products Offered</label>
    <div class="col-sm-10">
        <select class="form-control" id="">
        	<option value="0">Select</option>
            <option value="1">Deposit</option>
            <option value="12">Remittence</option>
            <option value="13" selected>Insurance</option>
            <option value="13">Pension</option>
            <option value="13">Credit Products</option>
            <option value="13">Other</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail5" class="col-sm-2 control-label">Minimum Cash Handling Limit</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail5" placeholder="Minimum Cash Handling Limit" value="20000">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail5" class="col-sm-2 control-label">Monthly Fixed Renumeration</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail5" placeholder="Monthly Fixed Renumeration" value="3000">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail5" class="col-sm-2 control-label">Monthly Variable Renumeration</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail5" placeholder="Monthly Variable Renumeration" value="5000">
    </div>
</div>
