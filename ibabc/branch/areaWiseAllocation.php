<?php 
$breadcrumbs = [
    "Reports" => "reports.php",
    "Area wise BC Allocation" => "areaWiseAllocation.php"
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
    <link href="../assets/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>
        <form class="form-inline">
          <div class="form-group">
            <label for="exampleInputName2">Date</label>
            <input type="date" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
          </div>
          <button type="submit" class="btn btn-default">Show List</button>
        </form>
        <hr>
        
        <table id="example" class="display" cellspacing="0" width="100%">
	        <thead>
	            <tr>
                    <th>Area ID</th>
	                <th>BC Count</th>
	                <th>Last update Date</th>
                    <th>View</th>
	            </tr>
	        </thead>
            <tbody>
                <tr>
                    <td>AREA1232</td>
                    <td>2</td>
                    <td>2012-03-12</td>
                    <td><a href="viewListInArea.php" class="btn btn-primary">View</a></td>
                </tr>
                <tr>
                    <td>AREA23423</td>
                    <td>0</td>
                    <td>2012-03-12</td>
                    <td><a href="viewListInArea.php" class="btn btn-primary">View</a></td>
                </tr>
            </tbody>
	    </table>
		        


        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
	    var t = $('#example').DataTable({});


 	} );
</script>

</html>
