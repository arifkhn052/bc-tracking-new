<?php 
$breadcrumbs = [
    "Reports" => "reports.php",
    "List as on date" => "listason.php"
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
    <link href="../assets/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>

        <form class="form-inline">
          <div class="form-group">
            <label for="exampleInputName2">Date</label>
            <input type="date" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
          </div>
          <button type="submit" class="btn btn-default">Show List</button>
        </form>
        <hr>
        
        <table id="example" class="display" cellspacing="0" width="100%">
	        <thead>
	            <tr>
                    <th>ID</th>
	                <th>Name</th>
	                <th>Phone</th>
                    <th>Aadhar Card</th>
                    <th>Certificates</th>
                    <th>Villages</th>
                    <th>Current Status</th>
                    <th>View</th>
	            </tr>
	        </thead>
            <tbody>
                <tr>
                    <td>U-21231</td>
                    <td>BC 1</td>
                    <td>999123123</td>
                    <td>A21S1234</td>
                    <td>Auth1, Auth2</td>
                    <td>Vill1, Vill2</td>
                    <td>Blacklisted</td>
                    <td><a href="viewbc.php" class="btn btn-primary">View</a></td>
                </tr>
                <tr>
                    <td>U-21232</td>
                    <td>BC 2</td>
                    <td>9991231234</td>
                    <td>A21S1221</td>
                    <td>Auth1</td>
                    <td>Vill4, Vill3, Vill5</td>
                    <td>Present</td>
                    <td><a href="viewbc.php" class="btn btn-primary">View</a></td>
                </tr>
            </tbody>
	    </table>
		        


        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
	    var t = $('#example').DataTable({});


 	} );
</script>

</html>
